/*  (c) Copyright:  2017..2018  Patrn, Confidential Data  
 *
 *  $Workfile:          rpi_page.c
 *  $Revision:          $
 *  $Modtime:           $
 *
 *  Purpose:            Dynamic HTTP page for Raspberry pi webserver
 *
 *
 *  Entry Points:       HTTP_DynamicPageHandler()
 *                      HTTP_InitDynamicPages()
 *                      HTTP_PageIsDynamic()
 *                      HTTP_SendTextFile()
 *
 *  Compiler/Assembler: gnu gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       20 Oct 2017
 *
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_http.h"
#include "rpi_func.h"
#include "rpi_json.h"
//
#include "rpi_page.h"

//#define USE_PRINTF
#include "printf.h"

//
// Generic HTTP data from common lib
//
extern const char *pcHttpResponseHeader;
extern const char *pcWebPageStart;
extern const char *pcWebPageTitle;
extern const char *pcWebPageDefault;
extern const char *pcWebPageEnd;

//
// Local prototypes
//
static bool    http_DynPageDefault           (NETCL *, int);
static bool    http_DynPageEmpty             (NETCL *, int);
static bool    http_DynPageExitSlim          (NETCL *, int);
static bool    http_DynPageExit              (NETCL *, int);
static bool    http_DynPageLog               (NETCL *, int);
static bool    http_DynPageParms             (NETCL *, int);
//
// Local prototypes
//
static char   *http_ExtractPageName          (NETCL *);
static bool    http_SendTextFile             (NETCL *, char *);

//
//
// Enums and dynamic LUTs
//
enum
{
   #define  EXTRACT_DYN(a,b,c,d,e,f)   a,
   #include "pages.h"
   #undef EXTRACT_DYN
   NUM_PAGES_DYNS
};
//
static const NETDYN stDynamicPages[] =
{
//    tUrl,    tType,   iTimeout,   iFlags,  pcUrl,   pfDynCb;
   #define  EXTRACT_DYN(a,b,c,d,e,f)   {a,b,c,d,e,f},
   #include "pages.h"
   #undef EXTRACT_DYN
   {  -1,      0,       0,          0,       NULL,    NULL  }
};

//
// Misc global http header strings 
// HTTP Response:
//=============================
//    HTTP Response header
//    NEWLINE
//    Optional Message body
//    NEWLINE NEWLINE
//
const char *pcWebPageLineBreak          = "<br/>";
const char *pcWebPagePreStart           = "<div align=\"left\"><pre>";
const char *pcWebPagePreEnd             = "</pre></div> ";
//
const char *pcHttpResponseLength        = "HTTP/1.0 200 OK" NEWLINE
                                          "Content-Type: text/html" NEWLINE
                                          "Content-Length: %d" NEWLINE NEWLINE;
const char *pcHttpResponseHeaderJson    = "HTTP/1.0 200 OK" NEWLINE "Content-Type=application/json" NEWLINE NEWLINE;
const char *pcHttpResponseLengthJson    = "HTTP/1.0 200 OK" NEWLINE
                                          "Content-Type: application/json" NEWLINE
                                          "Content-Length: %d" NEWLINE NEWLINE;
const char *pcHttpResponseHeaderBad     = "HTTP/1.0 400 Bad Request" NEWLINE NEWLINE;
const char *pcWebPageCenter             = "<body style=\"text-align:center\">" NEWLINE;
const char *pcWebPageLeft               = "<body style=\"text-align:left\">" NEWLINE;
const char *pcWebPageFontStart          = "<p style=\"font-family:'%s'\">" NEWLINE;
const char *pcWebPageFontEnd            = "</p>" NEWLINE;
const char *pcWebPageExitWarning        = "<hr/>" NEWLINE
                                         "<h2>PATRN ESS</h2>" NEWLINE
                                         "<h3>Cannot exit Smart-E HTTP server !!</h3>" NEWLINE
                                         "<hr/>" NEWLINE;
const char *pcWebPageNotImplemented     = "<hr/>" NEWLINE
                                         "<h2>PATRN ESS</h2>" NEWLINE
                                         "<h3>Page not implemented</h3>" NEWLINE
                                         "<hr/>" NEWLINE;
//
// HTTP table start: Needs border, width(%), colspan, headerstring
//
const char *pcWebPageTableStart         = "<table align=center border=%d width=\"%d%%\">" NEWLINE
                                         "<body style=\"text-align:left\">" NEWLINE
                                         "<thead>" NEWLINE
                                         "<th colspan=%d>%s</th>" NEWLINE;
const char *pcWebPageTableEnd           = "</tbody></table>" NEWLINE;
//
// HTTP table row: Needs row height
//
const char *pcWebPageTableRowStart      = "<tr height=%d>" NEWLINE;
const char *pcWebPageTableRowEnd        = "</tr>" NEWLINE;
const char *pcWebPageStopBad            = "<p>No data</p>" NEWLINE;

//=================================================================================================
//
// Misc local http header strings 
//
//=================================================================================================
static const char *pcWebPageEmpty       = "<br/>" NEWLINE
                                         "Welcome to another PATRN mini HTTP server." NEWLINE
                                         "<a href=\"http://www.patrn.nl\" target=\"_blank\">Patrn.nl</a>" NEWLINE
                                         "<br/>" NEWLINE;
static const char *pcWebExitPage        = "<br/>" NEWLINE
                                         "<br/>" NEWLINE
                                         "Exiting PATRN HTTP server. Good bye !" NEWLINE
                                         "<br/>" NEWLINE
                                         "<br/>" NEWLINE;

//
// HTTP table cel data: Needs [cell width,] data
//
static const char *pcWebPageCellData[] = 
{
   "<td>%s</td>" NEWLINE,
   "<td width=\"%d%%\">%s</td>" NEWLINE NEWLINE
};

//
// HTTP table cel link data : Needs [cell width,] link, data
//
static const char *pcWebPageCellDataLink[] =
{
   "<td><a href=\"%s\">%s</td>" NEWLINE,
   "<td width=\"%d%%\"><a href=\"%s\">%s</td>" NEWLINE
};

//
// HTTP table cel numeric data : Needs [cell width,] data
//
static const char * pcWebPageCellDataNumber[] =
{
   "<td>%d</td>" NEWLINE,
   "<td width=\"%d%%\">%d</td>" NEWLINE
};

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   HTTP_DynamicPageHandler
// Purpose:    Execute the dynamic webpage callback
// 
// Parameters: Socket description, DynPage index
// Returns:    TRUE if there is data to be send back
// Note:       
//
bool HTTP_DynamicPageHandler(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   PFVOIDPI pfCb;

   PRINTF1("HTTP_DynamicPageHandler():Idx=%d" CRLF, iIdx);
   //
   pfCb = GEN_GetDynamicPageCallback(iIdx);
   if(pfCb)
   {
      fCc = pfCb(pstCl, iIdx);
   }
   return(fCc);
}

// 
// Function:   HTTP_InitDynamicPages
// Purpose:    Register dynamic webpages
// 
// Parameters: Port
// Returns:    Nr registered
// Note:       
//
int HTTP_InitDynamicPages(int iPort)
{
   int            iNr=0; 
   const NETDYN  *pstDyn=stDynamicPages;

   PRINTF1("HTTP_InitDynamicPages(): port=%d" CRLF, iPort);
   //
   while(pstDyn->pcUrl)
   {
      //PRINTF1("HTTP_InitDynamicPages():Url=%s" CRLF, pstDyn->pcUrl);
      if(pstDyn->iFlags & DYN_FLAG_PORT)
      {
         //
         // Register for this specific port
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout, iPort, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      else
      {
         //
         // Register for all ports
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout,     0, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      iNr++;
      pstDyn++;
   }
   return(iNr);
}

//
// Function:   HTTP_PageIsDynamic
// Purpose:    Check if the requested page is dynamic or (file)static
// 
// Parameters: Net client struct
// Returns:    >=0 if requested page is dynamic (and needs construction)
// Note:       
//
int HTTP_PageIsDynamic(NETCL *pstCl)
{
   int   iIdx=0, iLen, iPort;
   char *pcDynName;
   char *pcPagename;

   pcPagename = http_ExtractPageName(pstCl);

   if ( pcPagename == NULL)   return(-1);
   if (*pcPagename == '/')    pcPagename++;
   if (*pcPagename == 0)      return(0);

   do
   {
      if( (pcDynName = GEN_GetDynamicPageName(iIdx)) )
      {
         iLen = GEN_STRLEN(pcPagename);
         PRINTF3("HTTP_PageIsDynamic(): URL=%s:%04d, DynPage=%s" CRLF, pcPagename, pstCl->iPort, pcDynName);
         if(GEN_STRNCMP(pcDynName, pcPagename, iLen) == 0)
         {
            //
            // This request is a dynamic HTTP page: check if port matches !
            //
            iPort = GEN_GetDynamicPagePort(iIdx);
            if( (iPort == 0) || (iPort == pstCl->iPort) ) return(iIdx);
         }
         //
         // No match: keep looking
         //
         iIdx++;
      }
   }
   while(pcDynName);
   return(-1);
}

// 
// Function:   HTTP_SendTextFile
// Purpose:    Send a textfile from fs to socket
// 
// Parameters: Client, filename
// Returns:    TRUE if OKee
// Note:       
//
bool HTTP_SendTextFile(NETCL *pstCl, char *pcFile)
{
   bool     fCc=FALSE;
   int      iRead, iTotal=0;
   FILE    *ptFile;
   char    *pcBuffer;
   char    *pcRead;

   ptFile = fopen(pcFile, "r");

   if(ptFile)
   {
      pcBuffer = safemalloc(RCV_BUFFER_SIZE);
      //
      PRINTF1("HTTP_SendTextFile(): File=<%s>"CRLF, pcFile);
      do
      {
         pcRead = fgets(pcBuffer, RCV_BUFFER_SIZE, ptFile);
         if(pcRead)
         {
            iRead = GEN_STRLEN(pcBuffer);
            //PRINTF2("HTTP_SendTextFile(): %s(%d)"CRLF, pcBuffer, iRead);
            HTTP_SendData(pstCl, pcBuffer, iRead);
            iTotal += iRead;
         }
         else
         {
            //PRINTF("HTTP_SendTextFile(): EOF" CRLF);
         }
      }
      while(pcRead);
      //PRINTF1("HTTP_SendTextFile(): %d bytes written."CRLF, iTotal);
      fclose(ptFile);
      free(pcBuffer);
      fCc = TRUE;
   }
   else
   {
      PRINTF1("HTTP_SendTextFile(): Error open file <%s>"CRLF, pcFile);
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__BUILD_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   HTTP_BuildStart
// Purpose:    Put out HTTP page start 
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildStart(NETCL *pstCl)
{
   //
   // Put out the HTML header, start tag, title etc
   //
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildGeneric(pstCl, pcWebPageDefault);
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   return(TRUE);
}

//
// Function:   HTTP_BuildRespondHeader
// Purpose:    Put out HTTP response header
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildRespondHeader(NETCL *pstCl)
{
   return( HTTP_BuildGeneric(pstCl, pcHttpResponseHeader) );
}

//
// Function:   HTTP_BuildRespondHeaderBad
// Purpose:    Put out HTTP response header Bad Request
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildRespondHeaderBad(NETCL *pstCl)
{
   return( HTTP_BuildGeneric(pstCl, pcHttpResponseHeaderBad) );
}

//
// Function:   HTTP_BuildRespondHeaderJson
// Purpose:    Put out HTTP page start Json
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildRespondHeaderJson(NETCL *pstCl)
{
   return( HTTP_BuildGeneric(pstCl, pcHttpResponseHeaderJson) );
}

//
// Function:   HTTP_BuildLineBreaks
// Purpose:    Put out HTTP linebreaks
// 
// Parameters: Client, number of newlines
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildLineBreaks(NETCL *pstCl, int iNr)
{
   //
   // Put out the HTML header, start tag, title etc
   //
   while(iNr--) HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   return(TRUE);
}

//
// Function:   HTTP_BuildDivStart
// Purpose:    Put out HTTP page div/section start 
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildDivStart(NETCL *pstCl)
{
   //
   // Put out the HTML div start tag
   //
   HTTP_BuildGeneric(pstCl, pcWebPagePreStart);
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   return(TRUE);
}

//
// Function:   HTTP_BuildDivEnd
// Purpose:    Put out HTTP page div/section end
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildDivEnd(NETCL *pstCl)
{
   //
   // Put out the HTML div end tag
   //
   HTTP_BuildGeneric(pstCl, pcWebPagePreEnd);
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   return(TRUE);
}

//
// Function:   HTTP_BuildEnd
// Purpose:    Put out HTTP page end
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildEnd(NETCL *pstCl)
{
   //
   // Put out the HTML end
   //
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   HTTP_BuildGeneric(pstCl, pcWebPageEnd);
   return(TRUE);
}

/*------  Local functions separator ------------------------------------
__TABLE_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   HTTP_BuildTableStart
// Purpose:    Start a http web page table
// 
// Parameters: Client, page header, border with, table width, number of columns
// Returns:    
// Note:       
//
bool  HTTP_BuildTableStart(NETCL *pstCl, const char *pcPage, u_int16 usBorder, u_int16 usWidth, u_int16 usCols)
{
   bool  fOKee;
   
   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageTableStart, usBorder, usWidth, usCols, pcPage);

   return(fOKee);
}

//
// Function:   HTTP_BuildTableRowStart
// Purpose:    Starts a row in a table
// 
// Parameters: Client, row height
// Returns:    
// Note:       
//
bool  HTTP_BuildTableRowStart(NETCL *pstCl,  u_int16 usHeight)
{
   bool  fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageTableRowStart, usHeight);
   
   return(fOKee);
}

//
// Function:   HTTP_BuildTableRowEnd
// Purpose:    Ends a row
// 
// Parameters: Client
// Returns:    
// Note:       
//
bool  HTTP_BuildTableRowEnd(NETCL *pstCl)
{
   bool fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageTableRowEnd);
   
   return(fOKee);
}

//
// Function:   HTTP_BuildTableColumnText
// Purpose:    Start a generic column
// 
// Parameters: Client, page struct, column text, column width
// Returns:    
// Note:       
//
bool  HTTP_BuildTableColumnText(NETCL *pstCl, char *pcText, u_int16 usWidth)
{
   bool     fOKee;

   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellData[1], usWidth, pcText);
   else        fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellData[0], pcText);

   return(fOKee);
}

//
// Function:   HTTP_BuildTableColumnLink
// Purpose:    Start a link with a hyperlink
// 
// Parameters: Client, page struct, column text, width, link
// Returns:    
// Note:       
//
bool  HTTP_BuildTableColumnLink(NETCL *pstCl, char *pcText, u_int16 usWidth, char *pcLink)
{
   bool  fOKee;

   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellDataLink[1], usWidth, pcLink, pcText);
   else        fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellDataLink[0], pcLink, pcText);

   return(fOKee);
}

//
// Function:   HTTP_BuildTableColumnNumber
// Purpose:    Start a column with a number
// 
// Parameters: Client, page struct, number, width
// Returns:    
// Note:       
//
bool  HTTP_BuildTableColumnNumber(NETCL *pstCl, u_int16 usNr, u_int16 usWidth)
{
   bool     fOKee;
   
   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellDataNumber[1], usWidth, usNr);
   else        fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellDataNumber[0], usNr);

   return(fOKee);
}

//
// Function:   HTTP_BuildTableEnd
// Purpose:    End a http web page table
// 
// Parameters: Client
// Returns:    
// Note:       
//
bool  HTTP_BuildTableEnd(NETCL *pstCl)
{
   bool  fOKee;
   
   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageTableEnd);

   return(fOKee);
}

/*------  Local functions separator ------------------------------------
__PAGE_HANDLERS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   http_DynPageDefault
// Purpose:    Dynamically created default page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageDefault(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         //
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         HTTP_BuildGeneric(pstCl, pcWebPageEmpty);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "info");
         fCc = TRUE;
         break;
   }
   return(fCc);
}

//
// Function:   http_DynPageEmpty
// Purpose:    Dynamically created empty page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageEmpty(NETCL *pstCl, int iIdx)
{
   int      iSize;
   char    *pcFile;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //
   // Build default file pathname
   //
   iSize  = (2 * MAX_PATH_LEN) + 1;
   pcFile = safemalloc(iSize);
   //
   GEN_SPRINTF(pcFile, "%s%s", pstMap->G_pcWwwDir, pstMap->G_pcDefault);
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   //
   // Try to send out the default HTML file. If none exists, revert to default lines
   //
   if(!http_SendTextFile(pstCl, pcFile) )
   {
      //
      // Put out a default reply: 
      //    
      HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
      HTTP_BuildGeneric(pstCl, pcWebPageDefault);
      HTTP_BuildGeneric(pstCl, pcWebPageEmpty);
      HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   }
   HTTP_BuildGeneric(pstCl, pcWebPageEnd);
   safefree(pcFile);
   return(TRUE);
}

//
// Function:   http_DynPageExit
// Purpose:    Dynamically created system page to capture illegal exits from the server
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool http_DynPageExit(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         //
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageExitWarning);
         HTTP_BuildGeneric(pstCl, pcWebExitPage);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         //
         PRINTF("http_DynPageExit():Warning: no Smart-E HTTP server exit here !" CRLF);
         LOG_Report(0, "DYN", "Smart-E HTTP server exit request from browser: IGNORED");
         fCc = TRUE;
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "exit");
         break;
   }
   return(fCc);
}

//
// Function:   http_DynPageExitSlim
// Purpose:    Dynamically created system page to exit the SmartE meter server
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       This call acts as a gracefull exit from the server to the Linux command shell
//
static bool http_DynPageExitSlim(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         //
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         HTTP_BuildGeneric(pstCl, pcWebExitPage);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         //
         //PRINTF("http_DynPageExitSlim():OKee" CRLF);
         LOG_Report(0, "DYN", "Exit Smart-E server request from browser");
         //
         // Request HOST exit
         //
         kill(GLOBAL_PidGet(PID_HOST), SIGINT);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "exitslim");
         break;
   }
   return(fCc);
}

//
// Function:   http_DynPageLog
// Purpose:    Dynamically created system page to show the log file
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool http_DynPageLog(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         // 
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         //
         HTTP_BuildGeneric(pstCl, pcWebPagePreStart);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         http_SendTextFile(pstCl, RPI_PUBLIC_LOG_PATH);
         HTTP_BuildGeneric(pstCl, pcWebPagePreEnd);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         //
         fCc = TRUE;
         //PRINTF("http_DynPageLog():OKee" CRLF);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "log");
         break;
   }
   return(fCc);
}

// 
// Function:   http_DynPageParms
// Purpose:    Dynamically created parameters page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageParms(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   PRINTF("http_DynPageParms()" CRLF);
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("http_DynPageParms():No type !" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("http_DynPageParms():Html" CRLF);
         //
         // Put out the HTML header, start tag and the rest
         //
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         PRINTF("http_DynPageParms():Json" CRLF);
         GEN_STRCPY(pstMap->G_pcCommand, "parms");
         fCc = RPI_BuildJsonMessageArgs(pstCl, VAR_ALL);
         break;
   }
   return(fCc);
}


/*------  Local functions separator ------------------------------------
__HTTP_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/


// 
// Function:   http_ExtractPageName
// Purpose:    Remove HTTP specific chars 
// 
// Parameters: Net client struct
// Returns:    Filename ptr
// Note:       
//
static char *http_ExtractPageName(NETCL *pstCl)
{
   char *pcPageName = NULL;

   //
   //   "GET /index.html HTTP/1.1"
   //   "Host: 10.0.0.231
   //   "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)
   //   "Accept: image/png,image/*;q=0.8,*/*;q=0.5
   //   "
   //   "Accept-Encoding: gzip,deflate
   //   "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
   //   "Keep-Alive: 115
   //   "Connection: keep-alive
   //   CR,CR,LF,CR,CR,LF
   //
   //    pcUrl
   //       iFileIdx : index to filename
   //       iExtnIdx : index to extension
   //
   PRINTF2("http_ExtractPageName(%d)=%s" CRLF, pstCl->iFileIdx, pstCl->pcUrl);
   if(pstCl->iFileIdx != -1)
   {
      pcPageName = &pstCl->pcUrl[pstCl->iFileIdx];
      PRINTF2("http_ExtractPageName(%d)=%s" CRLF, pstCl->iFileIdx, pcPageName);
   }
   return(pcPageName);
}

// 
// Function:   http_SendTextFile
// Purpose:    Send a textfile from fs to socket
// 
// Parameters: Client, filename
// Returns:    TRUE if OKee
// Note:       
//
static bool http_SendTextFile(NETCL *pstCl, char *pcFile)
{
   bool     fCc=FALSE;
   int      iRead, iTotal=0;
   FILE    *ptFile;
   char    *pcBuffer;
   char    *pcRead;

   ptFile = fopen(pcFile, "r");

   if(ptFile)
   {
      pcBuffer = safemalloc(RCV_BUFFER_SIZE);
      //
      PRINTF1("http_SendTextFile(): File=<%s>"CRLF, pcFile);
      do
      {
         pcRead = fgets(pcBuffer, RCV_BUFFER_SIZE, ptFile);
         if(pcRead)
         {
            iRead = GEN_STRLEN(pcBuffer);
            //PRINTF2("http_SendTextFile(): %s(%d)"CRLF, pcBuffer, iRead);
            HTTP_SendData(pstCl, pcBuffer, iRead);
            iTotal += iRead;
         }
         else
         {
            //PRINTF("http_SendTextFile(): EOF" CRLF);
         }
      }
      while(pcRead);
      //PRINTF1("http_SendTextFile(): %d bytes written."CRLF, iTotal);
      fclose(ptFile);
      free(pcBuffer);
      fCc = TRUE;
   }
   else
   {
      PRINTF1("http_SendTextFile(): Error open file <%s>"CRLF, pcFile);
   }
   return(fCc);
}



