/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           obis.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Macro def file obis codes
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Oct 2017
 *
 *  Revisions:
 *
 *
**/

//
// Macro:
//       
//             Enum           OBIS              Funct                         Help text
EXTRACT_OBIS(OBIS_ADM_EQUIP,  "0-0:96.1.1",     obis_AdmEquipment,            "Equipment identifier"                                                              )
EXTRACT_OBIS(OBIS_ADM_PWRFL,  "0-0:96.7.9",     obis_AdmPowerFailLong,        "Number of long power failures in any phases"                                       )
EXTRACT_OBIS(OBIS_ADM_PWRFS,  "0-0:96.7.21",    obis_AdmPowerFailShort,       "Number of power failures in any phases"                                            )
EXTRACT_OBIS(OBIS_CUR_TARIF,  "0-0:96.14.0",    obis_CurrentTar,              "Tariff indicator electricity"                                                      )
EXTRACT_OBIS(OBIS_GAS,        "0-1:24.2.1",     obis_GasDelivered,            "Meter Reading gas delivered in m3"                                               )
EXTRACT_OBIS(OBIS_PRW_ACT_P,  "1-0:1.7.0",      obis_PowerActualDelivered,    "Actual electricity power delivered  (+P) in 0,001 kW resolution"                    )
EXTRACT_OBIS(OBIS_PWR_ACT_M,  "1-0:2.7.0",      obis_PowerActualReceived,     "Actual electricity power received   (-P)  in 0,001 kW resolution"                    )
EXTRACT_OBIS(OBIS_PWR_T1_P,   "1-0:1.8.1",      obis_PowerTotalDeliveredTar1, "Meter Reading electricity delivered (T1) in 0,001 kWh"           )
EXTRACT_OBIS(OBIS_PWR_T2_P,   "1-0:1.8.2",      obis_PowerTotalDeliveredTar2, "Meter Reading electricity delivered (T2) in 0,001 kWh"        )
EXTRACT_OBIS(OBIS_PWR_T1_M,   "1-0:2.8.1",      obis_PowerTotalReceivedTar1,  "Meter Reading electricity received  (T1) in 0,001 kWh"          )
EXTRACT_OBIS(OBIS_PWR_T2_M,   "1-0:2.8.2",      obis_PowerTotalReceivedTar2,  "Meter Reading electricity received  (T2) in 0,001 kWh"       )
EXTRACT_OBIS(OBIS_PWR_L1_P,   "1-0:21.7.0",     obis_PowerL1InstantDelivered, "Instantaneous active power L1 (+P)"                                                )
EXTRACT_OBIS(OBIS_PWR_L1_M,   "1-0:22.7.0",     obis_PowerL1InstantReceived,  "Instantaneous active power L1 (-P)"                                                )
EXTRACT_OBIS(OBIS_CUR_L1,     "1-0:31.7.0",     obis_CurrentL1Instant,        "Instantaneous current L1"                                                          )
EXTRACT_OBIS(OBIS_VOL_L1,     "1-0:32.7.0",     obis_VoltageL1Instant,        "Instantaneous voltage L1"                                                          )
EXTRACT_OBIS(OBIS_VOL_L1SAG,  "1-0:32.32.0",    obis_VoltageL1Sags,           "Number of voltage sags in phase L1"                                                )
EXTRACT_OBIS(OBIS_VOL_L1SWL,  "1-0:32.36.0",    obis_VoltageL1Swells,         "Number of voltage swells in phase L1"                                              )
EXTRACT_OBIS(OBIS_PWR_L2_P,   "1-0:41.7.0",     obis_PowerL2InstantDelivered, "Instantaneous active power L2 (+P)"                                                )
EXTRACT_OBIS(OBIS_PWR_L2_M,   "1-0:42.7.0",     obis_PowerL2InstantReceived,  "Instantaneous active power L2 (-P)"                                                )
EXTRACT_OBIS(OBIS_CUR_L2,     "1-0:51.7.0",     obis_CurrentL2Instant,        "Instantaneous current L2"                                                          )
EXTRACT_OBIS(OBIS_VOL_L2,     "1-0:52.7.0",     obis_VoltageL2Instant,        "Instantaneous voltage L2"                                                          )
EXTRACT_OBIS(OBIS_VOL_L2SAG,  "1-0:52.32.0",    obis_VoltageL2Sags,           "Number of voltage sags in phase L2"                                                )
EXTRACT_OBIS(OBIS_VOL_L2SWL,  "1-0:52.36.0",    obis_VoltageL2Swells,         "Number of voltage swells in phase L2"                                              )
EXTRACT_OBIS(OBIS_PWR_L3_P,   "1-0:61.7.0",     obis_PowerL3InstantDelivered, "Instantaneous active power L3 (+P)"                                                )
EXTRACT_OBIS(OBIS_PWR_L3_M,   "1-0:62.7.0",     obis_PowerL3InstantReceived,  "Instantaneous active power L3 (-P)"                                                )
EXTRACT_OBIS(OBIS_CUR_L3,     "1-0:71.7.0",     obis_CurrentL3Instant,        "Instantaneous current L3"                                                          )
EXTRACT_OBIS(OBIS_VOL_L3,     "1-0:72.7.0",     obis_VoltageL3Instant,        "Instantaneous voltage L3"                                                          )
EXTRACT_OBIS(OBIS_VOL_L3SAG,  "1-0:72.32.0",    obis_VoltageL3Sags,           "Number of voltage sags in phase L3"                                                )
EXTRACT_OBIS(OBIS_VOL_L3SWL,  "1-0:72.36.0",    obis_VoltageL3Swells,         "Number of voltage swells in phase L3"                                              )
EXTRACT_OBIS(OBIS_PWR_FAIL,   "1-0:99.97.0",    obis_PowerFailLog,            "Power failure event log"                                                           )
