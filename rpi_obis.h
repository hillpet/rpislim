/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           rpi_obis.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for rpi_obis.c
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Oct 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_OBIS_H_
#define _RPI_OBIS_H_

typedef enum _obis_ids_
{
#define EXTRACT_OBIS(a,b,c,d) a,
#include "obis.h"
   //
   OBIS_TIMEDATE,
   MAX_NUM_OBIS   
#undef   EXTRACT_OBIS
}  OBISID;

#define  MAX_EMTR       32          // EMTR Equipment storage space

//
// E-Meter stucture
//
typedef struct _e_meter_
{
   u_int32  ulSecsSample;           // Next sampling time
   int      iPeriod;                // Trigger for next period 
   int      iTotSamples;            // Total number of samples
   int      iCsvSamples;            // Csv   number of samples
   int      iCrcErrors;             // Total number of CRC errors
   int      tObis;                  // OBIS Enum
   //
   char     cEquipment[MAX_EMTR];   // OBIS_ADM_EQUIP  - Equipment identifier
   int      iNumLongPowerFails;     // OBIS_ADM_PWRFL  - Number of long power failures in any phases
   int      iNumShortPowerFails;    // OBIS_ADM_PWRFS  - Number of power failures in any phases
   int      iCurrTariff;            // OBIS_CUR_TARIF  - Tariff indicator electricity
   double   flGasDelivered;         // OBIS_GAS        - Actual gas delivered in 0,001 m3
   //
   double   flPowerActualDelivered; // OBIS_PRW_ACT_P  - Actual electricity power delivered (+P) in 0,001 kW
   double   flPowerActualReceived;  // OBIS_PWR_ACT_M  - Actual electricity power received  (-P) in 0,001 kW
   double   flPowerT1Delivered;     // OBIS_PWR_T1_P   - Meter Reading electricity delivered to client (low tariff) in 0,001 kWh
   double   flPowerT2Delivered;     // OBIS_PWR_T2_P   - Meter Reading electricity delivered to client (normal tariff) in 0,001 kWh
   double   flPowerT1Received;      // OBIS_PWR_T1_M   - Meter Reading electricity received from client (low tariff) in 0,001 kWh
   double   flPowerT2Received;      // OBIS_PWR_T2_M   - Meter Reading electricity received from client (normal tariff) in 0,001 kWh
   //
   double   flPowerL1Delivered;     // OBIS_PWR_L1_P   - Instantaneous active power L1 (+P)
   double   flPowerL1Received;      // OBIS_PWR_L1_M   - Instantaneous active power L1 (-P)
   double   flCurrentL1;            // OBIS_CUR_L1     - Instantaneous current L1
   double   flVoltageL1;            // OBIS_VOL_L1     - Instantaneous voltage L1
   int      iVoltageL1Sags;         // OBIS_VOL_L1SAG  - Number of voltage sags in phase L1
   int      iVoltageL1Swells;       // OBIS_VOL_L1SWL  - Number of voltage swells in phase L1
   //
   double   flPowerL2Delivered;     // OBIS_PWR_L2_P   - Instantaneous active power L2 (+P)
   double   flPowerL2Received;      // OBIS_PWR_L2_M   - Instantaneous active power L2 (-P)
   double   flCurrentL2;            // OBIS_CUR_L2     - Instantaneous current L2
   double   flVoltageL2;            // OBIS_VOL_L2     - Instantaneous voltage L2
   int      iVoltageL2Sags;         // OBIS_VOL_L2SAG  - Number of voltage sags in phase L2
   int      iVoltageL2Swells;       // OBIS_VOL_L2SWL  - Number of voltage swells in phase L2
   //
   double   flPowerL3Delivered;     // OBIS_PWR_L3_P   - Instantaneous active power L3 (+P)
   double   flPowerL3Received;      // OBIS_PWR_L3_M   - Instantaneous active power L3 (-P)
   double   flCurrentL3;            // OBIS_CUR_L3     - Instantaneous current L3
   double   flVoltageL3;            // OBIS_VOL_L3     - Instantaneous voltage L3
   int      iVoltageL3Sags;         // OBIS_VOL_L3SAG  - Number of voltage sags in phase L3
   int      iVoltageL3Swells;       // OBIS_VOL_L3SWL  - Number of voltage swells in phase L3
}  EMTR;

//
// Smart-E-meter OBIS data
//
typedef bool(*PFFUN)(char *, EMTR *);
//
typedef struct _obis_
{
   char       *pcObis;
   PFFUN       pfHandler;
   const char *pcInfo;
}  OBIS;

//
// Prototypes
//
int   OBIS_LookupCode            (char *, EMTR *);
int   OBIS_LookupMeterName       (char *, EMTR *);

#endif /* _RPI_OBIS_H_ */
