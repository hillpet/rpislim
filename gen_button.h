/*  (c) Copyright:  2018  Patrn, Confidential Data
 *
 *  Workfile:           gen_button.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for 
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       08 Feb 2018
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GEN_BUTTON_H_
#define _GEN_BUTTON_H_

bool GEN_Init           (void);
bool GEN_Exit           (void);


#endif /* _GEN_BUTTON_H_ */
