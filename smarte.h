/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           smarte.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Extract headerfile for Smart-E dynamic http pages
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       22 Oct 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

EXTRACT_DYN(DYN_SME_HTML_DATA,   HTTP_HTML,  0, DYN_FLAG_PORT, "smartdata.html",       slim_DynPageData              )
EXTRACT_DYN(DYN_SME_JSON_DATA,   HTTP_JSON,  0, DYN_FLAG_PORT, "smartdata.json",       slim_DynPageData              )
EXTRACT_DYN(DYN_SME_HTML_FILES,  HTTP_HTML,  0, DYN_FLAG_PORT, "smartfiles.html",      slim_DynPageFiles             )
EXTRACT_DYN(DYN_SME_JSON_FILES,  HTTP_JSON,  0, DYN_FLAG_PORT, "smartfiles.json",      slim_DynPageFiles             )
EXTRACT_DYN(DYN_SME_HTML_DAY,    HTTP_HTML,  0, DYN_FLAG_PORT, "smartday.html",        slim_DynPageDay               )
EXTRACT_DYN(DYN_SME_JSON_DAY,    HTTP_JSON,  0, DYN_FLAG_PORT, "smartday.json",        slim_DynPageDay               )
EXTRACT_DYN(DYN_SME_HTML_DAYSLM, HTTP_HTML,  0, DYN_FLAG_PORT, "slim",                 slim_DynPageDaySmart          )
EXTRACT_DYN(DYN_SME_JSON_DAYSLM, HTTP_JSON,  0, DYN_FLAG_PORT, "slim.json",            slim_DynPageDaySmart          )
EXTRACT_DYN(DYN_SME_HTML_DAYGAS, HTTP_HTML,  0, DYN_FLAG_PORT, "gas",                  slim_DynPageDayGas            )
EXTRACT_DYN(DYN_SME_HTML_DAYPWR, HTTP_HTML,  0, DYN_FLAG_PORT, "stroom",               slim_DynPageDayPower          )
EXTRACT_DYN(DYN_SME_HTML_DAYSOL, HTTP_HTML,  0, DYN_FLAG_PORT, "zon",                  slim_DynPageDaySolar          )
EXTRACT_DYN(DYN_SME_HTML_MON,    HTTP_HTML,  0, DYN_FLAG_PORT, "smartmonth.html",      slim_DynPageMonth             )
EXTRACT_DYN(DYN_SME_JSON_MON,    HTTP_JSON,  0, DYN_FLAG_PORT, "smartmonth.json",      slim_DynPageMonth             )
//
// LINK related HTML pages
//
EXTRACT_DYN(DYN_SME_HTML_MONPLS, HTTP_HTML,  0, DYN_FLAG_PORT, "smartmonthplus.html",  slim_DynPageMonthPlusHtml     )
EXTRACT_DYN(DYN_SME_HTML_MONMIN, HTTP_HTML,  0, DYN_FLAG_PORT, "smartmonthmin.html",   slim_DynPageMonthMinHtml      )
EXTRACT_DYN(DYN_SME_HTML_DAYPRV, HTTP_HTML,  0, DYN_FLAG_PORT, "smartprevday.html",    slim_DynPageDayPrevHtml       )
EXTRACT_DYN(DYN_SME_HTML_DAYNXT, HTTP_HTML,  0, DYN_FLAG_PORT, "smartnextday.html",    slim_DynPageDayNextHtml       )
EXTRACT_DYN(DYN_SME_HTML_DAYPLS, HTTP_HTML,  0, DYN_FLAG_PORT, "smartdayplus.html",    slim_DynPageDayPlusHtml       )
EXTRACT_DYN(DYN_SME_HTML_DAYMIN, HTTP_HTML,  0, DYN_FLAG_PORT, "smartdaymin.html",     slim_DynPageDayMinHtml        )
EXTRACT_DYN(DYN_SME_HTML_NXTSLM, HTTP_HTML,  0, DYN_FLAG_PORT, "slimnext.html",        slim_DynPageDaySmartNextHtml  )
EXTRACT_DYN(DYN_SME_HTML_PRVSLM, HTTP_HTML,  0, DYN_FLAG_PORT, "slimprev.html",        slim_DynPageDaySmartPrevHtml  )
EXTRACT_DYN(DYN_SME_HTML_PRVMON, HTTP_HTML,  0, DYN_FLAG_PORT, "smartprevmonth.html",  slim_DynPageMonthPrevHtml     )
EXTRACT_DYN(DYN_SME_HTML_NXTMON, HTTP_HTML,  0, DYN_FLAG_PORT, "smartnextmonth.html",  slim_DynPageMonthNextHtml     )
