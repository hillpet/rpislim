/*  (c) Copyright:  2017..2018  Patrn, Confidential Data
 *
 *  Workfile:           par_pids.h
 *  Revision:
 *  Modtime:
 *
 *  Purpose:            Extract headerfile for the RPI thread PIDs
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       28 Dec 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 **/

//          ePid           pcPid          pcHelp
EXTRACT_PID(PID_HOST,      "Host",        "Host")
EXTRACT_PID(PID_SRVR,      "Srvr",        "HTTP server")
EXTRACT_PID(PID_COMM,      "Comm",        "Serial Comms")
EXTRACT_PID(PID_SPARE1,    "Spr1",        "Spare 1")
EXTRACT_PID(PID_SPARE2,    "Spr2",        "Spare 2")
EXTRACT_PID(PID_SPARE3,    "Spr3",        "Spare 3")

