/*  (c) Copyright:  2017..2018 Patrn, Confidential Data
 *
 *  Workfile:           rpi_main.c
 *  Revision:   
 *  Modtime:    
 *
 *  Purpose:            rpislim reads the output of a smart e-meter
 *
 *
 *
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       13 Okt 2017
 *                                   
 *  Revisions:
 *  Entry Points:       main()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_srvr.h"
#include "rpi_func.h"
#include "rpi_comm.h"
#include "rpi_obis.h"
#include "gen_button.h"
//
#include "rpi_main.h"
//
#define USE_PRINTF
#include "printf.h"

#if defined(__DATE__) && defined(__TIME__)
   static const char RPI_BUILT[] = __DATE__ " " __TIME__;
#else
   static const char RPI_BUILT[] = "Unknown";
#endif

#ifdef      DEBUG_ASSERT                  // DEBUG BUILD 
//#define   SMARTMETER_SHORT              // Use short display of data
//#define   RPI_DISPLAY_FILE              // Display to log file
  #define   RPI_DISPLAY(a,b)
//static    void rpi_DisplaySmartMeter    (const char *, EMTR *);
//#define   RPI_DISPLAY(a,b)              rpi_DisplaySmartMeter(a,b)
  static    void rpi_DisplaySmartFloats   (const char *);
  #define   RPI_DAY(a)
//#define   RPI_DAY(a)                    rpi_DisplaySmartFloats(a)

#else       //DEBUG_ASSERT
  #define   RPI_DAEMON_MODE               // Release mode is rpi_Daemon() mode
  #define   SMARTMETER_SHORT              // Display to log file
  #define   RPI_DISPLAY_FILE              // Use short display of data
  static    pid_t   rpi_Daemon            (void);
  #define   RPI_DISPLAY(a,b)
//static    void rpi_DisplaySmartMeter    (const char *, EMTR *);
//#define   RPI_DISPLAY(a,b)              rpi_DisplaySmartMeter(a,b)
  #define   RPI_DAY(a)
#endif      //DEBUG_ASSERT
//
#if         defined FEATURE_SAMPLE_PER_YEAR
  #define   RTC_GETPERIOD(a)              RTC_GetYear(a)
#elif       defined FEATURE_SAMPLE_PER_MONTH
  #define   RTC_GETPERIOD(a)              RTC_GetMonth(a)
#elif       defined FEATURE_SAMPLE_PER_DAY
  #define   RTC_GETPERIOD(a)              RTC_GetDay(a)
#else
  #error    No Sample period specified !
#endif
//
static bool    rpi_AppendCsvSample        (OBISID, EMTR *);
static bool    rpi_CheckCrc16             (char *);
static void    rpi_WriteCsvRecord         (char *, int);
//
static void    rpi_EmtrAdd                (EMTR *, EMTR *);
static bool    rpi_EmtrConvertObis        (char *, EMTR *, EMTR *);
static void    rpi_EmtrSampleCsv          (u_int32, EMTR *, EMTR *, EMTR *);
static void    rpi_EmtrSampleDay          (u_int32, EMTR *, EMTR *);
static void    rpi_EmtrSampleMon          (u_int32, EMTR *, EMTR *);
static void    rpi_EmtrSampleQtr          (u_int32, EMTR *, EMTR *);
static bool    rpi_EmtrSaveDay            (u_int32, u_int8 *, int);
static bool    rpi_EmtrSaveMonth          (u_int32, u_int8 *, int);
static void    rpi_EmtrUpdateGlobals      (EMTR *, EMTR *);
static void    rpi_EmtrVerifyCsvPeriod    (u_int32, EMTR*);
//
static void    rpi_PowerOff               (void);
static bool    rpi_HandleGuard            (void);
static bool    rpi_GetWan                 (void);
static void    rpi_PostSemaphore          (void);
static bool    rpi_SignalRegister         (sigset_t *);
static void    rpi_ReceiveSignalInt       (int);
static void    rpi_ReceiveSignalTerm      (int);
static void    rpi_ReceiveSignalUser1     (int);
static void    rpi_ReceiveSignalUser2     (int);
static void    rpi_ReceiveSignalSegmnt    (int);
//
static int     rpi_Startup                (void);
static bool    rpi_WaitStartup            (int);
//
// CLI functions
//
static void    rpi_RepairMonthData        (int, int);
static void    rpi_RestoreActualData      (void);
static bool    rpi_RestoreMapData         (u_int32);
static void    rpi_RestoreMonthData       (int, int);
static void    rpi_ShowDay                (void);
static void    rpi_ShowMonth              (void);
static void    rpi_ShowObisData           (int, int, int);

//
// Generic timestamp
//
static char        cDateTime[32];
static const char *pcLogfile        = RPI_PUBLIC_LOG_PATH;
static bool        fRpiRunning      = TRUE;
static bool        fRpiDeamonMode   = FALSE;
static char       *pcCsvFile        = RPI_CSV_FILE;
static char       *pcFltFile        = RPI_FLT_FILE;
static int         tCsvFile         = 0;
//
static char       *pcCsvBuffer      = NULL;
static char       *pcCsvTimeDate    = NULL;
//
static const char *pcEms[NUM_EMS]   =
{
   "Gas",
   "T1+",
   "T1-",
   "T2+",
   "T2-"
};

/* ======   Local Functions separator ===========================================
___MAIN_FUNCTIONS(){}
==============================================================================*/

//
//  Function:   main
//  Purpose:    Main entry point for the Raspberry Pi Smart-E-meter monitor
//
//  Parms:      Commandline options
//  Returns:    Exit codes
//
int main(int argc, char **argv)
{
   bool        fMapOkee, fRpiPowerOff=FALSE;
   int         iGuardCount;
   u_int32     ulSecsNow;
   u_int32     ulSecsGuard, iGuardsSecs;
   u_int32     ulSecsWanCheck;
   int         iOpt, iAddZero=0, iAddSpare=0;
   int         iDay=0, iMonth=0, iYear=0, iBuild=0;
   int         iActivityLed=0;
   pid_t       tPid;
   sigset_t    tBlockset;
   char       *pcBuffer=NULL;
   EMTR       *pstEmtrTot;
   EMTR       *pstEmtrNow;
   EMTR       *pstEmtrQtr;
   EMTR       *pstEmtrDay;
   EMTR       *pstEmtrMon;
   EMTR       *pstEmtrCsv;
   RPIMAP     *pstMap;

   //
   // Use to determine G_ sizes
   //
   // #define FEATURE_PRINT_G_SIZES
   #ifdef FEATURE_PRINT_G_SIZES
      LOG_printf("rpi-main(): Sizeof stPidList  = %d" CRLF, sizeof(pstMap->G_stPidList));
      LOG_printf("rpi-main(): Sizeof pcHostname = %d" CRLF, sizeof(pstMap->G_pcHostname));
      LOG_printf("rpi-main(): Sizeof stDynPages = %d" CRLF, sizeof(pstMap->G_stDynPages));
      LOG_printf("rpi-main(): Sizeof stCmd      = %d" CRLF, sizeof(pstMap->G_stCmd));
      _exit(0);
   #endif

   //
   // Make sure we have the right mmap parameters setup for us !
   //
   fMapOkee = GLOBAL_Init();
   pstMap   = GLOBAL_GetMapping();
   //
   // Args:
   //    -Y 2017  : Set Year
   //    -M 11    : Set Month
   //    -D 25    : Show OBIS data month & days (ex: -Y 2017 -M 11 -D 25)
   //
   //    -B 11    : Build month file from day files (ex: -Y 2017 -B 11)
   //
   //    -R       : Restore MAP G_flThis* data from file
   //    -a 1000  : Expand map file persistent area
   //    -z 1000  : Expand map file zero-reset area
   //    
   //    OBIS Test mode (if no smartmeter attached:
   //    ================================================
   //    -O 1     : OBIS Testmode 0=COM data, 1=Test data
   //    -T 10000 : OBIS Testmode sample period in mSecs
   //
   while ((iOpt = getopt(argc, argv, "RM:Y:B:O:D:T:a:z:")) != -1)
   {
      switch (iOpt)
      {
         case 'B':
            LOG_printf("rpi-main():Option -B=%s" CRLF, optarg);
            iBuild = atoi(optarg);
            break;

         case 'M':
            LOG_printf("rpi-main():Option -M=%s" CRLF, optarg);
            iMonth = atoi(optarg);
            break;

         case 'Y':
            LOG_printf("rpi-main():Option -Y=%s" CRLF, optarg);
            iYear = atoi(optarg);
            break;

         case 'O':
            LOG_printf("rpi-main():Option -O=%s" CRLF, optarg);
            pstMap->G_iObisTest = atoi(optarg);
            break;

         case 'R':
            LOG_printf("rpi-main():Option -R" CRLF, optarg);
            rpi_RestoreActualData();
            GLOBAL_Close();
            _exit(0);
            break;

         case 'D':
            LOG_printf("rpi-main():Option -D=%s" CRLF, optarg);
            iDay = atoi(optarg);
            break;

         case 'T':
            LOG_printf("rpi-main():Option -T=%s" CRLF, optarg);
            pstMap->G_iObisTestTime = atoi(optarg);
            break;

         case 'a':
            LOG_printf("rpi-main():Option -a=%s" CRLF, optarg);
            iAddSpare = atoi(optarg);
            break;

         case 'z':
            LOG_printf("rpi-main():Option -z=%s" CRLF, optarg);
            iAddZero = atoi(optarg);
            break;

         default:
            LOG_printf("rpi-main(): Invalid option %c" CRLF, (char) iOpt);
            break;
      }
   }

   if(iDay && iMonth && iYear)
   {
      rpi_ShowObisData(iYear, iMonth, iDay);
      GLOBAL_Close();
      _exit(0);
   }

   if(iBuild && iYear)
   {
      rpi_RestoreMonthData(iYear, iBuild);
      GLOBAL_Close();
      _exit(0);
   }

   if(iMonth && iYear)
   {
      rpi_RepairMonthData(iYear, iMonth);
      GLOBAL_Close();
      _exit(0);
   }

   if( (iAddZero>0) || (iAddSpare>0) )
   {
      //
      // CL Option to expand the mmap file: do it and exit
      //
      GLOBAL_ExpandMap(iAddZero, iAddSpare);
      //
      LOG_printf("rpi-main(): Sizeof int       = %d" CRLF, sizeof(int));
      LOG_printf("rpi-main(): Sizeof long      = %d" CRLF, sizeof(long));
      LOG_printf("rpi-main(): Sizeof long long = %d" CRLF, sizeof(long long));
      LOG_printf("rpi-main(): Sizeof float     = %d" CRLF, sizeof(float));
      LOG_printf("rpi-main(): Sizeof double    = %d" CRLF, sizeof(double));
      LOG_printf("rpi-main(): Sizeof Day       = %d" CRLF, sizeof(pstMap->G_flThisDay));
      LOG_printf("rpi-main(): Sizeof Month     = %d" CRLF, sizeof(pstMap->G_flThisMonth));
      LOG_printf("rpi-main(): Sizeof EMTR      = %d" CRLF, sizeof(EMTR));
      //
      LOG_printf("rpi-main(): Buffer DispDay   = 0x%X" CRLF, offsetof(RPIMAP, G_flDispDay  ));
      LOG_printf("rpi-main(): Buffer DispMonth = 0x%X" CRLF, offsetof(RPIMAP, G_flDispMonth));
      LOG_printf("rpi-main(): Buffer LastMonth = 0x%X" CRLF, offsetof(RPIMAP, G_flLastMonth));
      LOG_printf("rpi-main(): Buffer ThisMonth = 0x%X" CRLF, offsetof(RPIMAP, G_flThisMonth));
      LOG_printf("rpi-main(): Buffer LastDay   = 0x%X" CRLF, offsetof(RPIMAP, G_flLastDay  ));
      LOG_printf("rpi-main(): Buffer ThisDay   = 0x%X" CRLF, offsetof(RPIMAP, G_flThisDay  ));
      //
      LOG_printf("rpi-main(): Done:" CRLF);
      //
      GLOBAL_Close();
      _exit(0);
   }
   //
   LOG_printf("rpi-main():%s:Build:%s" CRLF, VERSION, RPI_BUILT);
   #ifdef RPI_DAEMON_MODE
      rpi_Daemon();
      iGuardsSecs = GUARD_SECS;
   #else
      LOG_printf("rpi-main():Debug mode=NO background deamon !" CRLF);
      iGuardsSecs = GUARD_SECS_DEBUG;
   #endif
   //
   // Daemon mode (if not in debug build):
   //    the HOST has exited already and we are now running in the background
   //
   if( LOG_ReportOpenFile((char *) pcLogfile) == FALSE)
   {
      GEN_Printf("rpi-main(): Error opening log-file %s" CRLF, pcLogfile);
   }
   else
   {
      LOG_printf("rpi-main():Logfile is %s" CRLF, pcLogfile);
      LOG_Report(0, "SMA", "%s:Build:%s (%s)", VERSION, RPI_BUILT, RPI_IO_BOARD);
   }
   //
   if(rpi_SignalRegister(&tBlockset) == FALSE)
   {
      LOG_Report(errno, "SMA", "rpi-main():Exit Register ERROR:");
      GLOBAL_Close();
      _exit(1);
   }
   //
   if(sem_init(&pstMap->G_tSemHost, 1, 0) == -1) 
   {
      LOG_Report(errno, "SMA", "rpi-main():Exit sem_init ERROR:");
      GLOBAL_Close();
      _exit(1);
   }
   //
   // Init GPIO
   //
   GEN_Init();
   //
   // Start Smart-E-meter thread
   // Pass pointer to ping_pong buffer
   //
   GLOBAL_PidPut(PID_HOST, getpid());
   //
   pstMap->G_iGasPeriod = (int)strtoul(pstMap->G_pcGasPeriod, NULL, 10);
   pstMap->G_iGuards    = rpi_Startup();
   fRpiRunning          = rpi_WaitStartup(pstMap->G_iGuards);
   iGuardCount          = GUARD_COUNT;
   //
   // Get timestamp for each WAN network check
   //                        guard moment
   //
   ulSecsGuard          = RTC_GetSystemSecs();
   ulSecsWanCheck       = ulSecsGuard + (30*60);
   ulSecsGuard         += iGuardsSecs;
   //
   GLOBAL_PidClearGuards();
   GLOBAL_PidsLog();
   //
   pstEmtrTot = &(pstMap->G_stEmtrTot);
   pstEmtrNow = &(pstMap->G_stEmtrNow);
   pstEmtrQtr = &(pstMap->G_stEmtrQtr);
   pstEmtrDay = &(pstMap->G_stEmtrDay);
   pstEmtrMon = &(pstMap->G_stEmtrMon);
   pstEmtrCsv = &(pstMap->G_stEmtrCsv);
   //
   if(!fMapOkee)
   {
      pstEmtrTot->iPeriod = -1;              // No previous period
      pstEmtrNow->iPeriod = -1;              // No previous period
      pstEmtrMon->iPeriod = -1;              // No previous period
      pstEmtrDay->iPeriod = -1;              // No previous period
      pstEmtrQtr->iPeriod = -1;              // No previous period
      pstEmtrCsv->iPeriod = -1;              // No previous period
   }
   //
   pcCsvBuffer   = safemalloc(CSV_MAX);
   pcCsvTimeDate = safemalloc(CSV_MAX);
   //
   RPI_DAY("Init");
   //
   while(fRpiRunning)
   {
      //
      // OBIS dat should be coming in every 10 secs
      // Keep the GREEN Led ON as long as we keep receiving OBIS data
      //
      switch( GEN_WaitSemaphore(&pstMap->G_tSemHost, 15000) )
      {
         case 0:
            ulSecsNow = RTC_GetSystemSecs();
            //
            // Sem unlocked
            //
            // Wait until triggered by:
            //    o SIGSEGV: Segmentation fault 
            //    o SIGUSR1: COMMS buffer completed
            //    o SIGUSR2: Power Off
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_PWR_OFF)) 
            {
               LED(LED_R, 0);
               //
               PRINTF("rpi-PowerOff()" CRLF);
               LOG_Report(0, "SMA", "rpi-PowerOff()");
               //
               GEN_Sleep(500);
               fRpiPowerOff = TRUE;
               fRpiRunning  = FALSE;
            }
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_RPI_NFY)) break;
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_FLT_NFY)) break;
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_GRD_NFY)) rpi_HandleGuard();
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_PP0_NFY)) pcBuffer = pstMap->G_pcPing;
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_PP1_NFY)) pcBuffer = pstMap->G_pcPong;
            //
            if(pcBuffer)
            {
               PRINTF("rpi-main():Retrieve Obis data" CRLF);
               //
               // Keep activity LED ON as long as we have incoming OBIS data
               //
               iActivityLed = 1;
               //
               if(rpi_EmtrConvertObis(pcBuffer, pstEmtrTot, pstEmtrNow))
               {
                  //
                  // OBIS data has been acquired succesfully: handle sub-totals
                  //
                  rpi_EmtrVerifyCsvPeriod(ulSecsNow, pstEmtrTot);
                  //
                  rpi_EmtrSampleCsv(ulSecsNow, pstEmtrTot, pstEmtrCsv, pstEmtrNow);
                  rpi_EmtrSampleQtr(ulSecsNow, pstEmtrQtr, pstEmtrNow);
                  rpi_EmtrSampleDay(ulSecsNow, pstEmtrDay, pstEmtrNow);
                  rpi_EmtrSampleMon(ulSecsNow, pstEmtrMon, pstEmtrNow);
                  //
                  rpi_EmtrUpdateGlobals(pstEmtrTot, pstEmtrNow);
                  //
                  RPI_DISPLAY("TOT", pstEmtrTot);
                  RPI_DISPLAY("NOW", pstEmtrNow);
                  RPI_DISPLAY("QTR", pstEmtrQtr);
                  RPI_DISPLAY("DAY", pstEmtrDay);
                  RPI_DISPLAY("MON", pstEmtrMon);
               }
               pcBuffer = NULL;
            }
            break;

         default:
         case -1:
            ulSecsNow = RTC_GetSystemSecs();
            LOG_Report(errno, "RPI", "rpi-main():ERROR GEN_WaitSemaphore");
            PRINTF1("rpi-main():ERROR GEN_WaitSemaphore(errno=%d)" CRLF, errno);
            break;

         case 1:
            //
            // Timeout: No OBIS dat received
            // Timeout: Check guard at the end of the watch period (each GUARD_SECS secs)
            //
            ulSecsNow    = RTC_GetSystemSecs();
            iActivityLed = 0;
            //
            if(ulSecsNow > ulSecsGuard)
            {
               //
               // All active threads need to report back within GUARD_SECS secs.
               // If they didn't the service will be restarted by the WatchDog, 
               // who is triggered by PID_HOST through a SIGUSR1 (Restart) or 
               // SIGUSR2 (Reboot).
               //
               // G_stPidList[i]->pid_t       tPid   : The PID
               //                 int         iFlag  : The thread's guard marker
               //                 const char *pcName : The PID short-name
               //                 const char *pcHelp : The PID long-name
               //
               //
               // Check all threads if it has set its guard marker again
               // to signal it is still alive and kicking
               //
               if(GLOBAL_PidCheckGuards())
               {
                  //
                  // All threads reported back: Restart guards duty
                  //
                  iGuardCount  = GUARD_COUNT;
                  ulSecsGuard += iGuardsSecs;
               }
               else
               {
                  //
                  // Not all threads reported back: if multiple times in a row, take action
                  //
                  if(iGuardCount-- == 0)
                  {
                     //
                     // Problems: restart theApp
                     //
                     LOG_Report(0, "RPI", "rpi-main():Check Guard Error!");
                     PRINTF("rpi-main():Check Guard ERROR: Restart theApp !" CRLF);
                     GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_RPI_RST);
                     fRpiRunning = FALSE;
                  }
                  else
                  {
                     //
                     // Not all threads reported back
                     //
                     RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, cDateTime, ulSecsNow);
                     PRINTF2("rpi-main():Guard round found problem %d at %s" CRLF, GUARD_COUNT-iGuardCount, cDateTime);
                     LOG_Report(0, "RPI", "Guard round found problem %d at %s", GUARD_COUNT-iGuardCount, cDateTime);
                     ulSecsGuard += GUARD_SECS_RETRY;
                  }
               }
               GLOBAL_PidClearGuards();
            }
            if(ulSecsNow > ulSecsWanCheck)
            {
               //
               // Check if we still have a network connection
               //
               if(!rpi_GetWan())
               {
                  LOG_Report(errno, "RPI", "Not able to connect with WAN service");
               }
               ulSecsWanCheck += (30*60);
            }
            break;
      }
      LED(LED_G, iActivityLed);
   }
   PRINTF("rpi-main():Exit Obis data collection" CRLF);
   //
   // Always save what we have so far of the current day to file
   //
   rpi_EmtrSaveDay(pstEmtrDay->ulSecsSample, (u_int8 *)pstMap->G_flThisDay, NUM_SAMPLES_DAY * sizeof(double));
   //
   // Terminate Smart-E-meter thread
   //
   LOG_printf("rpi-main():Terminate all threads..." CRLF);
   //
   GLOBAL_PidsTerminate(SMART_E_TIMEOUT);
   //
   LOG_printf("rpi-main():...done" CRLF);
   LOG_Report(0, "SMA", "Version=%s now EXIT", VERSION);
   //
   if( sem_destroy(&pstMap->G_tSemHost) == -1) LOG_Report(errno, "SMA", "rpi-main():sem_destroy");
   //
   GEN_Printf("rpislim():Safe memory allocation=%d" CRLF, safemalloccount());
   safefree(pcCsvBuffer);
   safefree(pcCsvTimeDate);
   safeclose(tCsvFile);
   //
   // In CMD-Line mode, we do not need the Watchdog thread to check on us
   //
   if(fRpiDeamonMode) 
   {
      //
      // Check how to proceed from here:
      //    o Restart the App
      //    o Reboot the system
      //    o Just terminate the App   
      //
      if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_RPI_RST))
      {
         //
         // We need to restart the App: SIGUSR1 the watchdog
         //
         tPid = GEN_GetPidByName(RPI_WATCHDOG);
         if(tPid > 0) kill(tPid, SIGUSR1);
      }
      else if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_RPI_RBT))
      {
         //
         // We need to reboot the system: SIGUSR2 the watchdog
         //
         tPid = GEN_GetPidByName(RPI_WATCHDOG);
         if(tPid > 0) kill(tPid, SIGUSR2);
      }
   }
   else
   {
      if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_RPI_RST)) GEN_Printf("CmdLine-Mode:Skipping RESTART" CRLF);
      if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_RPI_RBT)) GEN_Printf("CmdLine-Mode:Skipping REBOOT" CRLF);
   }
   //
   GLOBAL_Sync();
   GLOBAL_Close();
   //==============================================================================================
   // From here on :
   // CANNOT Use functions which use MMAP anymore:
   // CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
   //==============================================================================================
   GEN_Printf("rpi-main():Exit normally" CRLF);
   LOG_ReportCloseFile();
   //
   // Check if we need to power down
   //
   if(fRpiPowerOff) 
   {
      //
      // Powerdown: turn Red and Green LEDs on to see when the system is really down 
      // (the LEDs will no longer be driven actively and will dim.
      //
      LED(LED_R, 1);
      LED(LED_G, 1);
      rpi_PowerOff();
   }
   else
   {
      //
      // Normal exit: turn OFF LEDs
      //
      GEN_Exit();
   }
   GEN_Printf("rpislim():_exit(0)" CRLF);
   //
   fflush(stderr);
   fflush(stdout);
   //
   _exit(0);
}

/* ======   Local Functions separator ===========================================
___LOCAL_FUNCTIONS(){}
==============================================================================*/

#ifndef  DEBUG_ASSERT
//
//  Function:   rpi_Daemon
//  Purpose:    Daemonize the watchdog
//
//  Parms:
//  Returns:
//
static pid_t rpi_Daemon(void)
{
   pid_t    tPid;

   fRpiDeamonMode = TRUE;
   tPid           = fork();
   //
   switch(tPid)
   {
      default:
         // Parent (Commandline or service) 
         LOG_Report(0, "SMA", "rpi-Daemon():Exit Parent, rpi_main now daemonized.");
         _exit(0);
         break;

      case 0:
         // child
         PRINTF("rpi-Daemon(): RpiSlim running background mode" CRLF);
         break;

      case -1:
         // Error
         LOG_Report(errno, "SMA", "rpi-Daemon():Exit fork ERROR:");
         _exit(1);
         break;
   }
   return(tPid);
}
#endif   //DEBUG_ASSERT

//
//  Function:   rpi_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool rpi_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &rpi_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "SMA", "rpi-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &rpi_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "SMA", "rpi-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &rpi_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "SMA", "rpi-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &rpi_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "SMA", "rpi-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &rpi_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "SMA", "rpi-ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
  }
  return(fCC);
}

//
//  Function:   rpi_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "SMA", "rpi-ReceiveSignalTerm()");
   //
   fRpiRunning = FALSE;
   rpi_PostSemaphore();
}

//
//  Function:   rpi_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "SMA", "rpi-ReceiveSignalInt()");
   //
   fRpiRunning = FALSE;
   rpi_PostSemaphore();
}

//
//  Function:   rpi_ReceiveSignalSegmnt
//  Purpose:    Add the SIGSEGV command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalSegmnt(int iSignal)
{
   if(fRpiRunning)
   {
      LOG_Report(errno, "SMA", "rpi-ReceiveSignalSegmnt()");
      rpi_PostSemaphore();
      fRpiRunning = FALSE;
   }
}

//
// Function:   rpi_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR1 is used to signal a buffer is ready
//
static void rpi_ReceiveSignalUser1(int iSignal)
{
   rpi_PostSemaphore();
}

//
// Function:   rpi_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR2 is used to handle the power-off button, send by the PID_SRVR daemon.
//
static void rpi_ReceiveSignalUser2(int iSignal)
{
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_PWR_OFF);
   LOG_Report(0, "SMA", "rpi-ReceiveSignalUser2()");
   rpi_PostSemaphore();
}

/* ======   Local Functions separator ===========================================
___MISC_FUNCTIONS(){}
==============================================================================*/


//
// Function:   rpi_AppendCsvSample
// Purpose:    Append a sample to the CSV buffer
//
// Parms:      ID, Emtr struct ptr
// Returns:    TRUE if OK
// Note:       
//              
static bool rpi_AppendCsvSample(OBISID tObis, EMTR *pstEmtr)
{
   bool  fCc=TRUE;
   int   iIdx, iMax;
   char  cSep=SEP, cDp=DEC_POINT;
   char *pcDest;

   iIdx = GEN_STRLEN(pcCsvBuffer);
   iMax = CSV_MAX - iIdx;
   //
   // Build CSV record:
   // Tx,YY,MM,DD,HH,MM,SS,Gas,+T1,-T1,+T2,-T2
   //
   if(iIdx) pcCsvBuffer[iIdx++] = cSep;
   //
   pcDest = &pcCsvBuffer[iIdx];
   //
   switch(tObis)
   {
      default:
         //
         // Make sure to terminate the record
         //
         pcCsvBuffer[iIdx] = '\0';
         fCc = FALSE;
         break;

      case OBIS_CUR_TARIF:
         //
         GEN_SNPRINTF(pcDest, iMax, "%d", pstEmtr->iCurrTariff);
         break;

      case OBIS_GAS:
         GEN_SNPRINTF(pcDest, iMax, "%5.3f", pstEmtr->flGasDelivered);
         if(cDp != '.') GEN_ReplaceChar(pcDest, '.', cDp);
         break;

      case OBIS_PWR_T1_P:
         GEN_SNPRINTF(pcDest, iMax, "%5.3f", pstEmtr->flPowerT1Delivered);
         if(cDp != '.') GEN_ReplaceChar(pcDest, '.', cDp);
         break;

      case OBIS_PWR_T2_P:
         GEN_SNPRINTF(pcDest, iMax, "%5.3f", pstEmtr->flPowerT2Delivered);
         if(cDp != '.') GEN_ReplaceChar(pcDest, '.', cDp);
         break;

      case OBIS_PWR_T1_M:
         GEN_SNPRINTF(pcDest, iMax, "%5.3f", pstEmtr->flPowerT1Received);
         if(cDp != '.') GEN_ReplaceChar(pcDest, '.', cDp);
         break;

      case OBIS_PWR_T2_M:
         GEN_SNPRINTF(pcDest, iMax, "%5.3f", pstEmtr->flPowerT2Received);
         if(cDp != '.') GEN_ReplaceChar(pcDest, '.', cDp);
         break;

      case OBIS_TIMEDATE:
         RTC_ConvertDateTime(TIME_FORMAT_CSV_YYYY_MM_DD_HH_MM_SS, pcCsvTimeDate, pstEmtr->ulSecsSample);
         GEN_SNPRINTF(pcDest, iMax, "%s", pcCsvTimeDate);
         if(cSep != ',') GEN_ReplaceChar(pcDest, ',', cSep);
         break;
   }
   return(fCc);
}

//
// Function:   rpi_CheckCrc16
// Purpose:    Check if the CRC16 of the buffer is OKee
//
// Parms:      Buffer ptr
// Returns:    TRUE if CRC16 OK
// Note:       CRC is a CRC16 value calculated over the preceding characters in the data message (from
//             �/� to �!� using the polynomial: x16+x15+x2+1). CRC16 uses no XOR in, no XOR out and is
//             computed with least significant bit first. The value is represented as 4 hexadecimal characters
//             (MSB first).
//              
static bool rpi_CheckCrc16(char *pcBuffer)
{
   bool     fCc=FALSE, fBusy=TRUE;
   int      iLen=0;
   u_int16  usCrc;
   u_int32  ulCrc;
   char    *pcStart=NULL;

   if(pcBuffer)
   {
      pcStart = pcBuffer;
      //
      // Determine record size
      //
      while(fBusy)
      {
         switch(*pcBuffer++)
         {
            case '!':
               //
               // EOR reached: check CRC
               //
               ulCrc = strtoul(pcBuffer, NULL, 16);
               usCrc = SLIM_CalculateCrc16((u_int8 *)pcStart, iLen+1);
               fCc = (usCrc == (u_int16)(ulCrc & 0xFFFF));
               //
               //if(!fCc) PRINTF2("rpi-CheckCrc16():Act CRC=%4X - Buffer says %4X" CRLF, usCrc, ulCrc);
               //
               fBusy = FALSE;
               break;
            
            default:
               iLen++;
               break;
         
            case '\0':
               PRINTF1("rpi-CheckCrc16():No end of record '!' found (%d bytes)" CRLF, iLen);
               fBusy = FALSE;
               break;
         }
      }
   }
   else
   {
      PRINTF("rpi-CheckCrc16():No start of record '/' found" CRLF);
   }
   return(fCc);
}

//
// Function:   rpi_PostSemaphore
// Purpose:    Unlock the watchdog Semaphore
//
// Parms:      
// Returns:    
// Note:       sem_post() increments (unlocks) the semaphore pointed to by sem. If the semaphore's 
//             value consequently becomes greater than zero, then another process or thread blocked
//             in a sem_wait(3) call will be woken up and proceed to lock the semaphore. 
//
static void rpi_PostSemaphore()
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //
   // sem++ (UNLOCK)   
   //
   if(sem_post(&pstMap->G_tSemHost) == -1) LOG_Report(errno, "SMA", "rpi-PostSemaphore(): sem_post error");
}

//
// Function:   rpi_WriteCsvRecord
// Purpose:    Write CSV record
//
// Parms:      Buffer, size
// Returns:    
// Note:       
//             
static void rpi_WriteCsvRecord(char *pcCsvBuffer, int iSize)
{
   int      iNr;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   if(tCsvFile == 0)
   {
      int      iLen;
      u_int32  ulSecs;
      char    *pcFile;
      char    *pcTemp;

      ulSecs = RTC_GetSystemSecs();
      //
      // Build sample CSV file "/WwwDir/Name_yyymmdd_hhmmss.csv"
      //
      iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS) + 10;
      iLen  += GEN_STRLEN(pstMap->G_pcWwwDir);
      iLen  += GEN_STRLEN(pcCsvFile);
      pcFile = safemalloc(iLen);
      pcTemp = safemalloc(iLen);
      //
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcTemp, ulSecs);
      PRINTF1("rpi-WriteCsvRecord():Date/Time=%s" CRLF, pcTemp);
      //
      GEN_SNPRINTF(pcFile, iLen, pcCsvFile, pstMap->G_pcWwwDir, pcTemp);
      PRINTF1("rpi-WriteCsvRecord():Open %s" CRLF, pcFile);
      //
      tCsvFile = safeopen2(pcFile, O_RDWR|O_CREAT|O_APPEND, (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH));
      if(tCsvFile > 0)  LOG_Report(0,     "SMA", "rpi-WriteCsvRecord():CSV-file %s Open OKee",  pcFile);
      else              LOG_Report(errno, "SMA", "rpi-WriteCsvRecord():CSV-file %s Open ERROR:", pcFile);
      //
      safefree(pcFile);
      safefree(pcTemp);
   }
   //
   if(tCsvFile > 0)
   {
      iNr = safewrite(tCsvFile, pcCsvBuffer, iSize);
      if(iNr == iSize) safewrite(tCsvFile, "\n", 1);
      else             
      {
         // Problems with CSV file: retry new file
         LOG_Report(errno, "SMA", "rpi-WriteCsvRecord():ERROR write csv file: retry !");
         safeclose(tCsvFile);
         tCsvFile = 0;
      }
   }
   else
   {
      PRINTF("rpi-WriteCsvRecord():ERROR write csv file" CRLF);
      LOG_Report(errno, "SMA", "rpi-WriteCsvRecord():ERROR write csv file");
   }
}

//
// Function:   rpi_PowerOff
// Purpose:    Turn OFF power
//
// Parms:      
// Returns:     Exit yes/no: FALSE=Exit
// Note:       
//             
static void rpi_PowerOff()
{
// const char *pcShell="shutdown -h -t 5 \"Power OFF in 5 mins\" ";
   const char *pcShell="poweroff";

   system(pcShell);
}

//
// Function:   rpi_GetWan
// Purpose:    Retrieve the WAN address
//
// Parms:      
// Returns:    TRUE if WAN retrieved
// Note:       This call will only check if the WAN network connection is OKee
//
static bool rpi_GetWan(void)
{
   bool        fFound;
   const char *pcName = "ipecho.net/plain";
   const char *pcAny  = "";

   if( (fFound = HTTP_GetFromNetwork((char *)pcName, (char *)pcAny, NULL, 0, 20)) )
   {
      PRINTF("rpi-GetWan():WAN IP-Address found" CRLF);
   }
   else
   {
      PRINTF("rpi-GetWan():WAN IP-address not resolved" CRLF);
   }
   return(fFound);
}

//
//  Function:  rpi_HandleGuard
//  Purpose:   Handle the guard notification
//
//  Parms:     
//  Returns:   
//  Note:      One or more threads did not respond to the generic guard duty. A signal
//             GLOBAL_GRD_RUN has been send and now replied with GLOBAL_GRD_NFY.
//
static bool rpi_HandleGuard()
{
   LOG_Report(0, "RPI", "rpi-HandleGuard():Notification received");
   return(TRUE);
}


/* ======   Local Functions separator ===========================================
___DEBUG_FUNCTIONS(){}
==============================================================================*/

#ifdef   DEBUG_ASSERT
//
// Function:   rpi_DisplaySmartFloats
// Purpose:    Display smart meter data
//
// Parms:      Title
// Returns:    
// Note:       
//
static void rpi_DisplaySmartFloats(const char *pcTitle)
{
   bool     fNew;
   int      iIdx;
   double   flLast, flThis;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   fNew = TRUE;
   for(iIdx=0; iIdx<MAX_QRTS; iIdx++)
   {
      flLast = pstMap->G_flLastDay[EMS_GAS][iIdx];
      flThis = pstMap->G_flThisDay[EMS_GAS][iIdx];
      //
      if( (flLast > (double) 0.0) || (flThis > (double) 0.0) )
      {
         #ifdef RPI_DISPLAY_FILE
            if(fNew) LOG_Report(0, "RPI", "rpi-DisplaySmartFloats(): GAS (%s)", pcTitle);
            LOG_Report(0, "RPI", "rpi-DisplaySmartFloats():%2d:Last=%5.3f This=%5.3f", iIdx, flLast, flThis);
         #else
            if(fNew) LOG_printf("rpi-DisplaySmartFloats(): GAS (%s)" CRLF, pcTitle);
            LOG_printf("rpi-DisplaySmartFloats():%2d:Last=%5.3f This=%5.3f" CRLF, iIdx, flLast, flThis);
         #endif
         fNew = FALSE;
      }
   }
   fNew = TRUE;
   for(iIdx=0; iIdx<MAX_QRTS; iIdx++)
   {
      flLast = pstMap->G_flLastDay[EMS_T1P][iIdx];
      flThis = pstMap->G_flThisDay[EMS_T1P][iIdx];
      //
      if( (flLast > (double) 0.0) || (flThis > (double) 0.0) )
      {
         #ifdef RPI_DISPLAY_FILE
            if(fNew) LOG_Report(0, "RPI", "rpi-DisplaySmartFloats(): T1P (%s)", pcTitle);
            LOG_Report(0, "RPI", "rpi-DisplaySmartFloats():%2d:Last=%5.3f This=%5.3f", iIdx, flLast, flThis);
         #else
            if(fNew) LOG_printf("rpi-DisplaySmartFloats(): T1P (%s)" CRLF, pcTitle);
            LOG_printf("rpi-DisplaySmartFloats():%2d:Last=%5.3f This=%5.3f" CRLF, iIdx, flLast, flThis);
         #endif
         fNew = FALSE;
      }
   }
   fNew = TRUE;
   for(iIdx=0; iIdx<MAX_QRTS; iIdx++)
   {
      flLast = pstMap->G_flLastDay[EMS_T2P][iIdx];
      flThis = pstMap->G_flThisDay[EMS_T2P][iIdx];
      //
      if( (flLast > (double) 0.0) || (flThis > (double) 0.0) )
      {
         #ifdef RPI_DISPLAY_FILE
            if(fNew) LOG_Report(0, "RPI", "rpi-DisplaySmartFloats(): T2P (%s)", pcTitle);
            LOG_Report(0, "RPI", "rpi-DisplaySmartFloats():%2d:Last=%5.3f This=%5.3f", iIdx, flLast, flThis);
         #else
            if(fNew) LOG_printf("rpi-DisplaySmartFloats(): T2P (%s)" CRLF, pcTitle);
            LOG_printf("rpi-DisplaySmartFloats():%2d:Last=%5.3f This=%5.3f" CRLF, iIdx, flLast, flThis);
         #endif
         fNew = FALSE;
      }
   }
}

//
// Function:   rpi_DisplaySmartMeter
// Purpose:    Display smart meter data
//
// Parms:      Hdr, EMTR struct ptr
// Returns:    
// Note:       Example output Smart-E-Meter:
//
//             /KFM5KAIFA-METER
//             
//             1-3:0.2.8(42)
//             0-0:1.0.0(171013150653S)
//             0-0:96.1.1(4530303235303030303639373538373136)
//             1-0:1.8.1(001852.792*kWh)
//             1-0:1.8.2(002550.969*kWh)
//             1-0:2.8.1(000000.000*kWh)
//             1-0:2.8.2(000000.000*kWh)
//             0-0:96.14.0(0002)
//             1-0:1.7.0(02.236*kW)
//             1-0:2.7.0(00.000*kW)
//             0-0:96.7.21(00002)
//             0-0:96.7.9(00002)
//             1-0:99.97.0(2)(0-0:96.7.19)(170116085159W)(0000000933*s)(000101000001W)(2147483647*s)
//             1-0:32.32.0(00000)
//             1-0:32.36.0(00000)
//             0-0:96.13.1()
//             0-0:96.13.0()
//             1-0:31.7.0(009*A)
//             1-0:21.7.0(02.235*kW)
//             1-0:22.7.0(00.000*kW)
//             0-1:24.1.0(003)
//             0-1:96.1.0(4730303139333430333037303635303136)
//             0-1:24.2.1(171013150000S)(02348.695*m3)
//             !6546
//
static void rpi_DisplaySmartMeter(const char *pcHdr, EMTR *pstEmtr)
{
#ifdef SMARTMETER_SHORT
//
// Total number of samples
// Csv   number of samples
// Total number of CRC errors
//
   // LOG_printf("%s:Tot=%d Csv=%d Crc=%d:" CRLF,
   //             pcHdr,
   //             pstEmtr->iTotSamples, pstEmtr->iCsvSamples, pstEmtr->iCrcErrors);

   LOG_printf("%s:%s - Period=%d Secs=%d GAS=%5.3f" CRLF, 
      pcHdr, pstEmtr->cEquipment, pstEmtr->iPeriod, pstEmtr->ulSecsSample, pstEmtr->flGasDelivered);

#else //SMARTMETER_SHORT
//
   LOG_printf("%s:Equipment %s:"                      CRLF, pcHdr, pstEmtr->cEquipment);
   LOG_printf("%s:Gas     Delivered    = %5.3f m3"    CRLF, pcHdr, pstEmtr->flGasDelivered);
   //
   LOG_printf("%s:Tariff  T1 or T2     = T%d"         CRLF, pcHdr, pstEmtr->iCurrTariff);
   LOG_printf("%s:Power   Delivered T1 = %5.3f kWh"   CRLF, pcHdr, pstEmtr->flPowerT1Delivered);
   LOG_printf("%s:Power   Received  T1 = %5.3f kWh"   CRLF, pcHdr, pstEmtr->flPowerT1Received);
   LOG_printf("%s:Power   Delivered T2 = %5.3f kWh"   CRLF, pcHdr, pstEmtr->flPowerT2Delivered);
   LOG_printf("%s:Power   Received  T2 = %5.3f kWh"   CRLF, pcHdr, pstEmtr->flPowerT2Received);
   //
   LOG_printf("%s:Power   Delivered    = %5.3f kW"    CRLF, pcHdr, pstEmtr->flPowerActualDelivered);
   LOG_printf("%s:Power   Received     = %5.3f kW"    CRLF, pcHdr, pstEmtr->flPowerActualReceived);
   //
   LOG_printf("%s:Power   Delivered L1 = %5.3f kW"    CRLF, pcHdr, pstEmtr->flPowerL1Delivered);
   LOG_printf("%s:Power   Received  L1 = %5.3f kW"    CRLF, pcHdr, pstEmtr->flPowerL1Received);     
   LOG_printf("%s:Current Actual    L1 = %5.3f A"     CRLF, pcHdr, pstEmtr->flCurrentL1);
   LOG_printf("%s:Voltage Actual    L1 = %5.3f V"     CRLF, pcHdr, pstEmtr->flVoltageL1);
   LOG_printf("%s:Voltage sags      L1 = %d"          CRLF, pcHdr, pstEmtr->iVoltageL1Sags);
   LOG_printf("%s:Voltage swells    L1 = %d"          CRLF, pcHdr, pstEmtr->iVoltageL1Swells);
   //
   LOG_printf("%s:Power   Delivered L2 = %5.3f kW"    CRLF, pcHdr, pstEmtr->flPowerL2Delivered);
   LOG_printf("%s:Power   Received  L2 = %5.3f kW"    CRLF, pcHdr, pstEmtr->flPowerL2Received);
   LOG_printf("%s:Current Actual    L2 = %5.3f A"     CRLF, pcHdr, pstEmtr->flCurrentL2);
   LOG_printf("%s:Voltage Actual    L2 = %5.3f V"     CRLF, pcHdr, pstEmtr->flVoltageL2);
   LOG_printf("%s:Voltage sags      L2 = %d"          CRLF, pcHdr, pstEmtr->iVoltageL2Sags);
   LOG_printf("%s:Voltage swells    L2 = %d"          CRLF, pcHdr, pstEmtr->iVoltageL2Swells);
   //
   LOG_printf("%s:Power   Delivered L3 = %5.3f kW"    CRLF, pcHdr, pstEmtr->flPowerL3Delivered);
   LOG_printf("%s:Power   Received  L3 = %5.3f kW"    CRLF, pcHdr, pstEmtr->flPowerL3Received);
   LOG_printf("%s:Current Actual    L3 = %5.3f A"     CRLF, pcHdr, pstEmtr->flCurrentL3);
   LOG_printf("%s:Voltage Actual    L3 = %5.3f V"     CRLF, pcHdr, pstEmtr->flVoltageL3);
   LOG_printf("%s:Voltage sags      L3 = %d"          CRLF, pcHdr, pstEmtr->iVoltageL3Sags);
   LOG_printf("%s:Voltage swells    L3 = %d"          CRLF, pcHdr, pstEmtr->iVoltageL3Swells);
   //
   LOG_printf("%s:Power fail Long      = %d"          CRLF, pcHdr, pstEmtr->iNumLongPowerFails);
   LOG_printf("%s:Power fail Short     = %d"          CRLF, pcHdr, pstEmtr->iNumShortPowerFails);
#endif //SMARTMETER_SHORT
}
#endif   //DEBUG_ASSERT

/* ======   Local Functions separator ===========================================
___EMTR_FUNCTIONS(){}
==============================================================================*/

//
// Function:   rpi_EmtrAdd
// Purpose:    Add applicable Emeter structure members
//
// Parms:      pstDest, pstSrc
// Returns:    
// Note:       
//              
static void rpi_EmtrAdd(EMTR *pstEmtrDst, EMTR *pstEmtrDlt)
{
   //
   // Add 
   //
   pstEmtrDst->flGasDelivered     += pstEmtrDlt->flGasDelivered; 
   pstEmtrDst->flPowerT1Delivered += pstEmtrDlt->flPowerT1Delivered; 
   pstEmtrDst->flPowerT1Received  += pstEmtrDlt->flPowerT1Received;  
   pstEmtrDst->flPowerT2Delivered += pstEmtrDlt->flPowerT2Delivered; 
   pstEmtrDst->flPowerT2Received  += pstEmtrDlt->flPowerT2Received;  
}

//
// Function:   rpi_EmtrConvertObis
// Purpose:    Convert OBIS buffer which contains the smart meter response
//
// Parms:      Buffer, EMTR totals, EMTR delta
// Returns:    TRUE if OKee
//             EMTR totals = New totals
//             EMTR delta  = Current period delta
// Note:       Buffer contains:
//
//             "/KFM5KAIFA-METER\r\n
//             \r\n
//             1-3:0.2.8(42)\r\n
//             0-0:1.0.0(171013150653S)\r\n
//             0-0:96.1.1(4530303235303030303639373538373136)\r\n
//             1-0:1.8.1(001852.792*kWh)\r\n
//             1-0:1.8.2(002550.969*kWh)\r\n
//             1-0:2.8.1(000000.000*kWh)\r\n
//             1-0:2.8.2(000000.000*kWh)\r\n
//             0-0:96.14.0(0002)\r\n
//             1-0:1.7.0(02.236*kW)\r\n
//             1-0:2.7.0(00.000*kW)\r\n
//             0-0:96.7.21(00002)\r\n
//             0-0:96.7.9(00002)\r\n
//             1-0:99.97.0(2)(0-0:96.7.19)(170116085159W)(0000000933*s)(000101000001W)(2147483647*s)\r\n
//             1-0:32.32.0(00000)\r\n
//             1-0:32.36.0(00000)\r\n
//             0-0:96.13.1()\r\n
//             0-0:96.13.0()\r\n
//             1-0:31.7.0(009*A)\r\n
//             1-0:21.7.0(02.235*kW)\r\n
//             1-0:22.7.0(00.000*kW)\r\n
//             0-1:24.1.0(003)\r\n
//             0-1:96.1.0(4730303139333430333037303635303136)\r\n
//             0-1:24.2.1(171013150000S)(02348.695*m3)\r\n
//             !6546\r\n"
//
static bool rpi_EmtrConvertObis(char *pcBuffer, EMTR *pstEmtrTot, EMTR *pstEmtrNow)
{
   bool     fCc=FALSE;
   int      iIdx=0;
   char    *pcCode=pcBuffer;
   EMTR     stEmtrTmp;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //PRINTF("rpi-EmtrConvertObis():Start" CRLF);
   //
   pstEmtrNow->iTotSamples++;
   GEN_SNPRINTF(pstMap->G_pcSmartTotals, MAX_PARM_LEN, "%d", pstEmtrNow->iTotSamples);
   //
   if(rpi_CheckCrc16(pcBuffer))
   {
      //
      // CRC Okee: Extract meter name
      //
      OBIS_LookupMeterName(pcBuffer, pstEmtrNow);
      //
      // Parse through the ping/pong buffer and fill result struct
      //
      do
      {
         if( (iIdx = OBIS_LookupCode(pcCode, pstEmtrNow)) > 0)
         {
            pcCode += iIdx;
         }
         else
         {
            //PRINTF("rpi-EmtrConvertObis():EOR" CRLF);   
         }
      }
      while(iIdx>0);
      //
      RPI_DISPLAY("OBIS", pstEmtrNow);
      //
      // Copy current delta's:
      // Gas,+T1,-T1,+T2,-T2
      //
      stEmtrTmp = *pstEmtrNow;
      if(pstEmtrNow->iPeriod == -1)
      {
         //
         // We just started sampling,  EmtrTot = EmtrNow
         //                            EmtrNow = 0
         //
         GEN_MEMSET(pstEmtrNow, 0, sizeof(EMTR));
      }
      else
      {
         //
         // Calculate delta's:         EmtrTot = Totals
         //                            EmtrNow = Delta's
         //
         pstEmtrNow->flGasDelivered     -= pstEmtrTot->flGasDelivered;
         pstEmtrNow->flPowerT1Delivered -= pstEmtrTot->flPowerT1Delivered;
         pstEmtrNow->flPowerT2Delivered -= pstEmtrTot->flPowerT2Delivered;
         pstEmtrNow->flPowerT1Received  -= pstEmtrTot->flPowerT1Received;
         pstEmtrNow->flPowerT2Received  -= pstEmtrTot->flPowerT2Received;
      }
      //
      // Preserve Totals period change !
      //
      pstEmtrNow->iPeriod = pstEmtrTot->iPeriod;
      stEmtrTmp.iPeriod   = pstEmtrTot->iPeriod;
      *pstEmtrTot         = stEmtrTmp;
      fCc = TRUE;
   }
   else
   {
      pstEmtrNow->iCrcErrors++;
      GEN_SNPRINTF(pstMap->G_pcSmartCrcs, MAX_PARM_LEN, "%d", pstEmtrNow->iCrcErrors);
      PRINTF("rpi-EmtrConvertObis():CRC error" CRLF);
      LOG_Report(0, "SMA", "rpi-EmtrConvertObis():CRC error");
   }
   //PRINTF("rpi-EmtrConvertObis():End" CRLF);
   return(fCc);
}

//
// Function:   rpi_EmtrSampleCsv
// Purpose:    Check if it is CSV-file sampling time
//
// Parms:      Secs now, EMTR total, EMTR CSV (deltas), EMTR now
// Returns:    
// Note:       Update EmtrCsv for next sample time
//
static void rpi_EmtrSampleCsv(u_int32 ulSecsNow, EMTR *pstEmtrTot, EMTR *pstEmtrCsv, EMTR *pstEmtrNow)
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //
   // Check if it is sampling time already
   //
   if(ulSecsNow >= pstEmtrCsv->ulSecsSample)
   {
      //PRINTF("rpi-EmtrSampleCsv()" CRLF);
      //
      // Sample time:
      // If initial measure: ignore delta's
      //
      if(pstEmtrCsv->ulSecsSample)
      {
         //
         // Build CSV record:
         // Tx,YYYY,MM,DD,HH,MM,SS,Gas,+T1,-T1,+T2,-T2
         //
         pcCsvBuffer[0] = '\0';
         rpi_AppendCsvSample(OBIS_CUR_TARIF, pstEmtrTot);
         rpi_AppendCsvSample(OBIS_TIMEDATE,  pstEmtrCsv);
         rpi_AppendCsvSample(OBIS_GAS,       pstEmtrTot);
         rpi_AppendCsvSample(OBIS_PWR_T1_P,  pstEmtrTot);
         rpi_AppendCsvSample(OBIS_PWR_T1_M,  pstEmtrTot);
         rpi_AppendCsvSample(OBIS_PWR_T2_P,  pstEmtrTot);
         rpi_AppendCsvSample(OBIS_PWR_T2_M,  pstEmtrTot);
         //
         // Copy current data, append delta's:
         // Gas,+T1,-T1,+T2,-T2
         //
         rpi_AppendCsvSample(OBIS_GAS,       pstEmtrCsv);
         rpi_AppendCsvSample(OBIS_PWR_T1_P,  pstEmtrCsv);
         rpi_AppendCsvSample(OBIS_PWR_T1_M,  pstEmtrCsv);
         rpi_AppendCsvSample(OBIS_PWR_T2_P,  pstEmtrCsv);
         rpi_AppendCsvSample(OBIS_PWR_T2_M,  pstEmtrCsv);
         //
         // Write results to CSV file and G_xxx
         //
         rpi_WriteCsvRecord(pcCsvBuffer, GEN_STRLEN(pcCsvBuffer));
         //
         pstEmtrTot->iCsvSamples++;
         GEN_SNPRINTF(pstMap->G_pcSmartCsvs, MAX_PARM_LEN, "%d", pstEmtrTot->iCsvSamples);
      }
      //
      // Setup next sampling time
      //
      *pstEmtrCsv = *pstEmtrNow;
      pstEmtrCsv->ulSecsSample = ulSecsNow + CSV_EMTR_SECS;
      RTC_ConvertDateTime(TIME_FORMAT_HH_MM_SS, pstMap->G_pcSmartTime, pstEmtrCsv->ulSecsSample);
   }
   else
   {
      //
      // Not yet time to write a CSV record with totals and deltas. We DO need to add all deltas !
      //
      rpi_EmtrAdd(pstEmtrCsv, pstEmtrNow);
   }
}

//
// Function:   rpi_EmtrSampleDay
// Purpose:    Check is sampling day is over
//
// Parms:      Secs now, EMTR today, EMTR now (delta's)
// Returns:    
// Note:       
//
static void rpi_EmtrSampleDay(u_int32 ulSecsNow, EMTR *pstEmtrDay, EMTR *pstEmtrNow)
{
   int      iThisDay, iLastDay;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //
   // RTC_GetDay() gets the day 1..31: make Index 0..30
   //
   iThisDay = RTC_GetDay(ulSecsNow) - 1;
   //
   // Check if sampling day is over
   //
   if(pstEmtrDay->iPeriod != iThisDay)
   {
      PRINTF1("rpi-EmtrSampleDay():This day=%d" CRLF, iThisDay);
      //
      // Sample time (if it's not the first one)
      //
      if(pstEmtrDay->iPeriod != -1)
      {
         iLastDay = pstEmtrDay->iPeriod;
         //
         // It's time to move to the next day, save previous day value's
         // Calculate the amount of energie for each type:
         //    o EMS_GAS:  m3 --> dm3
         //    o EMS_T1P: kWh --> Wh
         //    o EMS_T1M: kWh --> Wh
         //    o EMS_T2P: kWh --> Wh
         //    o EMS_T2M: kWh --> Wh
         //
         // Reset samples for the new day
         //
         pstMap->G_flThisMonth[EMS_GAS][iLastDay] = ((double) 1000 * pstEmtrDay->flGasDelivered);
         pstMap->G_flThisMonth[EMS_T1P][iLastDay] = ((double) 1000 * pstEmtrDay->flPowerT1Delivered);
         pstMap->G_flThisMonth[EMS_T1M][iLastDay] = ((double) 1000 * pstEmtrDay->flPowerT1Received);
         pstMap->G_flThisMonth[EMS_T2P][iLastDay] = ((double) 1000 * pstEmtrDay->flPowerT2Delivered);
         pstMap->G_flThisMonth[EMS_T2M][iLastDay] = ((double) 1000 * pstEmtrDay->flPowerT2Received);
         //
         // It's time to move to the next day 
         // Reset quarter samples for this day
         //
         GEN_MEMCPY(pstMap->G_flLastDay, pstMap->G_flThisDay, NUM_SAMPLES_DAY * sizeof(double));
         GEN_MEMSET(pstMap->G_flThisDay, 0x00, NUM_SAMPLES_DAY * sizeof(double));
         //
         // Save previous day to file yymmdd.bin
         //
         if( rpi_EmtrSaveDay(pstEmtrDay->ulSecsSample, (u_int8 *)pstMap->G_flLastDay, NUM_SAMPLES_DAY * sizeof(double)) )
         {
            RPI_DAY("Day saved");
            PRINTF1("rpi-EmtrSampleDay():Last day=%d saved." CRLF, iLastDay);
         }
         else
         {
            RPI_DAY("Day not yet saved");
            PRINTF1("rpi-EmtrSampleDay():Last day=%d NOT YET saved." CRLF, iLastDay);
         }
      }
      *pstEmtrDay = *pstEmtrNow;
      pstEmtrDay->iPeriod = iThisDay;
   }
   else
   {
      //
      // It's still the same day: SUM all delta's per day
      // Save the secs timestamp
      //
      rpi_EmtrAdd(pstEmtrDay, pstEmtrNow);
      pstEmtrDay->ulSecsSample = ulSecsNow;
   }
}  

//
// Function:   rpi_EmtrSampleMon
// Purpose:    Check is sampling month is over
//
// Parms:      Secs now, EMTR this month, EMTR now (delta's)
// Returns:    
// Note:       
//
static void rpi_EmtrSampleMon(u_int32 ulSecsNow, EMTR *pstEmtrMon, EMTR *pstEmtrNow)
{
   int      iThisMonth;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //
   // RTC_GetMonth() gets the month 1..12: make Index 0..11
   //
   iThisMonth = RTC_GetMonth(ulSecsNow) - 1;
   //
   // Check if sampling month is over
   //
   if(pstEmtrMon->iPeriod != iThisMonth)
   {
      if(pstEmtrMon->iPeriod != -1)
      {
         PRINTF1("rpi-EmtrSampleMon():This month=%d" CRLF, iThisMonth);
         //
         // It's time to move to the next month
         // Reset samples for this month
         //
         GEN_MEMCPY(pstMap->G_flLastMonth, pstMap->G_flThisMonth, NUM_SAMPLES_MONTH * sizeof(double));
         GEN_MEMSET(pstMap->G_flThisMonth, 0x00, NUM_SAMPLES_MONTH * sizeof(double));
         //
         // If we have a previous month, save it to file yymm.bin
         //
         if( rpi_EmtrSaveMonth(pstEmtrMon->ulSecsSample, (u_int8 *)pstMap->G_flLastMonth, NUM_SAMPLES_MONTH * sizeof(double)) )
         {
            PRINTF1("rpi-EmtrSampleMon():Last month=%d saved." CRLF, RTC_GetMonth(pstEmtrMon->ulSecsSample));
         }
         else
         {
            PRINTF1("rpi-EmtrSampleMon():Last month=%d NOT YET saved." CRLF, RTC_GetMonth(pstEmtrMon->ulSecsSample));
         }
      }
      *pstEmtrMon = *pstEmtrNow;
      //
      pstEmtrMon->iPeriod      = iThisMonth;
      pstEmtrMon->ulSecsSample = ulSecsNow;
   }
   else
   {
      //
      // It's still the same month: SUM all delta's per month
      // Save the secs timestamp
      //
      rpi_EmtrAdd(pstEmtrMon, pstEmtrNow);
      pstEmtrMon->ulSecsSample = ulSecsNow;
   }
}

//
// Function:   rpi_EmtrSampleQtr
// Purpose:    Check is sampling quarter hour is over
//
// Parms:      Secs now, EMTR quarter, EMTR now (delta's)
// Returns:    
// Note:       
//
static void rpi_EmtrSampleQtr(u_int32 ulSecsNow, EMTR *pstEmtrQtr, EMTR *pstEmtrNow)
{
   int      iCurQtr, iPrevQtr;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   iCurQtr = (int)RTC_GetCurrentQuarter(ulSecsNow);
   //
   // Check if sampling quarter is over
   //
   if(pstEmtrQtr->iPeriod != iCurQtr)
   {
      PRINTF1("rpi-EmtrSampleQtr():This quarter=%d" CRLF, iCurQtr);
      //
      // Sample time (if it's not the first one)
      //
      if(pstEmtrQtr->iPeriod != -1)
      {
         iPrevQtr = pstEmtrQtr->iPeriod;
         //
         // It's time to move to the next quarter hour
         // Calculate the amount of energie for each type:
         //    o EMS_GAS:  m3 --> dm3
         //    o EMS_T1P: kWh --> Wh
         //    o EMS_T1M: kWh --> Wh
         //    o EMS_T2P: kWh --> Wh
         //    o EMS_T2M: kWh --> Wh
         //
         // Reset samples for the new quarter
         //
         pstMap->G_flThisDay[EMS_GAS][iPrevQtr] = ((double) 1000 * pstEmtrQtr->flGasDelivered);
         pstMap->G_flThisDay[EMS_T1P][iPrevQtr] = ((double) 1000 * pstEmtrQtr->flPowerT1Delivered);
         pstMap->G_flThisDay[EMS_T1M][iPrevQtr] = ((double) 1000 * pstEmtrQtr->flPowerT1Received);
         pstMap->G_flThisDay[EMS_T2P][iPrevQtr] = ((double) 1000 * pstEmtrQtr->flPowerT2Delivered);
         pstMap->G_flThisDay[EMS_T2M][iPrevQtr] = ((double) 1000 * pstEmtrQtr->flPowerT2Received);
         //
         RPI_DAY("New Quarter");
         PRINTF4("rpi-EmtrSampleQtr(%2d):Gas=%5.3f  T1+=%5.3f  T2+=%5.3f" CRLF, 
                     iPrevQtr,
                     pstMap->G_flThisDay[EMS_GAS][iPrevQtr], 
                     pstMap->G_flThisDay[EMS_T1P][iPrevQtr], 
                     pstMap->G_flThisDay[EMS_T2P][iPrevQtr]);
      }
      else
      {
         RPI_DAY("SKIP Quarter");
      }
      *pstEmtrQtr = *pstEmtrNow;
      pstEmtrQtr->iPeriod = iCurQtr;
   }
   else
   {
      //
      // It's still the same quarter: SUM all delta's per quarter hour
      // Save the timestamp
      //
      rpi_EmtrAdd(pstEmtrQtr, pstEmtrNow);
      pstEmtrQtr->ulSecsSample = ulSecsNow;
   }
}

//
// Function:   rpi_EmtrSaveDay
// Purpose:    Save a day to file
//
// Parms:      Secs timestamp, dataptr
// Returns:
// Note:
//
static bool rpi_EmtrSaveDay(u_int32 ulSecs, u_int8 *pubData, int iSize)
{
   bool     fCc=FALSE;
   int      iLen, tFile;
   char    *pcFile;
   char    *pcTemp;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   if(ulSecs)
   {
      //
      // Build bin file "/WwwDir/SmartE_yyymmdd.bin"
      //
      iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM_DD);
      iLen  += 32;
      iLen  += GEN_STRLEN(pstMap->G_pcWwwDir);
      iLen  += GEN_STRLEN(pcFltFile);
      pcFile = safemalloc(iLen);
      pcTemp = safemalloc(iLen);
      //
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD, pcTemp, ulSecs);
      //
      GEN_SNPRINTF(pcFile, iLen, pcFltFile, pstMap->G_pcWwwDir, pcTemp);
      PRINTF1("rpi-EmtrSaveDay():Open %s" CRLF, pcFile);
      //
      tFile = safeopen2(pcFile, O_RDWR|O_CREAT, (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH));
      if(tFile > 0)
      {
         safewrite(tFile, (char *)pubData, iSize);
         LOG_Report(0, "SMA", "rpi-EmtrSaveDay():Day-file %s OKee",  pcFile);
         fCc = TRUE;
      }
      else
      {
         PRINTF("rpi-EmtrSaveDay():ERROR write day file" CRLF);
         LOG_Report(errno, "SMA", "rpi-EmtrSaveDay():ERROR write day file");
      }
      safefree(pcFile);
      safefree(pcTemp);
   }
   else
   {
      LOG_Report(0, "SMA", "rpi-EmtrSaveDay():Skip first initial day !");
   }
   return(fCc);
}

//
// Function:   rpi_EmtrSaveMonth
// Purpose:    Save a month to file
//
// Parms:      Secs timestamp, dataptr
// Returns:
// Note:
//
static bool rpi_EmtrSaveMonth(u_int32 ulSecs, u_int8 *pubData, int iSize)
{
   bool     fCc=FALSE;
   int      iLen, tFile;
   char    *pcFile;
   char    *pcTemp;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   if(ulSecs)
   {
      //
      // Build bin file "/WwwDir/SmartE_yyymm.bin"
      //
      iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM);
      iLen  += 32;
      iLen  += GEN_STRLEN(pstMap->G_pcWwwDir);
      iLen  += GEN_STRLEN(pcFltFile);
      pcFile = safemalloc(iLen);
      pcTemp = safemalloc(iLen);
      //
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM, pcTemp, ulSecs);
      //
      GEN_SNPRINTF(pcFile, iLen, pcFltFile, pstMap->G_pcWwwDir, pcTemp);
      PRINTF1("rpi-EmtrSaveMonth():Open %s" CRLF, pcFile);
      //
      tFile = safeopen2(pcFile, O_RDWR|O_CREAT, (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH));
      if(tFile > 0)
      {
         safewrite(tFile, (char *)pubData, iSize);
         LOG_Report(0, "SMA", "rpi-EmtrSaveMonth():Month-file %s OKee",  pcFile);
         fCc = TRUE;
      }
      else
      {
         PRINTF("rpi-EmtrSaveMonth():ERROR write month file" CRLF);
         LOG_Report(errno, "SMA", "rpi-EmtrSaveMonth():ERROR write month file");
      }
      safefree(pcFile);
      safefree(pcTemp);
   }
   else
   {
      LOG_Report(0, "SMA", "rpi-EmtrSaveMonth():Skip first initial month !");
   }
   return(fCc);
}

//
// Function:   rpi_EmtrUpdateGlobals
// Purpose:    Update actual globals
//
// Parms:      EMTR data totals, now
// Returns:
// Note:
//
static void rpi_EmtrUpdateGlobals(EMTR *pstEmtrTot, EMTR *pstEmtrNow)
{
   double   flTotal;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   GEN_SNPRINTF(pstMap->G_pcSmartTariff,        MAX_PARM_LEN, "%d", pstEmtrNow->iCurrTariff);
   GEN_SNPRINTF(pstMap->G_pcSmartEquipment,     MAX_PARM_LEN, "%s", pstEmtrNow->cEquipment);
   //
   //PRINTF1("rpi-EmtrUpdateGlobals():Equipment %s:" CRLF, pstMap->G_pcSmartEquipment);
   //
   // Sum all power data
   //
   flTotal  = pstEmtrNow->flPowerL1Delivered;
   flTotal += pstEmtrNow->flPowerL2Delivered;
   flTotal += pstEmtrNow->flPowerL3Delivered;
   flTotal -= pstEmtrNow->flPowerL1Received;
   flTotal -= pstEmtrNow->flPowerL2Received;
   flTotal -= pstEmtrNow->flPowerL3Received;
   GEN_SNPRINTF(pstMap->G_pcSmartPower, MAX_PARM_LEN, "%5.3f", flTotal);
   //
   // Sum all currents
   //
   flTotal  = pstEmtrNow->flCurrentL1;
   flTotal += pstEmtrNow->flCurrentL2;
   flTotal += pstEmtrNow->flCurrentL3;
   GEN_SNPRINTF(pstMap->G_pcSmartCurrent, MAX_PARM_LEN, "%5.3f", flTotal);
   //
   GEN_SNPRINTF(pstMap->G_pcGas,          MAX_PARM_LEN, "%5.3f", pstEmtrTot->flGasDelivered);
   GEN_SNPRINTF(pstMap->G_pcT1Plus,       MAX_PARM_LEN, "%5.3f", pstEmtrTot->flPowerT1Delivered);
   GEN_SNPRINTF(pstMap->G_pcT1Min,        MAX_PARM_LEN, "%5.3f", pstEmtrTot->flPowerT1Received);
   GEN_SNPRINTF(pstMap->G_pcT2Plus,       MAX_PARM_LEN, "%5.3f", pstEmtrTot->flPowerT2Delivered);
   GEN_SNPRINTF(pstMap->G_pcT2Min,        MAX_PARM_LEN, "%5.3f", pstEmtrTot->flPowerT2Received);
}

//
// Function:   rpi_EmtrVerifyCsvPeriod
// Purpose:    Verify if we need a new csv-file for the next period
//
// Parms:      SecsNow, EMTR totals
// Returns:    
// Note:       Sample period can be specified in config.h as
//                o FEATURE_SAMPLE_PER_YEAR
//                o FEATURE_SAMPLE_PER_MONTH
//                o FEATURE_SAMPLE_PER_DAY
//              
static void rpi_EmtrVerifyCsvPeriod(u_int32 ulSecsNow, EMTR *pstEmtrTot)
{
   int iPeriod=RTC_GETPERIOD(ulSecsNow);

   if(pstEmtrTot->iPeriod != iPeriod)
   {
      PRINTF2("rpi-EmtrVerifyCsvPeriod():Sample period %d->%d: start new CSV-file" CRLF, pstEmtrTot->iPeriod, iPeriod);
      //
      // This sample period is over: signal to build new CSV-File name 
      //
      pstEmtrTot->iPeriod      = iPeriod;
      pstEmtrTot->iCsvSamples  = 0;
      pstEmtrTot->ulSecsSample = ulSecsNow;
      //
      if(tCsvFile) safeclose(tCsvFile);
      tCsvFile = 0;
   }
}

//
//  Function:  rpi_Startup
//  Purpose:   Startup all threads
//
//  Parms:     
//  Returns:   Init markers
//
static int rpi_Startup(void)
{
   int   iStup;

   //
   // Start aux threads:
   //
   iStup  = SRVR_Init();
   iStup |= SLIM_Init();

   return(iStup);
}

//
//  Function:  rpi_WaitStartup
//  Purpose:   Wait until all threads are doing fine
//
//  Parms:     Initial flags of all threads which started
//  Returns:   True if all OK
//
static bool rpi_WaitStartup(int iInit)
{
   bool     fWaiting=TRUE;
   bool     fCc=FALSE;
   int      iStartup=0, iHalfSecs = 20;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //
   // Mask only INIT threads
   //
   iInit &= GLOBAL_XXX_INI;
   //
   while(fWaiting)
   {
      switch( GEN_WaitSemaphore(&pstMap->G_tSemHost, 500) )
      {
         case 0:
            // Thread signalled: verify completion
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_SVR_INI)) { LOG_Report(0, "RPI", "rpi-WaitStartup():SRVR OKee"); iStartup |= GLOBAL_SVR_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_COM_INI)) { LOG_Report(0, "RPI", "rpi-WaitStartup():COMM OKee"); iStartup |= GLOBAL_COM_INI;}
            //
            if(iStartup == iInit)
            {
               LOG_Report(0, "RPI", "rpi-WaitStartup():All inits OKee");
               fWaiting = FALSE;
               fCc      = TRUE;
            }
            else
            {
               PRINTF2("rpi-WaitStartup(): Waiting for 0x%08X (now 0x%08X)" CRLF, iInit, iStartup);
            }
            break;

         case -1:
            // Error
            fWaiting = FALSE;
            break;

         case 1:
            if(iHalfSecs-- == 0)
            {
               // Timeout: problems
               LOG_Report(0, "RPI", "rpi-WaitStartup(): Timeout: Init=0x%08X, Actual==0x%08X", iInit, iStartup);
               PRINTF2("rpi-WaitStartup(): Timeout: Init=0x%08X, Actual==0x%08X" CRLF, iInit, iStartup);
               fWaiting = FALSE;
            }
            break;
      }
   }
   return(fCc);
}

/* ======   Local Functions separator ===========================================
___CLI_FUNCTIONS(){}
==============================================================================*/

//
// Function:   rpi_RepairMonthData
// Purpose:    Repair a month data file by averaging holes of blank days
//
// Parms:      Year (2016...), month (1..12)
// Returns:    
// Note:       CLI: -Y 2017 -M 12
//
//             If month day data is lost due to normal exit before end of the month, 
//             the month file can be repaired by averaging empty days
//              
static void rpi_RepairMonthData(int iYear, int iMonth)
{
   bool     fDoRestore=TRUE;
   int      i, iDay, tEms, iDays, iDaye; 
   u_int32  ulSecs;
   double  *pflMon;
   double   flAvg;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   if(iYear  < 2016)                   fDoRestore = FALSE;
   if( (iMonth < 1) || (iMonth > 12) ) fDoRestore = FALSE;
   //
   if(fDoRestore)
   {
      //
      // Get secs for the year,month
      //
      ulSecs = RTC_GetMidnight(0);
      //
      while(1)
      {
         if( (iMonth == RTC_GetMonth(ulSecs)) && (iYear == RTC_GetYear(ulSecs)) )
         {
            if(RTC_GetDay(ulSecs) == 1) break;     // Got right day in secs since 1970
            else ulSecs -= (24*60*60);             // Else back 1 day
         }
         else ulSecs -= (28*24*60*60);             // back 4 weeks
      }
      //
      LOG_printf("rpi-RepairMonthData():Year=%d, Month=%d Day=%d (secs=%d)" CRLF, 
                  RTC_GetYear(ulSecs), RTC_GetMonth(ulSecs), RTC_GetDay(ulSecs), ulSecs);
      //
      // Read month from file
      //
      if(!RPI_RestoreMonth(ulSecs, (u_int8 *)pstMap->G_flDispMonth, NUM_SAMPLES_MONTH * sizeof(double)))
      {
         GEN_MEMSET((u_int8 *)pstMap->G_flDispMonth, 0x00, NUM_SAMPLES_MONTH * sizeof(double));
      }
      //
      // Month is open(restored partly from file or cleared) : Find holes and average them
      //
      for(tEms=0; tEms<NUM_EMS; tEms++)
      {
         pflMon = pstMap->G_flDispMonth[tEms];
         //
         // Look for consecutive days with no data
         //
         iDays = -1;
         iDaye = -1;
         //
         for(iDay=0; iDay<MAX_DAYS; iDay++)
         {
            if( (pflMon[iDay] <= (double)0.01) && (iDays == -1) )                  iDays = iDay;
            if( (pflMon[iDay] >  (double)0.01) && (iDays != -1) && (iDaye == -1) ) iDaye = iDay;
            if( (iDays != -1) && (iDaye != -1) )
            {
               //
               // Found gap: fill with avarage
               //
               flAvg = pflMon[iDaye]/(double)(iDaye-iDays+1);
               LOG_printf("rpi-RepairMonthData(%d):Hole=%d-%d: Ofl=%5.3f, Avg=%5.3f" CRLF, tEms, iDays+1, iDaye+1, pflMon[iDaye], flAvg);
               for(i=iDays; i<=iDaye; i++) 
               {
                  pflMon[i] = flAvg;
               }
               iDays = -1;
               iDaye = -1;
            }
         }
      }
      //
      // Save repaired data to Month file
      //
      if( rpi_EmtrSaveMonth(ulSecs, (u_int8 *)pstMap->G_flDispMonth, NUM_SAMPLES_MONTH * sizeof(double)) )
      {
         LOG_printf("rpi-RepairMonthData():Reparing Year(%d)-Month(%d) OKee" CRLF, iYear, iMonth);
      }
      else
      {
         LOG_printf("rpi-RepairMonthData():ERROR Reparing Year(%d)-Month(%d)" CRLF, iYear, iMonth);
      }
      //
      // Show G_flDispMonth data
      // 
      rpi_RestoreActualData();
      rpi_ShowMonth();
   }
   else
   {
      LOG_printf("rpi-RepairMonthData():Year(%d)-Month(%d) need to be 2016-1..12 !" CRLF, iYear, iMonth);
   }
}

//
// Function:   rpi_RestoreActualData
// Purpose:    Restore map data from month/day files
//
// Parms:      
// Returns:    
// Note:       CLI: -R : Restore MAP G_flThis* data from file and exit().
//
//             If map file data is lost, the month and day data could be restored from file.
//             Month : SmartE_201712.bin     size=[NUM_EMS][MAX_DAYS];
//             Day   : SmartE_20171214.bin   size=[NUM_EMS][MAX_QRTS];
//              
static void rpi_RestoreActualData(void)
{
   u_int32  ulSecs;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //
   // Get secs for the month,day
   //
   ulSecs = RTC_GetSystemSecs();
   //
   if( rpi_RestoreMapData(ulSecs) )
   {
      // All went well: copy data
      GEN_MEMCPY((u_int8 *)pstMap->G_flThisMonth, (u_int8 *)pstMap->G_flDispMonth, NUM_SAMPLES_MONTH * sizeof(double));
      GEN_MEMCPY((u_int8 *)pstMap->G_flThisDay,   (u_int8 *)pstMap->G_flDispDay,   NUM_SAMPLES_DAY   * sizeof(double));
   }
   else
   {
      LOG_printf("rpi-RestoreActualData():Restoring NOT successfull" CRLF);
   }
}

//
// Function:   rpi_RestoreMapData
// Purpose:    Restore map data from month/day files
//
// Parms:      ulSecs of the day/month
// Returns:    TRUE if OKee and the data is in the Month/Day display data
// Note:       Month : SmartE_201712.bin     size=[NUM_EMS][MAX_DAYS];
//             Day   : SmartE_20171214.bin   size=[NUM_EMS][MAX_QRTS];
//              
static bool rpi_RestoreMapData(u_int32 ulSecs)
{
   bool     fCc=TRUE;
   int      iMonth, iDay;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   iMonth = RTC_GetMonth(ulSecs);
   iDay   = RTC_GetDay(ulSecs);
   //
   // Restore this month
   //
   if(RPI_RestoreMonth(ulSecs, (u_int8 *)pstMap->G_flDispMonth, NUM_SAMPLES_MONTH * sizeof(double)))
   {
      LOG_printf("rpi-RestoreMapData():Restoring Month(%d) OKee" CRLF, iMonth);
   }
   else
   {
      // Not found: clear data to be sure
      LOG_printf("rpi-RestoreMapData():Restoring Month(%d) NOT successfull" CRLF, iMonth);
      GEN_MEMSET((u_int8 *)pstMap->G_flDispMonth, 0x00, NUM_SAMPLES_MONTH * sizeof(double));
      fCc = FALSE;
   }
   //
   // Restore today
   //
   if(RPI_RestoreDay(ulSecs, (u_int8 *)pstMap->G_flDispDay, NUM_SAMPLES_DAY * sizeof(double)))
   {
      LOG_printf("rpi-RestoreMapData():Restoring Day(%d) OKee" CRLF, iDay);
   }
   else
   {
      // Not found: clear data to be sure
      LOG_printf("rpi-RestoreMapData():Restoring Day(%d) NOT successfull" CRLF, iDay);
      GEN_MEMSET((u_int8 *)pstMap->G_flDispDay, 0x00, NUM_SAMPLES_DAY * sizeof(double));
      fCc = FALSE;
   }
   rpi_ShowMonth();
   return(fCc);
}

//
// Function:   rpi_RestoreMonthData
// Purpose:    Restore a month data from day files
//
// Parms:      Year (2016...), month (1..12)
// Returns:    
// Note:       CLI: -Y 2017 -B 12
//
//             If month file data is lost due to normal exit before end of the month, 
//             the month file can be restored using the day-file data.
//             Month : SmartE_201711.bin     size=[NUM_EMS][MAX_DAYS];
//             Day   : SmartE_20171103.bin   size=[NUM_EMS][MAX_QRTS];
//              
static void rpi_RestoreMonthData(int iYear, int iMonth)
{
   bool     fDoRestore=TRUE;
   int      iQtr, iDay, tEms; 
   u_int32  ulSecs, ulSecsDay;
   double  *pflMon;
   double  *pflDay;
   double   flTotal;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   if(iYear  < 2016)                   fDoRestore = FALSE;
   if( (iMonth < 1) || (iMonth > 12) ) fDoRestore = FALSE;
   //
   if(fDoRestore)
   {
      //
      // Get secs for today
      //
      ulSecs = RTC_GetMidnight(0);
      //
      while(1)
      {
         if( (iMonth == RTC_GetMonth(ulSecs)) && (iYear == RTC_GetYear(ulSecs)) )
         {
            if(RTC_GetDay(ulSecs) == 1) break;     // Got right day in secs since 1970
            else ulSecs -= (24*60*60);             // Else back 1 day
         }
         else ulSecs -= (28*24*60*60);             // back 4 weeks
      }
      //
      PRINTF4("rpi-RestoreMonthData():Year=%d, Month=%d Day=%d (secs=%d)" CRLF, 
                  RTC_GetYear(ulSecs), RTC_GetMonth(ulSecs), RTC_GetDay(ulSecs), ulSecs);
      //
      // Read month
      //
      if(!RPI_RestoreMonth(ulSecs, (u_int8 *)pstMap->G_flDispMonth, NUM_SAMPLES_MONTH * sizeof(double)))
      {
         GEN_MEMSET((u_int8 *)pstMap->G_flDispMonth, 0x00, NUM_SAMPLES_MONTH * sizeof(double));
      }
      //
      // Month is open(restored partly from file or cleared) : augment day-file data
      //
      ulSecsDay = ulSecs;
      //
      for(iDay=0; iDay<MAX_DAYS; iDay++)
      {
         //
         // Check if we are still withing the same month
         //
         if(iMonth == RTC_GetMonth(ulSecsDay))
         {
            if(RPI_RestoreDay(ulSecsDay, (u_int8 *)pstMap->G_flDispDay, NUM_SAMPLES_DAY * sizeof(double)))
            {
               //
               // Day is on file: Average each stuff (Gas, T1P,..) and store in in month
               //
               for(tEms=0; tEms<NUM_EMS; tEms++)
               {
                  pflMon  = pstMap->G_flDispMonth[tEms];
                  pflDay  = pstMap->G_flDispDay[tEms];
                  flTotal = 0.0;
                  //
                  // Sum the day totals for this energy
                  //
                  for(iQtr=0; iQtr<MAX_QRTS; iQtr++) flTotal += pflDay[iQtr];
                  //
                  if(pflMon[iDay] <= (double)0.01) 
                  {  
                     if(flTotal > 0)
                     {   
                        LOG_printf("rpi-RestoreMonthData():Ems=%s-Day=%d data = bad (%5.3f): Assume day totals (%5.3f) are better!" CRLF, pcEms[tEms], iDay+1, pflMon[iDay], flTotal);
                        pflMon[iDay] = flTotal;
                     }
                     else
                     {
                        LOG_printf("rpi-RestoreMonthData():Ems=%s-Day=%d Month and day have no valid data (Day-total = %5.3f)" CRLF, pcEms[tEms], iDay+1, flTotal);
                     }
                  }
                  else
                  {
                     if(pflMon[iDay] == flTotal)   
                     {
                        LOG_printf("rpi-RestoreMonthData():Ems=%s-Day=%d was already OK" CRLF, pcEms[tEms], iDay+1);
                     }
                     else
                     {
                        if(flTotal > 0)
                        {   
                           LOG_printf("rpi-RestoreMonthData():Ems=%s-Day=%d was different: M=%5.3f D=%5.3f" CRLF, pcEms[tEms], iDay+1, pflMon[iDay], flTotal);
                           pflMon[iDay] = flTotal;
                        }
                        else
                        {
                           LOG_printf("rpi-RestoreMonthData():Ems=%s-Day=%d has no valid data (Day-total = %5.3f)" CRLF, pcEms[tEms], iDay+1, flTotal);
                        }
                     }
                  }
                  LOG_printf("rpi-RestoreMonthData():Ems=%s-Day=%d Now=%5.3f)" CRLF, pcEms[tEms], iDay+1, pflMon[iDay]);
               }
            }
         }
         //
         // Next day
         //
         ulSecsDay += (24*60*60);
      }
      if( rpi_EmtrSaveMonth(ulSecs, (u_int8 *)pstMap->G_flDispMonth, NUM_SAMPLES_MONTH * sizeof(double)) )
      {
         LOG_printf("rpi-RestoreMonthData():Restoring Year(%d)-Month(%d) OKee" CRLF, RTC_GetYear(ulSecs), RTC_GetMonth(ulSecs) );
      }
      else
      {
         LOG_printf("rpi-RestoreMonthData():ERROR restoring Year(%d)-Month(%d)" CRLF, RTC_GetYear(ulSecs), RTC_GetMonth(ulSecs) );
      }
      rpi_RestoreActualData();
   }
   else
   {
      LOG_printf("rpi-RestoreMonthData():Year(%d)-Month(%d) need to be 2016-1..12 !" CRLF, iYear, iMonth);
   }
}

//
// Function:   rpi_ShowDay
// Purpose:    Show whole Day
//
// Parms:      
// Returns:    
// Note:       
//              
static void rpi_ShowDay()
{
   int      iQtr;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //
   // Show all data
   // Day:
   for(iQtr=0; iQtr<MAX_QRTS; iQtr++)
   {
      LOG_printf("Qtr[%02d]:Gas=%5.1f  T1P=%5.1f  T1M=%5.1f  T2P=%5.1f  T2M=%5.1f" CRLF, 
                  iQtr+1,
                  pstMap->G_flDispDay[EMS_GAS][iQtr],
                  pstMap->G_flDispDay[EMS_T1P][iQtr],
                  pstMap->G_flDispDay[EMS_T1M][iQtr],
                  pstMap->G_flDispDay[EMS_T2P][iQtr],
                  pstMap->G_flDispDay[EMS_T2M][iQtr]);
   }
}

//
// Function:   rpi_ShowMonth
// Purpose:    Show whole month
//
// Parms:      Year (2016...), month (1..12), Day (1..31)
// Returns:    
// Note:       
//              
static void rpi_ShowMonth()
{
   int      iDay;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //
   // Show all data
   // Month:
   for(iDay=0; iDay<MAX_DAYS; iDay++)
   {
      LOG_printf("Day[%02d]:Gas=%5.1f  T1P=%5.1f  T1M=%5.1f  T2P=%5.1f  T2M=%5.1f" CRLF, 
                  iDay+1,
                  pstMap->G_flDispMonth[EMS_GAS][iDay],
                  pstMap->G_flDispMonth[EMS_T1P][iDay],
                  pstMap->G_flDispMonth[EMS_T1M][iDay],
                  pstMap->G_flDispMonth[EMS_T2P][iDay],
                  pstMap->G_flDispMonth[EMS_T2M][iDay]);
   }
}

//
// Function:   rpi_ShowObisData
// Purpose:    Show the OBIS data from file/mapfile
//
// Parms:      Year (2016...), month (1..12), Day (1..31)
// Returns:    
// Note:       Show MAP data if today,else from file
//             Month : SmartE_201711.bin     size=[NUM_EMS][MAX_DAYS];
//             Day   : SmartE_20171103.bin   size=[NUM_EMS][MAX_QRTS];
//              
static void rpi_ShowObisData(int iDispYear, int iDispMonth, int iDispDay)
{
   int      iYear, iMonth, iDay;
   u_int32  ulSecs;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //
   // Get secs for the month,day
   //
   ulSecs = RTC_GetSystemSecs();
   //
   iYear  = RTC_GetYear(ulSecs);
   iMonth = RTC_GetMonth(ulSecs);
   iDay   = RTC_GetDay(ulSecs);
   //
   if( (iYear == iDispYear) && (iMonth == iDispMonth) && (iDay == iDispDay) )
   {
      // This day: copy MAP data 
      GEN_MEMCPY((u_int8 *)pstMap->G_flDispMonth, (u_int8 *)pstMap->G_flThisMonth, NUM_SAMPLES_MONTH * sizeof(double));
      GEN_MEMCPY((u_int8 *)pstMap->G_flDispDay,   (u_int8 *)pstMap->G_flThisDay,   NUM_SAMPLES_DAY   * sizeof(double));
   }
   else
   {
      ulSecs = RTC_GetSecs(iDispYear, iDispMonth, iDispDay);
      if(!rpi_RestoreMapData(ulSecs) )
      {
         LOG_printf("rpi-ShowObisData():Restoring %04d-%02d-%02d NOT successfull" CRLF, iDispYear, iDispMonth, iDispDay);
         return;
      }
   }
   //
   // Display current Month and Day data
   //
   rpi_ShowMonth();
   rpi_ShowDay();
}

