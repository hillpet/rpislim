#!/usr/bin/env bash

#################################################################################
# rebuildslim
#	Simple rebuild and install script
#
#	Copyright (c) 2017 Patrn.nl
#################################################################################
# This file is part of rpislim:
#
# RpiSlim is a simple Smart-E-Meter monitor which periodically reads the KAIFA
# smart-meter (kWh and m3) and stores each record as a CSV-file. The utility
# shares the RPi#9 with the OpenVPN server, which runs in a RAMDISK RootFS, and 
# the RpiSlim changes will vanish on any powerfail and/or reboot.
#
# This script will rebuild and restart the RpiSlim files and service in this case.
# To rebuild all RpiSlim files, run:
#
# $ rpi-mount
# 	 connect to //Vulture/Raspberry
# $	mkdir ~/proj
# $	cd ~/proj
# $	mkdir common
# $	mkdir rpislim
# $	cd rpislim
# $	mkdir bin
# $ mc 
# 	 copy from //Vulture/Raspberry/softw/common:
# 		makefile
# 	 copy from //Vulture/Raspberry/softw/rpislim:
# 		makefile
# $	cd ~
# $	./rebuild all
#
#################################################################################

check_make_ok() {
  if [ $? != 0 ]; then
    echo ""
    echo "Make Failed..."
    echo "Please check the messages and fix any problems."
    echo ""
#	exit 1
  fi
}

if [ x$1 = "xdirs" ]; then
	if ! test -d proj
	then
	  mkdir proj
	  mkdir proj/common
	  mkdir proj/rpislim
	  mkdir proj/rpislim/bin
#		exit 1
	fi
fi

if [ x$1 = "xinstall" ]; then
  echo
  echo "RpiSlim install"
  echo "======================="
  cd proj/rpislim
  sudo cp bin/rpislim /usr/sbin/rpislim
  check_make_ok
  sudo cp rpislim.service /lib/systemd/system/rpislim.service
  check_make_ok
  sudo systemctl enable rpislim.service
  check_make_ok
  sudo systemctl start rpislim.service
  check_make_ok
  echo "Rebuild done."
  echo ""
#	exit 1
fi

if [ x$1 = "xall" ]; then
  echo
  echo "RpiSlim Re-Build script"
  echo "======================="
  echo
  echo "Rebuild Common Library"
  cd proj/common
  make sync
  make clean
  make debug
  check_make_ok
  make clean
  make all
  check_make_ok
  echo "Rebuild RpiSlim"
  cd ../rpislim
  make sync
  make clean
  make debug
  check_make_ok
  make clean
  make all
  check_make_ok
  echo "Rebuild done."
  echo ""
#	exit 1
fi

if [ x$1 = "xcommon" ]; then
  echo
  echo "common Re-Build script"
  echo "======================"
  echo
  echo "Rebuild Common Library"
  cd proj/common
  make sync
  make clean
  make debug
  check_make_ok
  make clean
  make all
  check_make_ok
  cd ..
  echo "Rebuild done."
  echo ""
#	exit 1
fi

if [ x$1 = "x" ]; then
	echo ""
	echo "Rpi Smart-E-meter rebuilder. Use to create all necessary files on a new or modified"
	echo "installation. Usage: $0 [all | dirs | common | install]"
	echo ""
fi
