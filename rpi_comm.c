/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           rpi_comm.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Reading data from a Smart-E-meter
 *                      Smart-E-meter  :  KAIFA MA105C
 *                      Settings       :  115200 baud, 8 bits, no parity
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       13 Okt 2017
 *
 *  Note:               Raspberry Pi has several ways to access the serial port. For each type this differs:
 *                      
 *                      RPI-1: /dev/ttyAMA0 is the first UART
 *                      RPI-2: /dev/ttyAMA0 is the first UART
 *                      RPI-3: /dev/ttyAMA0 is the Bluetooth device (chosen because of the high bandwidth)
 *                             /dev/ttyS0   is the first UART
 *
 *                      To overcome incompatibilities, raspbian (from 20160510) has two aliases for this:
 *
 *                            /dev/serial0 is ttyS0   (UART0)
 *                            /dev/serial1 is ttyAMA0 (Bluetooth)
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sched.h>
#include <errno.h>
#include <time.h>
#include <termios.h>
#include <unistd.h>
//
#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_comm.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
static pid_t   slim_Daemon                (void);
static int     slim_Init                  (void);
static bool    slim_SignalRegister        (sigset_t *);
static void    slim_ReceiveSignalInt      (int);
static void    slim_ReceiveSignalTerm     (int);
static void    slim_ReceiveSignalUser1    (int);
static void    slim_ReceiveSignalUser2    (int);
static void    slim_ReceiveSignalSegmnt   (int);
static void    slim_HandleMeter           (int);
//
static bool    fSlimRunning=TRUE;
//
// OBIS test data
//
static const char *pcObisTestData = 
                     "/KFM5KAIFA-TEST-METER" CRLF
                     "" CRLF
                     "1-3:0.2.8(42)" CRLF
                     "0-0:1.0.0(171013150653S)" CRLF
                     "0-0:96.1.1(4530303235303030303639373538373136)" CRLF
                     "1-0:1.8.1(%6.3f*kWh)" CRLF
                     "1-0:1.8.2(%6.3f*kWh)" CRLF
                     "1-0:2.8.1(%6.3f*kWh)" CRLF
                     "1-0:2.8.2(%6.3f*kWh)" CRLF
                     "0-0:96.14.0(0002)" CRLF
                     "1-0:1.7.0(02.000*kW)" CRLF
                     "1-0:2.7.0(01.000*kW)" CRLF
                     "0-0:96.7.21(00002)" CRLF
                     "0-0:96.7.9(%05d)" CRLF
                     "1-0:99.97.0(2)(0-0:96.7.19)(170116085159W)(0000000933*s)(000101000001W)(2147483647*s)" CRLF
                     "1-0:32.32.0(00000)" CRLF
                     "1-0:32.36.0(00000)" CRLF
                     "0-0:96.13.1()" CRLF
                     "0-0:96.13.0()" CRLF
                     "1-0:31.7.0(009*A)" CRLF
                     "1-0:21.7.0(02.235*kW)" CRLF
                     "1-0:22.7.0(00.000*kW)" CRLF
                     "0-1:24.1.0(003)" CRLF
                     "0-1:96.1.0(4730303139333430333037303635303136)" CRLF
                     "0-1:24.2.1(171013150000S)(%5.3f*m3)" CRLF
                     "!";
//
// Default values format string
//
static double  flT1P                   = 0.0;
static double  flT2P                   = 0.0;
static double  flT1M                   = 0.0;
static double  flT2M                   = 0.0;
static int     iPeriod                 = 0;  
static double  flGas                   = 0.0;
//
static int     iObisTest               = 0;
static u_int16 usCrc                   = 0;
//
//                                         0  0  0  0  0  0  0  0  0  0  1  1  1  1  1  1  1  1  1  1  2  2  2  2
//                                         0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5  6  7  8  9  0  1  2  3
static const    int iPeriodTarifs[24]  = { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1 };
//
static const double flDefaultT1P       = 1500.0;         // 1.8.1
static const double flDefaultT2P       = 2500.0;         // 1.8.2
static const double flDefaultT1M       = 500.0;          // 2.8.1
static const double flDefaultT2M       = 1000.0;         // 2.8.2
static const double flDefaultGas       = 3000.0;         // 24.2.1

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   SLIM_Init
// Purpose:    Init the smart E meter thread
// 
// Parameters: 
// Returns:    Startup code XXX_INI
// Note:       The serial port thread will send out SIGUSRx on buffer completion:
//             SIGUSR1: Buffer is complete (ping or pong)
//             SIGUSR2:
//  
int SLIM_Init()
{
   pid_t tPid;
   
   //
   // Start the serial thread
   //
   tPid = slim_Daemon();
   GLOBAL_PidPut(PID_COMM, tPid);
   return(GLOBAL_COM_INI);
}

// 
// Function:   SLIM_CalculateCrc16
// Purpose:    Compute the CRC-16 for the data buffer
//             x^16 + x^15 + x^2 + 1
// 
// Parms:      Data pointer, number of bytes in the buffer
//
// Returns:    The CRC value.
// Note:       CRC is a CRC16 value calculated over the preceding characters in the data message (from
//             �/� to �!� using the polynomial: x16+x15+x2+1). CRC16 uses no XOR in, no XOR out and is
//             computed with least significant bit first. The value is represented as 4 hexadecimal characters
//             (MSB first).
//
//             Implementation kindly used from the Arduino project 
//             from jantenhove/P1-Meter-ESP8266
// 
u_int16 SLIM_CalculateCrc16(const u_int8 *pubBuffer, int iLen)
{
   int      x, i;
   u_int16  usCrc=0x0000;

	for (x=0; x<iLen; x++)
	{
      //
		// XOR byte into least sig. byte of crc
      //
		usCrc ^= (u_int16)pubBuffer[x];     
      //
		for (i=8; i!=0; i--) 
		{    
		   // Loop over each bit
			if ((usCrc & 0x0001) != 0) 
			{      
			   // If the LSB is set
				// Shift right and XOR 0xA001
				usCrc >>= 1;                    
				usCrc  ^= 0xA001;
			}
			else
			{
			   // Else LSB is not set
				// Just shift right
				usCrc >>= 1;
         }
		}
	}
	return(usCrc);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
//  Function:  slim_Daemon
//  Purpose:   Daemonize the serial port reader
//
//  Parms:     Fd
//  Returns:
//
static pid_t slim_Daemon()
{
   int      iFd;
   pid_t    tPid;
   sigset_t tBlockset;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         break;

      case 0:
         // child
         GEN_Sleep(2000);
         PRINTF("slim_Daemon()" CRLF);
         if(slim_SignalRegister(&tBlockset) == FALSE) 
         {
            LOG_Report(errno, "COM", "slim_Daemon():Exit:Register ERROR:");
            _exit(1);
         }
         //
         // Open serial comms with the Smart-E-meter
         // Fd=0 means dummy Obis reads.
         //
         iFd = slim_Init();
         if(iFd >= 0) 
         {
            slim_HandleMeter(iFd);
            LOG_Report(0, "COM", "slim_Daemon():Exit normally");
            if(iFd) close(iFd);
            _exit(0);
         }
         LOG_Report(errno, "COM", "slim_Daemon():Exit Init ERROR:");
         _exit(1);
         break;

      case -1:
         // Error
         LOG_Report(errno, "COM", "slim_Daemon():Exit fork ERROR:");
         _exit(1);
         break;
   }
   return(tPid);
}

//  
// Function:   slim_Init
// Purpose:    Init the smart E meter comms
// 
// Parameters: 
// Returns:    Fd
// Note:       
//  
int slim_Init(void)
{
   int            iFd=0;
   struct termios stOpt;
   RPIMAP        *pstMap=GLOBAL_GetMapping();

   iObisTest = pstMap->G_iObisTest;
   //
   if(iObisTest == 0)
   {
      //
      // OPEN THE UART
      // The flags (defined in fcntl.h):
      // Access modes (use 1 of these):
      //    O_RDONLY -  Open for reading only.
      //    O_RDWR   -  Open for reading and writing.
      //    O_WRONLY -  Open for writing only.
      //
      // O_NDELAY    
      // O_NONBLOCK  -  Enables nonblocking mode. When set read requests on the file can return immediately with a failure status
      //                if there is no input immediately available (instead of blocking). Likewise, write requests can also return
      //                immediately with a failure status if the output can't be written immediately.
      //
      // O_NOCTTY -     When set and path identifies a terminal device, open() shall not cause the terminal device to become the 
      //                controlling terminal for the process.
      //
      iFd = open("/dev/serial0", (O_RDONLY|O_NOCTTY));
      if (iFd == -1)
      {
         LOG_Report(errno, "COM", "slim_Init(): Error - Unable to open UART.  Ensure it is not in use by another application");
         PRINTF("slim_Init(): Error - Unable to open UART.  Ensure it is not in use by another application" CRLF);
         return(-1);
      }
      //
      // Configure the UART:
      //
      // The flags (defined in /usr/include/termios.h:
      // Baud rate:  B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, ...
      // CSIZE:      CS5, CS6, CS7, CS8
      // CLOCAL:     Ignore modem status lines
      // CREAD:      Enable receiver
      //
      // IGNPAR:     Ignore parity errors
      // PARENB:     Parity enable
      // PARODD:     Odd parity (else even)
      //
      // ICANON:     Canonical mode
      //
      tcgetattr(iFd, &stOpt);
      //
      stOpt.c_cflag = B115200|CS8|CLOCAL|CREAD;
      stOpt.c_iflag = IGNPAR;
      stOpt.c_oflag = 0;
      stOpt.c_lflag = ICANON;
      //
      tcflush(iFd, TCIFLUSH);
      tcsetattr(iFd, TCSANOW, &stOpt);
   }
   return(iFd);
}

//  
// Function:   slim_ObisRead
// Purpose:    Handle non-conical reading data from the serial port or dummy port
// 
// Parameters: Fd, buffer, size
// Returns:    Number read
// Note:       Depending G_iObisTest, the data comes from the COM port (and smart-meter)
//             or from a buffer with test data
//                
static int slim_ObisRead(int iFd, char *pcBuffer, int iMax)
{
   int      iRd=0, x;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(iObisTest)
   {
      case 0:
         PRINTF("slim_ObisRead():Blocking read OBIS data" CRLF);
         //
         // BLOCKING Read actual data from Obis COM port
         //
         iRd = read(iFd, pcBuffer, iMax);
         break;

      case 1:
         //
         // Init variables
         //
         flT1P = flDefaultT1P;
         flT2P = flDefaultT2P;
         flT1M = flDefaultT1M;
         flT2M = flDefaultT2M;
         flGas = flDefaultGas;
         //
         iObisTest = 2;
         // Fallthrough
      case 2:
         //
         // Dummy read delay (OBIS has 10 secs intervals)
         //
         PRINTF1("slim_ObisRead(): TEST cycle: Wait %d secs" CRLF, pstMap->G_iObisTestTime);
         GEN_Sleep(pstMap->G_iObisTestTime);
         //
         x = RTC_GetHour(0);
         //
         // Hour = 0..23
         // Read test data from buffer
         //
         iPeriod = iPeriodTarifs[x];
         GEN_SNPRINTF(pcBuffer, iMax, pcObisTestData, flT1P, flT2P, flT1M, flT2M, iPeriod, flGas);
         iRd   = GEN_STRLEN(pcBuffer);
         usCrc = SLIM_CalculateCrc16((u_int8 *)pcBuffer, iRd);
         //
         // Drop the last char: report that one together with the CRC16
         //
         iRd--;
         //
         iObisTest = 3;
         break;

      case 3:
         //
         // Update and return CRC16
         //
         GEN_SNPRINTF(pcBuffer, 8, "!%04X" CRLF, usCrc);
         iRd = GEN_STRLEN(pcBuffer);
         PRINTF1("slim_ObisRead(): TEST cycle:return CRC=%s" CRLF, pcBuffer);
         //
         // Update dummy figures
         //
         flT1P *= (double) 1.00010;
         flT2P *= (double) 1.00015;
         flT1M *= (double) 1.00020;
         flT2M *= (double) 1.00025;
         flGas *= (double) 1.00045;
         //
         iObisTest = 2;
         break;
   }
   return(iRd);
}

//  
// Function:   slim_HandleMeter
// Purpose:    Handle non-conical reading data from the serial port
// 
// Parameters: Fd
// Returns:    
// Note:       The Smart-E-meter outputs every 10 secs the following records:
//                
//             /KFM5KAIFA-METER
//             
//             1-3:0.2.8(42)
//             0-0:1.0.0(171013150653S)
//             0-0:96.1.1(4530303235303030303639373538373136)
//             1-0:1.8.1(001852.792*kWh)
//             1-0:1.8.2(002550.969*kWh)
//             1-0:2.8.1(000000.000*kWh)
//             1-0:2.8.2(000000.000*kWh)
//             0-0:96.14.0(0002)
//             1-0:1.7.0(02.236*kW)
//             1-0:2.7.0(00.000*kW)
//             0-0:96.7.21(00002)
//             0-0:96.7.9(00002)
//             1-0:99.97.0(2)(0-0:96.7.19)(170116085159W)(0000000933*s)(000101000001W)(2147483647*s)
//             1-0:32.32.0(00000)
//             1-0:32.36.0(00000)
//             0-0:96.13.1()
//             0-0:96.13.0()
//             1-0:31.7.0(009*A)
//             1-0:21.7.0(02.235*kW)
//             1-0:22.7.0(00.000*kW)
//             0-1:24.1.0(003)
//             0-1:96.1.0(4730303139333430333037303635303136)
//             0-1:24.2.1(171013150000S)(02348.695*m3)
//             !6546
//
//  
static void slim_HandleMeter(int iFd)
{
   int      iRd;
   int      iAct=0, iActMax, iActNew;
   int      iPingPong=0;
   char    *pcPingPong;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   PRINTF1("slim_HandleMeter():Buffer size=%d bytes (LINE-mode)" CRLF, SLIM_SIZE_MAX);
   pcPingPong = pstMap->G_pcPing;
   //
   GLOBAL_PidSaveGuard(PID_COMM, GLOBAL_COM_INI);
   GLOBAL_HostNotification(GLOBAL_COM_INI);
   //
   // Blocking read
   // Reserve terminator for ASCIIZ
   //
   while(fSlimRunning)
   {
      //
      // Reading is on a line-by-line basis
      //
      iActMax = SLIM_SIZE_MAX-iAct-1;
      PRINTF2("slim_HandleMeter():Read max %d bytes, Idx=%d" CRLF, iActMax, iAct);
      iRd = slim_ObisRead(iFd, &pcPingPong[iAct], iActMax);
      if(iRd > 0)
      {
         // Bytes received
         PRINTF2("slim_HandleMeter():Read %d bytes, Last=%c" CRLF, iAct, pcPingPong[iAct]);
         switch(pcPingPong[iAct])
         {
            default:
               iActNew = iAct+iRd;
               pcPingPong[iActNew] = 0;
               PRINTF1("slim_HandleMeter():%d bytes read:" CRLF, iRd);
               PRINTF1("slim_HandleMeter():%s" CRLF, &pcPingPong[iAct]);
               //
               iAct = iActNew;
               break;

            case '!':
               iActNew = iAct+iRd;
               pcPingPong[iActNew] = 0;
               PRINTF1("slim_HandleMeter():Total size=%d" CRLF, iAct);
               PRINTF1("slim_HandleMeter():%s" CRLF, &pcPingPong[iAct]);
               //
               // This line is the CRC marker: ignore crc for now
               // Terminate buffer and signal host
               //
               if(iPingPong & 1) 
               {
                  pcPingPong = pstMap->G_pcPing;
                  GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_PP1_NFY);
                  iPingPong  = 0;
               }
               else
               {
                  pcPingPong = pstMap->G_pcPong;
                  GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_PP0_NFY);
                  iPingPong  = 1;
               }
               kill(GLOBAL_PidGet(PID_HOST), SIGUSR1);
               iAct = 0;
               break;
         }
      }
      else if(iRd < 0)
      {
         // Error
         PRINTF("slim_HandleMeter():Error reading Smart-E-meter comms" CRLF);
         LOG_Report(errno, "COM", "slim_HandleMeter():Error reading Smart-E-meter comms");
         fSlimRunning = FALSE;
      }
      //
      // Check if we do not run out of buffer space !
      //
      if(iAct >= SLIM_SIZE_MAX)
      {
         PRINTF("slim_HandleMeter():Error: Bufferoverflow" CRLF);
         LOG_Report(0, "COM", "slim_HandleMeter():Error:Buffer overflow!");
         //
         iAct       = 0;
         iPingPong  = 0;
         pcPingPong = pstMap->G_pcPing;
      }
      //
      // Handle the Guard duty here
      //
      GLOBAL_PidSetGuard(PID_COMM);
      if(!GLOBAL_PidGetGuard(PID_COMM)) LOG_Report(0, "COM", "slim_HandleMeter(): ERROR Guard set !!!!!!!!");
   }
   PRINTF("slim_HandleMeter():EXIT" CRLF);
}

//
//  Function:   slim_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool slim_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &slim_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "COM", "slim_SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &slim_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "COM", "slim_SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &slim_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "COM", "slim_SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &slim_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "COM", "slim_SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &slim_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "COM", "slim_ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  //
  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
  }
  return(fCC);
}

//
//  Function:   slim_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void slim_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "COM", "slim_ReceiveSignalTerm()");
   //
   fSlimRunning = FALSE;
}

//
//  Function:   slim_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void slim_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "COM", "slim_ReceiveSignalInt()");
   //
   fSlimRunning = FALSE;
}

//
//  Function:   slim_ReceiveSignalSegmnt
//  Purpose:    Add the SIGSEGV command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void slim_ReceiveSignalSegmnt(int iSignal)
{
   if(fSlimRunning)
   {
      LOG_Report(errno, "COM", "slim_ReceiveSignalSegmnt()");
      GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_FLT_NFY);
      kill(GLOBAL_PidGet(PID_HOST), SIGTERM);
      fSlimRunning = FALSE;
   }
}

//
// Function:   slim_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR1 is used to signal HTTP data
//
static void slim_ReceiveSignalUser1(int iSignal)
{
}

//
// Function:   slim_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       
//
static void slim_ReceiveSignalUser2(int iSignal)
{
}

