/*  (c) Copyright:  2017..2018  Patrn, Confidential Data
 *
 *  Workfile:           globals.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Define the mmap variables, common over all threads. If
 *                      the structure needs to change, roll the revision number to make sure a new mmap file is created !
 *                     
 * 
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       14 Oct 2017
 *
 *  Revisions:
 *  Entry Points:
 *
 *
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <semaphore.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <netinet/in.h>

//
// Add OBIS defines for global storage
//
#include "rpi_obis.h"

#define  MAX_DYN_PAGES        64
//
#define  GUARD_COUNT          3
#define  GUARD_SECS           (30*60)
#define  GUARD_SECS_RETRY     ( 2*60)
#define  GUARD_SECS_DEBUG     ( 1*60)
//
#define  MAX_ADDR_LEN         INET_ADDRSTRLEN
#define  MAX_ADDR_LENZ        (MAX_ADDR_LEN+1)
//
#define  MAX_URL_LEN          31
#define  MAX_URL_LENZ         MAX_URL_LEN+1
//
#define  MAX_PARM_LEN         31
#define  MAX_PARM_LENZ        (MAX_PARM_LEN+1)
//
#define  MAX_PATH_LEN         255
#define  MAX_PATH_LENZ        (MAX_PATH_LEN+1)
//
#define  MAX_CMD_LEN          200
#define  MAX_ARG_LEN          1000
#define  MAX_CMDLINE_LEN      (MAX_CMD_LEN + MAX_ARG_LEN)
//
#define  SLIM_SIZE_MAX        2048
//
// Define the debug mask bits
//
#define  DEBUG_NET            0x00000001L
#define  DEBUG_CAM            0x00000010L
#define  DEBUG_CMD            0x00000100L
//
#define  DEBUG_RCU_1          0x00001000L
#define  DEBUG_RCU_2          0x00002000L
#define  DEBUG_RCU_3          0x00004000L
#define  DEBUG_RCU_4          0x00008000L
//
#define  DEBUG_USB_1          0x00010000L
#define  DEBUG_USB_2          0x00020000L
#define  DEBUG_USB_3          0x00040000L
#define  DEBUG_USB_4          0x00080000L
//
// Global notification flags
//
#define  GLOBAL_SVR_INI       0x00000001     // Ini: HTTP Server
#define  GLOBAL_COM_INI       0x00000004     // Ini: Serial Communication
#define  GLOBAL_XXX_INI       0x000000FF     // Init Mask
//
#define  GLOBAL_RPI_NFY       0x00000100     // Generic host nfy
#define  GLOBAL_VAR_NFY       0x00000200     // Variable update
#define  GLOBAL_PP0_NFY       0x00000400     // Ping buffer
#define  GLOBAL_PP1_NFY       0x00000800     // Pong buffer
#define  GLOBAL_SVR_NFY       0x00001000     // Generic HTTP request
#define  GLOBAL_FLT_NFY       0x00002000     // Segmentation fault host nfy (exit)
#define  GLOBAL_PWR_OFF       0x00004000     // Power Off button
#define  GLOBAL_RPI_RST       0x00008000     // Guard duty restart theApp
#define  GLOBAL_RPI_RBT       0x00010000     // Guard duty reboot
#define  GLOBAL_GRD_RUN       0x00020000     // Guard Run
#define  GLOBAL_GRD_NFY       0x00040000     // Guard Nfy

//
// HTTP Functions (callback through HTML or JSON)
// Internal Functions
//
#define  VAR_KEY              0x04000000     // RCU IR keypress
#define  VAR_BUT              0x08000000     // Button changed
//
// JSON markers for text-style-vars ("value" = "123456")
//             or number-style-vars ("value" =  123456 )
//
#define  JSN_TXT              0x10000000     // JSON Variable is text
#define  JSN_INT              0x20000000     // JSON Variable is integer
#define  WB                   0x80000000     // Warm boot var
//
// Parameter groups
//
#define  VAR____              0x00000000     // 
#define  VAR_STI              0x00000001     // Snapshot foto
#define  VAR_TIM              0x00000002     // Timelapse foto's
#define  VAR_REC              0x00000004     // Video recording
#define  VAR_STR              0x00000008     // Video streaming
#define  VAR_PIX              0x00000010     // List of pictures
#define  VAR_VID              0x00000020     // List of Videos
#define  VAR_PRM              0x00000040     // List of Parameters
#define  VAR_RMP              0x00000080     // Remove fotos
#define  VAR_RMV              0x00000100     // Remove videos
#define  VAR_DEF              0x00000200     // Defaults
#define  VAR_STP              0x00000400     // Stop
#define  VAR_ALM              0x00000800     // Alarm timer
#define  VAR_VEC              0x00001000     // Video motion vectors
//
#define  VAR_ERR              0x00008000     // Error replies
#define  VAR_ALL              0x0000FFFF     // All HTTP functions
//
#define  ALWAYS_CHANGED       -1
#define  NEVER_CHANGED        -2

//
// Enums PIDs
//
typedef enum _proc_pids_
{
   #define  EXTRACT_PID(a,b,c)   a,
   #include "par_pids.h"
   #undef EXTRACT_PID
   //
   NUM_PIDT
}  PIDT;
//
typedef struct _proc_pidlist_
{
   pid_t       tPid;
   int         iFlag;
   int         iNotify;
   const char *pcName;
   const char *pcHelp;
}  PIDL;

typedef u_int8 (*PFKEY)(u_int8);
//
typedef struct glocmd
{
   int      iStatus;                      // CAM_STATUS_...          CMD Camera status (RECORDING, STREAMING, ..)
   int      iError;                       // CAM_STATUS_...          Generic error conditions
   int      tUrl;                         // CAM_DYN_HTML_INFO, ...  Dynamic Page URL
   int      iCommand;                     // CAM_CMD_xxx, ...        Page command
   int      iArgs;                        // CAM_xxx                 Page argument type
   int      iData1;                       //   ..misc
   int      iData2;                       //   ..misc
   int      iData3;                       //   ..misc
   //                                     //
   bool   (*pfCallback)(struct glocmd *); // Completion callback function 
   //                                     //
   char     pcExec[MAX_CMD_LEN];          // tools pathnames
   char     pcArgs[MAX_ARG_LEN];          // tools arguments
}  GLOCMD;
//
typedef bool (*PFCMD)(GLOCMD *);
//
//
// Time defines
//
typedef enum _emeter_samples_
{
   EMS_GAS     = 0,           // Smart-E-meter Gas
   EMS_T1P,                   // Smart-E-meter T1+ Delivered
   EMS_T1M,                   // Smart-E-meter T1- Received
   EMS_T2P,                   // Smart-E-meter T2+ Delivered
   EMS_T2M,                   // Smart-E-meter T2- Received
   //
   NUM_EMS,
   //
   EMS_T1 = NUM_EMS,          // Smart-E-meter T1 total
   EMS_T2,                    // Smart-E-meter T2 total
   EMS_TP,                    // Smart-E-meter Delivered total
   EMS_TM,                    // Smart-E-meter Received total
   EMS_T,                     // Smart-E-meter Total
   //
   NUM_ALL_EMS
}  EMS;
//
typedef enum _obis_periods_
{
   OBIS_QUARTER   = 0,
   OBIS_DAY,
   OBIS_MONTH,
   OBIS_YEAR
}  OBPER;

#define MAX_DAYS              31
#define MAX_DAYSZ            (31+1)
#define MAX_QRTS             (24*4)
#define MAX_QRTSZ           ((24*4)+1)
//
#define NUM_SAMPLES           (MAX_DAYS * NUM_EMS)
#define NUM_SAMPLES_MONTH     (MAX_DAYS * NUM_EMS)
#define NUM_SAMPLES_DAY       (MAX_QRTS * NUM_EMS)
//
#define MAX_LINES             25
#define NUM_SCALES            16

//
// Global parms
//
typedef enum _global_opt_
{
   GLOOPT_NONE = 0,                 // 
   GLOOPT_SWITCH_ON,                // Option is flag and  ON
   GLOOPT_SWITCH_OFF,               //                     OFF
   GLOOPT_VALUE_ON,                 //           value and present
   GLOOPT_VALUE_OFF,                //                     not present
}  GLOOPT;

typedef enum _global_parameters_
{
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    a,
#include "par_defs.h"
#undef EXTRACT_PAR
   NUM_GLOBAL_DEFS
}  GLOPAR;

typedef struct _par_args_
{
   int            iFunction;
   const char    *pcJson;
   const char    *pcHtml;
   const char    *pcOption;
   int            iValueOffset;
   int            iValueSize;
   int            iChangedOffset;
}  PARARGS;
//
typedef struct _globals_defaults_
{
   int         iFunction;
   const char *pcDefault;
   int         iChanged;
   int         iChangedOffset;
   int         iValueOffset;
   int         iGlobalSize;
}  GLODEFS;
//
typedef struct _rpidata_
{
   char    *pcObject;
   FTYPE    tType;
   int      iIdx;
   int      iSize;
   int      iFree;
   int      iLastComma;
}  RPIDATA;

//
// Generic status strings/enums
//
typedef enum _genstat_
{
#define  EXTRACT_ST(a,b)   a,
#include "gen_stats.h"
#undef   EXTRACT_ST
   //
   NUM_GEN_STATUS,
   GEN_STATUS_ASK                   // Return current status
}  GENSTAT;
//
typedef enum _varreset_
{
   VAR_COLD    = 0,                 // Cold reset
   VAR_WARM,                        // Warm reset
   VAR_UPDATE,                      // Update

   NUM_VARRESETS
}  VARRESET;
//
typedef enum _mapstate_
{
  MAP_CLEAR = 0,                    // Clear all the mapped memory
  MAP_SYNC,                         // Synchronize the map
  MAP_RESTART,                      // Restart the counters using the last stored data

  NUM_MAPSTATES
} MAPSTATE;

typedef struct _globals_
{
   int         iFunction;
   const char *pcDefault;
   int         iChanged;
   int         iChangedOffset;
   int         iValueOffset;
   int         iGlobalSize;
}  GLOBALS;

//
//
// Dynamic page registered URLs
//
typedef bool(*PFVOIDPI)(void *, int);
typedef struct _dynr_
{
   int         tUrl;                   // RPI_DYN_HTML_EMPTY, ...
   FTYPE       tType;                  // HTTP_JSON, HTTP_...
   int         ePid;                   // Owner pid enum PID_xxxx
   int         iTimeout;               // Estimated call duration 
   int         iPort;                  // Port
   char        pcUrl[MAX_URL_LENZ];    // URL
   PFVOIDPI    pfCallback;             // Callback function
}  DYNR;

//=============================================================================
// Global parameters mapped to a shared MMAP file 
//=============================================================================
typedef struct _rpimap_
{
   int               G_iSignature1;          // Signature top
   char              G_iVersionMajor;
   char              G_iVersionMinor;
   sem_t             G_tSemSrvr;
   sem_t             G_tSemHost;
   pthread_mutex_t   G_tMutex;
   //
   //=============== Start WB reset area ======================================
   int               G_iResetStart;
   //
   int               G_iTraceCode;
   int               G_iNotify;
   int               G_iHttpStatus;
   int               G_iHttpArgs;
   int               G_iCurDynPage;
   int               G_iNumDynPages;
   int               G_iRcuKey;
   int               G_iMallocs;
   int               G_iGuards;
   //
   u_int32           G_ulStartTimestamp;
   u_int32           G_ulSecondsCounter;
   u_int32           G_ulDebugMask;
   //
   double            G_flDispDay             [NUM_EMS][MAX_QRTS];
   double            G_flDispMonth           [NUM_EMS][MAX_DAYS];
   //
   GLOCMD            G_stCmd;
   DYNR              G_stDynPages            [MAX_DYN_PAGES];
   PIDL              G_stPidList             [NUM_PIDT];
   //--------------------------------------------------------------------------
   // Take new power-on reset parms from this pool:
   //--------------------------------------------------------------------------
   #define SPARE_POOL_ZERO 1000
   #define EXTRA_POOL_ZERO 0
   //
   char              G_pcSparePoolZero       [SPARE_POOL_ZERO-EXTRA_POOL_ZERO];
   //
   int               G_iResetEnd;
   //=============== End WB reset area ========================================
   //
   double            G_flLastMonth           [NUM_EMS][MAX_DAYS];
   double            G_flThisMonth           [NUM_EMS][MAX_DAYS];
   double            G_flLastDay             [NUM_EMS][MAX_QRTS];
   double            G_flThisDay             [NUM_EMS][MAX_QRTS];
   //
   char              G_pcDebugMask           [MAX_PARM_LENZ];
   char              G_pcSrvrPort            [MAX_PARM_LENZ];
   char              G_pcPing                [SLIM_SIZE_MAX];
   char              G_pcPong                [SLIM_SIZE_MAX];
   //
   // Smart-E data
   //
   char              G_pcSmartTotals         [MAX_PARM_LENZ];
   char              G_pcSmartCsvs           [MAX_PARM_LENZ];
   char              G_pcSmartCrcs           [MAX_PARM_LENZ];
   char              G_pcSmartTime           [MAX_PARM_LENZ];
   char              G_pcT1Plus              [MAX_PARM_LENZ];
   char              G_pcT1Min               [MAX_PARM_LENZ];
   char              G_pcT2Plus              [MAX_PARM_LENZ];
   char              G_pcT2Min               [MAX_PARM_LENZ];
   char              G_pcGas                 [MAX_PARM_LENZ];
   char              G_pcGasPeriod           [MAX_PARM_LENZ];
   //
   char              G_pcSmartEquipment      [MAX_PARM_LENZ];
   char              G_pcSmartTariff         [MAX_PARM_LENZ];
   char              G_pcSmartPower          [MAX_PARM_LENZ];
   char              G_pcSmartCurrent        [MAX_PARM_LENZ];
   //
   char              G_pcSmartStuff          [MAX_PARM_LENZ];
   char              G_pcSmartPeriod         [MAX_PARM_LENZ];
   //
   u_int32           G_ulSmartSecs;
   //
   int               G_iGasPeriod;
   int               G_iSmartScaleMonth;
   int               G_iSmartScaleDay;
   int               G_iObisTest;
   int               G_iObisTestTime;
   //
   // Misc vars
   //
   char              G_pcHostname            [MAX_URL_LENZ];
   char              G_pcSwVersion           [MAX_PARM_LENZ];
   char              G_pcCommand             [MAX_PARM_LENZ];
   char              G_pcStatus              [MAX_PARM_LENZ];
   char              G_pcFilter              [MAX_PARM_LENZ];
   char              G_pcRcuKey              [MAX_PARM_LENZ];
   char              G_pcLastFile            [MAX_PATH_LENZ];
   char              G_pcLastFileSize        [MAX_PARM_LENZ];
   char              G_pcNumFiles            [MAX_PARM_LENZ];
   char              G_pcDelete              [MAX_PATH_LENZ];
   char              G_pcWwwDir              [MAX_PATH_LENZ];
   char              G_pcRamDir              [MAX_PATH_LENZ];
   char              G_pcDefault             [MAX_PATH_LENZ];
   char              G_pcMyIpIfce            [MAX_PARM_LENZ];
   char              G_pcMyIpAddr            [MAX_ADDR_LENZ];
   char              G_pcIpAddr              [MAX_ADDR_LENZ];
   //
   // Changed markers
   //
   u_int8            G_ubCommandChanged;
   u_int8            G_ubIpAddrChanged;
   //
   EMTR              G_stEmtrTot;
   EMTR              G_stEmtrNow;
   EMTR              G_stEmtrQtr;
   EMTR              G_stEmtrDay;
   EMTR              G_stEmtrMon;
   EMTR              G_stEmtrCsv;
   //--------------------------------------------------------------------------
   // Take new static parms from this pool:
   //--------------------------------------------------------------------------
   #define SPARE_POOL_PERS 2000
   #define EXTRA_POOL_PERS (MAX_PARM_LENZ + MAX_PARM_LENZ)
   //
   // Extra from non-volatile pool
   //
   char              G_pcSunQsta             [MAX_PARM_LENZ];
   char              G_pcSunQend             [MAX_PARM_LENZ];
   //
   char              G_pcSparePool2          [SPARE_POOL_PERS-EXTRA_POOL_PERS];
   //
   int               G_iSignature2;          // Signature bottom
}  RPIMAP;

//
// Global functions
//
bool           GLOBAL_Init                   (void);
//
void           GLOBAL_ExpandMap              (int, int);
bool           GLOBAL_Close                  (void);
RPIMAP        *GLOBAL_GetMapping             (void);
int            GLOBAL_Lock                   (void);
int            GLOBAL_Unlock                 (void);
char          *GLOBAL_GetHostName            (void);
int            GLOBAL_GetTraceCode           (void);
u_int32        GLOBAL_ConvertDebugMask       (bool);
u_int32        GLOBAL_GetDebugMask           (void);
void           GLOBAL_SetDebugMask           (u_int32);
u_int32        GLOBAL_ReadSecs               (void);
void           GLOBAL_RestoreDefaults        (void);
bool           GLOBAL_Share                  (void);
int            GLOBAL_Status                 (int);
bool           GLOBAL_Sync                   (void);
const PARARGS *GLOBAL_GetParameters          (void);
char          *GLOBAL_GetParameter           (GLOPAR);
bool           GLOBAL_SetParameterChanged    (GLOPAR);
int            GLOBAL_InsertOptions          (char *, int, int);
char          *GLOBAL_GetOption              (const char *);
GLOOPT         GLOBAL_OptionIsActive         (const PARARGS *);
bool           GLOBAL_ParmHasChanged         (const PARARGS *);
bool           GLOBAL_ParmSetChanged         (const PARARGS *, bool);
GLOCMD        *GLOBAL_CommandCopy            (void);
void           GLOBAL_CommandDestroy         (GLOCMD *);
void           GLOBAL_CommandGet             (GLOCMD *);
void           GLOBAL_CommandGetAll          (GLOCMD *);
void           GLOBAL_CommandPut             (GLOCMD *);
void           GLOBAL_CommandPutAll          (GLOCMD *);
bool           GLOBAL_PidCheckGuards         (void);
bool           GLOBAL_PidGetGuard            (PIDT);
bool           GLOBAL_PidSetGuard            (PIDT);
void           GLOBAL_PidClearGuards         (void);
void           GLOBAL_PidSaveGuard           (PIDT, int);
pid_t          GLOBAL_PidGet                 (PIDT);
const char    *GLOBAL_PidGetName             (PIDT);
const char    *GLOBAL_PidGetHelp             (PIDT);
bool           GLOBAL_PidPut                 (PIDT, pid_t);
int            GLOBAL_PidsLog                (void);
int            GLOBAL_PidsTerminate          (int);
int            GLOBAL_Notify                 (int, int, int);
int            GLOBAL_HostNotification       (int);
bool           GLOBAL_GetSignalNotification  (int, int);
int            GLOBAL_SetSignalNotification  (int, int);
void           GLOBAL_UpdateSecs             (void);
//
int            GLOBAL_GetMallocs             (void);
void           GLOBAL_PutMallocs             (int);

#endif /* _GLOBALS_H_ */
