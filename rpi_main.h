/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           rpi_main.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for main thread
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       17 Sep 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_MAIN_H_
#define _RPI_MAIN_H_

#define  CSV_MAX           256            // buffer for CSV record
#define  SECS_PER_QTR      (60*15)        // Secs in 1 quarter hour
#define  SECS_PER_DAY      (60*60*24)     // Secs in 1 day of 24 hours

#ifdef   DEBUG_ASSERT
//
// Debug mode: write every 60 secs
//
#define  CSV_EMTR_SECS     60             // 60 secs sampling time
#else    //DEBUG_ASSERT
//
// Sample CSV record every quarter hour
//
#define  CSV_EMTR_SECS     (15*60)        // 15 mins sampling time
//
#endif   //DEBUG_ASSERT

#endif /* _RPI_MAIN_H_ */
