/*  (c) Copyright:  2013..2018 Patrn, Confidential Data 
 *
 *  Workfile:           rpi_json.c
 *  Revision:   
 *  Modtime:    
 *
 *  Purpose:            JSON functions and data
 *
 *
 * 
 * 
 *
 * 
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       27 Jun 2013
 *
 *  Revisions:
 *  Entry Points:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_page.h"
//
#include "rpi_json.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
static RPIDATA *json_AllocateObject       (int);
static void     json_FreeObject           (RPIDATA *);
static RPIDATA *json_Insert               (RPIDATA *, const char *);
static RPIDATA *json_InsertTextFile       (RPIDATA *, char *);
//
extern const char pcHttpResponseLengthJson[];

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : JSON_RespondObject
 * Description : Raspicam Respond with the JSON objects
 *
 * Parameters  : Client, JSON object
 * Returns     : TRUE if OKee
 *
 */
bool JSON_RespondObject(NETCL *pstCl, RPIDATA *pstCam)
{
   HTTP_BuildGeneric(pstCl, pcHttpResponseLengthJson, pstCam->iIdx);
   HTTP_BuildGeneric(pstCl, pstCam->pcObject);
   return(TRUE);
}

/*
 * Function    : JSON_InsertFile
 * Description : Insert array from file into JSON object
 *
 * Parameters  : JSON object, parameter, filename, style (multi/text/int)
 * Returns     : JSON object
 *
 */
RPIDATA *JSON_InsertFile(RPIDATA *pstCam, const char *pcParm, char *pcFile, int iStyle)
{
   int      iSize;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   if(pstCam == NULL)
   {
      pstCam = json_AllocateObject(HTTP_DEFAULT_DATA_SIZE);
   }
   //
   pstCam->iLastComma = -1;
   //
   // Supply a filename to insert as result array
   //
   FINFO_GetFileInfo(pstMap->G_pcLastFile, FI_SIZE, pstMap->G_pcLastFileSize, MAX_PATH_LEN);
   iSize = strtoul(pstMap->G_pcLastFileSize, NULL, 10);
   if(iSize)
   {
      pstCam = JSON_InsertParameter(pstCam, pcParm,   NULL,    0);
      pstCam = json_InsertTextFile( pstCam, pcFile);
      pstCam = JSON_InsertParameter(pstCam, NULL,     NULL,    JE_COMMA|JE_CRLF);
   }
   else
   {
      pstCam = JSON_InsertParameter(pstCam, pcParm,   "[]",    JE_COMMA);
   }
   return(pstCam);
}

/*
 * Function    : JSON_InsertInteger
 * Description : Insert integer parameter into JSON object
 *
 * Parameters  : JSON object, parameter, integer, style (multi/text/int
 * Returns     : JSON object
 *
 */
RPIDATA *JSON_InsertInteger(RPIDATA *pstCam, const char *pcParm, int iValue, int iStyle)
{
   char  pcValue[32];

   if(pstCam == NULL)
   {
      pstCam = json_AllocateObject(HTTP_DEFAULT_DATA_SIZE);
   }
   //
   pstCam->iLastComma = -1;
   GEN_SNPRINTF(pcValue, 32, "%d", iValue);
   //
   //PRINTF3("JSON_InsertInteger():parm=%s, value=%s, style=%d" CRLF, pcParm, pcValue, iStyle);
   //
   if(iStyle & JE_CURLB)   pstCam = json_Insert(pstCam, "{");
   if(pcParm)
   {
                           pstCam = json_Insert(pstCam, "\"");
                           pstCam = json_Insert(pstCam, pcParm);
                           pstCam = json_Insert(pstCam, "\"");
                           pstCam = json_Insert(pstCam, ":");
   }
   if(iStyle & JE_SQRB)  { pstCam->iLastComma = -1;              pstCam = json_Insert(pstCam, "[");  }
   if(iStyle & JE_TEXT)    pstCam = json_Insert(pstCam, "\"");
   pstCam = json_Insert(pstCam, pcValue);
   if(iStyle & JE_TEXT)    pstCam = json_Insert(pstCam, "\"");
   if(iStyle & JE_COMMA) { pstCam->iLastComma = pstCam->iIdx;    pstCam = json_Insert(pstCam, ", "); }
   if(iStyle & JE_SQRE)  { pstCam->iLastComma = pstCam->iIdx +1; pstCam = json_Insert(pstCam, "],"); }
   if(iStyle & JE_CURLE) { pstCam->iLastComma = pstCam->iIdx +1; pstCam = json_Insert(pstCam, "},"); }
   if(iStyle & JE_CRLF)    pstCam = json_Insert(pstCam, "\r\n");
   //
   return(pstCam);
}

/*
 * Function    : JSON_InsertParameter
 * Description : Insert parameter into JSON object
 *
 * Parameters  : JSON object, parameter, value, style (multi/text/int
 * Returns     : JSON object
 *
 */
RPIDATA *JSON_InsertParameter(RPIDATA *pstCam, const char *pcParm, char *pcValue, int iStyle)
{
   if(pstCam == NULL)
   {
      pstCam = json_AllocateObject(HTTP_DEFAULT_DATA_SIZE);
   }
   //
   pstCam->iLastComma = -1;
   //
   //PRINTF3("JSON_InsertParameter():parm=%s, value=%s, style=%d" CRLF, pcParm, pcValue, iStyle);
   //
   if(iStyle & JE_CURLB)   pstCam = json_Insert(pstCam, "{");
   if(pcParm)
   {
                           pstCam = json_Insert(pstCam, "\"");
                           pstCam = json_Insert(pstCam, pcParm);
                           pstCam = json_Insert(pstCam, "\"");
                           pstCam = json_Insert(pstCam, ":");
   }
   if(iStyle & JE_SQRB)  { pstCam->iLastComma = -1;              pstCam = json_Insert(pstCam, "[");  }
   if(iStyle & JE_TEXT)    pstCam = json_Insert(pstCam, "\"");
   if(pcValue)             pstCam = json_Insert(pstCam, pcValue);
   if(iStyle & JE_TEXT)    pstCam = json_Insert(pstCam, "\"");
   if(iStyle & JE_COMMA) { pstCam->iLastComma = pstCam->iIdx;    pstCam = json_Insert(pstCam, ", "); }
   if(iStyle & JE_SQRE)  { pstCam->iLastComma = pstCam->iIdx +1; pstCam = json_Insert(pstCam, "],"); }
   if(iStyle & JE_CURLE) { pstCam->iLastComma = pstCam->iIdx +1; pstCam = json_Insert(pstCam, "},"); }
   if(iStyle & JE_CRLF)    pstCam = json_Insert(pstCam, "\r\n");
   //
   return(pstCam);
}

/*
 * Function    : JSON_InsertValue
 * Description : Insert single value into JSON object
 *
 * Parameters  : JSON object, value, style (multi/text/int
 * Returns     : JSON object
 *
 */
RPIDATA *JSON_InsertValue(RPIDATA *pstCam, char *pcValue, int iStyle)
{
   if(pstCam == NULL)
   {
      pstCam = json_AllocateObject(HTTP_DEFAULT_DATA_SIZE);
   }
   //
   pstCam->iLastComma = -1;
   //
   //PRINTF2("JSON_InsertParameter():value=%s, style=%d" CRLF, pcValue, iStyle);
   //
   if(iStyle & JE_CURLB)   pstCam = json_Insert(pstCam, "{");
   if(iStyle & JE_SQRB)    pstCam = json_Insert(pstCam, "[");
   if(iStyle & JE_TEXT)    pstCam = json_Insert(pstCam, "\"");
   pstCam = json_Insert(pstCam, (const char *)pcValue);
   if(iStyle & JE_TEXT)    pstCam = json_Insert(pstCam, "\"");
   if(iStyle & JE_COMMA) { pstCam->iLastComma = pstCam->iIdx;    pstCam = json_Insert(pstCam, ", "); }
   if(iStyle & JE_SQRE)  { pstCam->iLastComma = pstCam->iIdx +1; pstCam = json_Insert(pstCam, "],"); }
   if(iStyle & JE_CURLE) { pstCam->iLastComma = pstCam->iIdx +1; pstCam = json_Insert(pstCam, "},"); }
   if(iStyle & JE_CRLF)    pstCam = json_Insert(pstCam, "\r\n");
   //
   return(pstCam);
}

/*
 * Function    : JSON_TerminateObject
 * Description : Terminate JSON object
 *
 * Parameters  : JSON object
 * Returns     : JSON Object
 *
 */
RPIDATA *JSON_TerminateObject(RPIDATA *pstCam)
{
   if(pstCam->iLastComma > 0)
   {
      pstCam->pcObject[pstCam->iLastComma] = ' ';
      pstCam->iLastComma = -1;
   }
   pstCam = json_Insert(pstCam, "}");
   PRINTF1("JSON_TerminateObject():%s" CRLF, pstCam->pcObject);
   return(pstCam);
}

/*
 * Function    : JSON_TerminateArray
 * Description : Build JSON array
 *
 * Parameters  : JSON object
 * Returns     : JSON Object
 *
 */
RPIDATA *JSON_TerminateArray(RPIDATA *pstCam)
{
   if(pstCam->iLastComma > 0)
   {
      pstCam->pcObject[pstCam->iLastComma] = ' ';
      pstCam->iLastComma = -1;
   }
   pstCam = json_Insert(pstCam, "]");
   pstCam->iLastComma = pstCam->iIdx;
   pstCam = json_Insert(pstCam, ",\r\n");
   return(pstCam);
}

/*
 * Function    : JSON_GetObjectSize
 * Description : return JSON object size
 *
 * Parameters  : JSON object
 * Returns     : Size
 *
 */
int JSON_GetObjectSize(RPIDATA *pstCam)
{
   //PRINTF1("JSON_GetObjectSize():%d" CRLF, pstCam->iIdx);
   return(pstCam->iIdx);
}

/*
 * Function    : JSON_ReleaseObject
 * Description : Release JSON objects
 *
 * Parameters  : JSON object
 * Returns     : 
 *
 */
void JSON_ReleaseObject(RPIDATA *pstCam)
{
   //PRINTF("JSON_ReleaseObject()" CRLF);
   //
   json_FreeObject(pstCam);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

/*
 * Function    : json_AllocateObject
 * Description : Build empty JSON object
 *
 * Parameters  : Initial size
 * Returns     : JSON object
 *
 */
static RPIDATA *json_AllocateObject(int iLength)
{
   RPIDATA *pstCam = safemalloc(sizeof(RPIDATA));

   pstCam->pcObject     = safemalloc(iLength);
   pstCam->iIdx         = 0;
   pstCam->iLastComma   = -1;
   pstCam->iSize        = iLength;
   pstCam->iFree        = iLength;
   pstCam->tType        = HTTP_JSON;
   
   //PRINTF1("json_AllocateObject():%d" CRLF, iLength);
   return(pstCam);
}

/*
 * Function    : json_FreeObject
 * Description : Free JSON object
 *
 * Parameters  : JSON Object
 * Returns     : 
 *
 */
static void json_FreeObject(RPIDATA *pstCam)
{
   //PRINTF2("json_FreeObject():%d bytes used, %d bytes free" CRLF, pstCam->iIdx, pstCam->iFree);
   //
   if(pstCam)
   {
      safefree(pstCam->pcObject);
      safefree(pstCam);
   }
}

/*
 * Function    : json_Insert
 * Description : Insert JSON object
 *
 * Parameters  : JSON Object, data
 * Returns     : JSON Object (might have changed)
 *
 */
static RPIDATA *json_Insert(RPIDATA *pstCam, const char *pcData)
{
   RPIDATA *pstNew = pstCam;

   int   iIdx=pstNew->iIdx;
   int   iSize, iLen=GEN_STRLEN(pcData);

   //PRINTF1("json_Insert():[%s]" CRLF, pcData);
   //
   if(iLen >= pstCam->iFree)
   {
      //
      // Calc min required size (allocate more)
      //
      iSize = pstCam->iSize + iLen - pstCam->iFree;
      PRINTF2("json_Insert(): Need more space: size=%d, free=%d" CRLF, iLen, pstNew->iFree);
      pstNew = json_AllocateObject(iSize + HTTP_DEFAULT_DATA_SIZE);
      GEN_STRNCPY(pstNew->pcObject, pstCam->pcObject, pstCam->iSize);
      // Copy new values or necessary values from old object
      pstNew->iIdx       = iIdx;
      pstNew->iFree      = pstNew->iFree - iIdx;
      pstNew->iLastComma = pstCam->iLastComma;
      json_FreeObject(pstCam);
   }
   GEN_STRNCPY(&pstNew->pcObject[iIdx], pcData, pstNew->iFree);
   pstNew->iIdx  += iLen;
   pstNew->iFree -= iLen;
   //
   return(pstNew);
}

// 
// Function:   json_InsertTextFile
// Purpose:    Insert a textfile from fs into JSON object 
// 
// Parameters: RPIDATA *, filename
// Returns:    TRUE if OKee
// Note:       
//
RPIDATA *json_InsertTextFile(RPIDATA *pstCam, char *pcFile)
{
   int      iRead, iTotal=0;
   FILE    *ptFile;
   char    *pcBuffer;
   char    *pcRead;

   ptFile = fopen(pcFile, "r");

   if(ptFile)
   {
      pcBuffer = safemalloc(COPY_BUFSIZE);
      //
      PRINTF1("json_InsertTextFile(): File=<%s>"CRLF, pcFile);
      do
      {
         pcRead = fgets(pcBuffer, COPY_BUFSIZE, ptFile);
         if(pcRead)
         {
            iRead = GEN_STRLEN(pcBuffer);
            //PRINTF2("json_InsertTextFile(): %s(%d)"CRLF, pcBuffer, iRead);
            pstCam = json_Insert(pstCam, pcBuffer);
            iTotal += iRead;
         }
      }
      while(pcRead);
      //PRINTF1("json_InsertTextFile(): %d bytes written."CRLF, iTotal);
      fclose(ptFile);
      free(pcBuffer);
   }
   else
   {
      PRINTF1("json_InsertTextFile(): Error open file <%s>"CRLF, pcFile);
   }
   return(pstCam);
}
