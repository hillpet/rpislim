/*  (c) Copyright:  2018  Patrn.nl, Confidential Data
 *
 *  Workfile:           gen_button.c
 *  Revision:   
 *  Modtime:    
 *
 *  Purpose:            Interface Simple Buttons functions
 *
 *
 * 
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       08 Feb 2018
 * 
 *  Revisions:          wiringPi
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_button.h"

//#define USE_PRINTF
#include "printf.h"


//
// Local prototypes/data
//
static void gen_SetupGpio        (void);

/*
 * Function    : GEN_Init
 * Description : Init Simple key handler
 *
 * Parameters  : 
 * Returns     : TRUE if OKee
 *
 */
bool GEN_Init(void)
{
   gen_SetupGpio();
   return(TRUE);
}

/*
 * Function    : GEN_Exit
 * Description : Exit Simple key handler
 *
 * Parameters  : 
 * Returns     : TRUE if OKee
 * Note        : Turn Red and Green LEDs OFF
 *
 */
bool GEN_Exit(void)
{
   LED(LED_R, 0);
   LED(LED_G, 0);
   //
   return(TRUE);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_(){};
----------------------------------------------------------------------------*/

/*
 * Function    : gen_SetupGpio()
 * Description : Setup GPIO for various IO alternatives
 *
 * Parameters  : 
 * Returns     : 
 *
 */
static void gen_SetupGpio()
{

#ifdef FEATURE_IO_NONE                                      // We have no Digital I/O
   //=============================================================================
   // Use NO I/O board
   //=============================================================================
   PRINTF("gen_SetupGpio(): NO IO-board Setup" CRLF);
   //
#endif


#ifdef FEATURE_IO_PATRN                                     // Custom digital I/O
   //=============================================================================
   // Used Patrn.nl IO board (wiringPi API)
   //=============================================================================
   PRINTF("gen_SetupGpio(): PatrnBoard Setup" CRLF);
   //
   if( (wiringPiSetup() == -1) )
   {
      LOG_Report(0, "BTN", "gen_SetupGpio(): ERROR: wiringPiSetup");
      LOG_printf("gen_SetupGpio(): wiringPiSetup failed!" CRLF);
   }
   //
   INP_GPIO(LED_R); OUT_GPIO(LED_R);
   INP_GPIO(LED_G); OUT_GPIO(LED_G);
   //
   INP_GPIO(BUTTON_OFF);
   //
#endif
   //
   // Set initial values
   //
   LED(LED_R, 1);
   LED(LED_G, 1);
   //
   PRINTF("gen_SetupGpio(): Ready" CRLF);
}


