/*  (c) Copyright:  2017..2018  Patrn, Confidential Data
 *
 *  Workfile:           par_defs.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            All defines for extracting rpicam data
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *
 *  Macro elements:           
 *
 *       a  Enum              
 *       b  iFunction         
 *       c  pcJson            
 *       d  pcHtml            
 *       e  pcOption          
 *       f  iValueOffset      
 *       g  iValueSize        
 *       h  iChangedOffset    
 *       i  Changed           
 *       j  pcDefault         
 *       k  pfHttpFun         
 *
 *
 *
 *
 *
 *
**/

//
// Macro:       a                                              b                                     c                 d        e           f                                     g                  h                                 i         j                   k
//          Cmd-Enum,         _____________________________iFunction____________________________   pcJson,           pcHtml   pcOption iValueOffset                           iValueSize        iChangedOffset                         pcDefault                     pfHttpFun
//                               Txt/Int Snaps   Tlapse  Recs    Strm    Misc
EXTRACT_PAR(VAR_COMMAND,     (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "Command",        "cmd=",  "",      offsetof(RPIMAP, G_pcCommand),         MAX_PARM_LEN,     offsetof(RPIMAP, G_ubCommandChanged),  0,        "",                 NULL)
   // Misc
EXTRACT_PAR(VAR_VERSION,     (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "Version",        "vrs=",  "",      offsetof(RPIMAP, G_pcSwVersion),       MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        VERSION,            NULL)
EXTRACT_PAR(VAR_NRFILES,     (WB|JSN_INT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "NumFiles",       "dir=",  "",      offsetof(RPIMAP, G_pcNumFiles),        MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "0",                NULL)
EXTRACT_PAR(VAR_FILTER,      (   JSN_TXT|VAR____|VAR____|VAR____|VAR_VID|VAR_PIX|VAR____|VAR____), "Filter",         "ftr=",  "",      offsetof(RPIMAP, G_pcFilter),          MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,        "*.csv",            NULL)
EXTRACT_PAR(VAR_STATUS,      (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "Status",         "",      "",      offsetof(RPIMAP, G_pcStatus),          MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "Gen-Idle",         NULL)
EXTRACT_PAR(VAR_DELETE,      (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "Delete",         "del=",  "",      offsetof(RPIMAP, G_pcDelete),          MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "",                 NULL)
   // Smart-E data :
EXTRACT_PAR(VAR_SLM_STF,     (   JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "Stuff",          "",      "",      offsetof(RPIMAP, G_pcSmartStuff),      MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "Gas",              NULL)
EXTRACT_PAR(VAR_SLM_PER,     (   JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "Period",         "",      "",      offsetof(RPIMAP, G_pcSmartPeriod),     MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "Month",            NULL)
EXTRACT_PAR(VAR_SLM_TOT,     (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "Totals",         "",      "",      offsetof(RPIMAP, G_pcSmartTotals),     MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "0",                NULL)
EXTRACT_PAR(VAR_SLM_SAM,     (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "Samples",        "",      "",      offsetof(RPIMAP, G_pcSmartCsvs),       MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "0",                NULL)
EXTRACT_PAR(VAR_SLM_CRC,     (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "Crc",            "",      "",      offsetof(RPIMAP, G_pcSmartCrcs),       MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "0",                NULL)
EXTRACT_PAR(VAR_SLM_TIME,    (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "Sampletime",     "",      "",      offsetof(RPIMAP, G_pcSmartTime),       MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "",                 NULL)
EXTRACT_PAR(VAR_SLM_GAS,     (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "Gas",            "",      "",      offsetof(RPIMAP, G_pcGas),             MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "",                 NULL)
EXTRACT_PAR(VAR_SLM_T1P,     (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "T1P",            "",      "",      offsetof(RPIMAP, G_pcT1Plus),          MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "",                 NULL)
EXTRACT_PAR(VAR_SLM_T1M,     (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "T1M",            "",      "",      offsetof(RPIMAP, G_pcT1Min),           MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "",                 NULL)
EXTRACT_PAR(VAR_SLM_T2P,     (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "T2P",            "",      "",      offsetof(RPIMAP, G_pcT2Plus),          MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "",                 NULL)
EXTRACT_PAR(VAR_SLM_T2M,     (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "T2M",            "",      "",      offsetof(RPIMAP, G_pcT2Min),           MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "",                 NULL)
EXTRACT_PAR(VAR_SLM_SQS,     (   JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "SunQs",          "",      "",      offsetof(RPIMAP, G_pcSunQsta),         MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "40",               NULL)
EXTRACT_PAR(VAR_SLM_SQE,     (   JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "SunQe",          "",      "",      offsetof(RPIMAP, G_pcSunQend),         MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "76",               NULL)
   // The files :
EXTRACT_PAR(VAR_LASTFILE,    (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "LastFile",       "o=",    "-o",    offsetof(RPIMAP, G_pcLastFile),        MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,        "-",                NULL)
EXTRACT_PAR(VAR_LASTFILESIZE,(WB|JSN_INT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "LastFileSize",   "",      "",      offsetof(RPIMAP, G_pcLastFileSize),    MAX_PARM_LEN,     NEVER_CHANGED,                         0,        "0",                NULL)
EXTRACT_PAR(VAR_WWW_DIR,     (   JSN_TXT|VAR_STI|VAR_TIM|VAR_REC|VAR_STR|VAR____|VAR_PRM|VAR____), "WwwDir",         "",      "",      offsetof(RPIMAP, G_pcWwwDir),          MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,        RPI_PUBLIC_WWW,     NULL)
EXTRACT_PAR(VAR_RAM_DIR,     (   JSN_TXT|VAR_STI|VAR_TIM|VAR_REC|VAR_STR|VAR____|VAR_PRM|VAR____), "RamDir",         "",      "",      offsetof(RPIMAP, G_pcRamDir),          MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,        RPI_RAMFS_DIR,      NULL)
EXTRACT_PAR(VAR_DEFAULT,     (   JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_PRM|VAR____), "Default",        "",      "",      offsetof(RPIMAP, G_pcDefault),         MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,        RPI_PUBLIC_DEFAULT, NULL)
   // Streamer/server
EXTRACT_PAR(VAR_HOSTNAME,    (   JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_ALL), "Hostname",       "",      "",      offsetof(RPIMAP, G_pcHostname),        MAX_URL_LEN,      ALWAYS_CHANGED,                        0,        "",                  NULL)
EXTRACT_PAR(VAR_MYIP,        (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR_STR|VAR____|VAR_PRM|VAR_ERR), "MyIpAddr",       "",      "",      offsetof(RPIMAP, G_pcMyIpAddr),        MAX_ADDR_LEN,     NEVER_CHANGED,                         0,        "",                 NULL)
EXTRACT_PAR(VAR_IP,          (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR_STR|VAR____|VAR_PRM|VAR_ERR), "IpAddr",         "",      "",      offsetof(RPIMAP, G_pcIpAddr),          MAX_ADDR_LEN,     offsetof(RPIMAP, G_ubIpAddrChanged),   0,        "",                 NULL)
EXTRACT_PAR(VAR_SRVR_PORT,   (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR_STR|VAR____|VAR_PRM|VAR_ERR), "ServerPort",     "",      "",      offsetof(RPIMAP, G_pcSrvrPort),        MAX_PARM_LEN,     NEVER_CHANGED,                         0,        "80",               NULL)
EXTRACT_PAR(VAR_DEBUG_MASK,  (   JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_PRM|VAR_ERR), "DebugMask",      "",      "",      offsetof(RPIMAP, G_pcDebugMask),       MAX_PARM_LEN,     NEVER_CHANGED,                         0,        "0",                http_CollectDebug )
EXTRACT_PAR(VAR_RCU_KEY,     (WB|JSN_TXT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_PRM), "RcuKey",         "key=",  "",      offsetof(RPIMAP, G_pcRcuKey),          MAX_PARM_LEN,     NEVER_CHANGED,                         0,        "0",                http_CollectRcuKey)
EXTRACT_PAR(VAR_GAS_PER,     (   JSN_INT|VAR____|VAR____|VAR____|VAR____|VAR____|VAR____|VAR_PRM), "GasPer",         "",      "",      offsetof(RPIMAP, G_pcGasPeriod),       MAX_PARM_LEN,     NEVER_CHANGED,                         0,        "4",                http_CollectGasPeriod)
