/*  (c) Copyright:  2017..2018  Patrn, Confidential Data
 *
 *  Workfile:           config.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            
 *
 *                     
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       13 Okt 2017
 * 
 *  Revisions:
 *
 *
 *
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CONFIG_H_
#define _CONFIG_H_

//#define FEATURE_NET_REPORT_PAGE      // Report full HTML page on NET errors
  #define FEATURE_NET_DUMP_RECORD      // Dump contents of network buffers
//#define FEATURE_DISPLAY_FULL_SCALE   // Display data per full scale (VS=xxx.x) i.o per schaaldeel (xxx.x/sd)
//#define FEATURE_ECHO_HTTP_IN         // Echo all incoming HTTP data
//#define FEATURE_ECHO_HTTP_OUT        // Echo all outging  HTTP data
//#define FEATURE_SAMPLE_PER_YEAR      // Sample in one file per year
  #define FEATURE_SAMPLE_PER_MONTH     // Sample in one file per month
//#define FEATURE_SAMPLE_PER_DAY       // Sample in one file per day

typedef enum RCUKEYS
{
#define  EXTRACT_KEY(a,b)  a,
#include "key_defs.h"
#undef   EXTRACT_KEY
   //
   NUM_RCU_KEYS
}  RCUKEYS;

//=============================================================================
#ifdef   FEATURE_IO_NONE      // IO : NO IO board used
//=============================================================================
#define  RPI_IO_BOARD         "RpiSlim uses NO IO !"
//
#define  LED_R
#define  LED_G
#define  LED_Y
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  BUTTON(x)            FALSE
#define  LED(x, y)            
#endif

//=============================================================================
#ifdef   FEATURE_IO_PATRN     // IO : Use Patrn.nl board
//=============================================================================
#include <wiringPi.h>
//
#define  RPI_IO_BOARD         "RpiSlim uses Discrete LEDs"
//
#define  LED_R                21
#define  LED_G                23
#define  LED_Y                0
//
#define  BUTTON_OFF           25
//
#define  INP_GPIO(x)          pinMode(x, INPUT)
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  BUTTON(x)            !digitalRead(x)
#define  LED(x, y)            digitalWrite(x, y)
#endif

//
// The build process determines RELEASE or DEBUG builds through the
// DEBUG_ASSERT compile-time switch.
//
#ifdef  DEBUG_ASSERT
#define VERSION                     "RpiSlim-v2.13-CR039"
#else   //DEBUG_ASSERT
#define VERSION                     "RpiSlim-v2.14"
#endif  //DEBUG_ASSERT
//
#define DATA_VALID_SIGNATURE        0xDEADBEEF           // Valid signature
#define DATA_VERSION_MAJOR          2
#define DATA_VERSION_MINOR          1
//
#define SEP                         '\t'
#define DEC_POINT                   ','
//
// Target RAM disk for high load file access
//
#define RPI_BASE_NAME               "rpislim"
#define RPI_ERR_FILE                RPI_BASE_NAME ".err"
#define RPI_LOG_FILE                RPI_BASE_NAME ".log"
#define RPI_MAP_FILE                RPI_BASE_NAME ".map"
#define RPI_SHELL_FILE              RPI_BASE_NAME ".txt"
#define RPI_ALL_FILES               RPI_BASE_NAME ".*"
//
#define RPI_WORK_DIR                "/mnt/rpicache/"
#define RPI_RAMFS_DIR               "/mnt/rpicache"
#define RPI_BACKUP_DIR              "/usr/local/share/rpi/"
//
#define RPI_PUBLIC_LOG_PATH         RPI_WORK_DIR RPI_LOG_FILE
#define RPI_ERROR_PATH              RPI_WORK_DIR RPI_ERR_FILE
#define RPI_MAP_PATH                RPI_WORK_DIR RPI_MAP_FILE
#define RPI_MAP_NEWPATH             RPI_WORK_DIR RPI_BASE_NAME "new.map"
//
#define RPI_DL_DIR                  "download/"
#define RPI_CSV_FILE                "%s" RPI_DL_DIR "SmartE_%s.csv"
#define RPI_FLT_FILE                "%s" RPI_DL_DIR "SmartE_%s.bin"
//
#define RPI_BACKUP_DIR              "/usr/local/share/rpi/"
#define RPI_PUBLIC_WWW              "/home/public/www/"
//
#define RPI_PUBLIC_DEFAULT          "index.html"
#define RPI_LOCALHOST               "www.patrn.nl"
#define RPI_WATCHDOG                "rpidog"

#define THE_APP                     "KAIFA MA105C Smart-E-Meter"

#endif /* _CONFIG_H_ */

