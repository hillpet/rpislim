/*  (c) Copyright:  2017..2018  Patrn, Confidential Data 
 *
 *  Workfile:           rpi_slim.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Handling dynamic HTTP pages for a Smart-E-meter
 *                      Smart-E-meter  :  KAIFA MA105C
 *                      Settings       :  115200 baud, 7 bits, even parity
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       
 *    22 Okt 2017: Created
 *    25 Mar 2018: Change Full scale (VS=..) to /sd
 *    21 Jul 2018: slim_CalculateGasAverage() creates average per QTR
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sched.h>
#include <errno.h>
#include <time.h>
#include <termios.h>
#include <unistd.h>
//
#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_http.h"
#include "rpi_page.h"
#include "rpi_html.h"
#include "rpi_json.h"
#include "rpi_func.h"
#include "rpi_slim.h"

#define USE_PRINTF
#include "printf.h"

//
// External HTTP data
//
extern const char *pcHttpResponseHeader;
extern const char *pcWebPageStart;
extern const char *pcWebPageCenter;
extern const char *pcWebPageLeft;
extern const char *pcWebPageStopBad;
extern const char *pcWebPageDefault;
extern const char *pcWebPageFontStart;
extern const char *pcWebPageFontEnd;

extern const char *pcWebPageNotImplemented;
extern const char *pcWebPageLineBreak;
extern const char *pcWebPageEnd;
//
// Misc tex strings/legends
//
static const char  *pcWebPageHdr           = "<h3>%s</h3>";
static const char  *pcWebPageLink          = "&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"%s\">%s</a>";
static const char  *pcLinkDay              = "[Dag Verbruik]";
static const char  *pcLinkSmart            = "[Dag Overzicht]";
static const char  *pcLinkPrevDay          = "[Vorige Dag]";
static const char  *pcLinkNextDay          = "[Volgende Dag]";
static const char  *pcLinkData             = "[Actueel]";
static const char  *pcLinkMonth            = "[Maand Verbruik]";
static const char  *pcLinkPrevMonth        = "[Vorige Maand]";
static const char  *pcLinkNextMonth        = "[Volgende Maand]";
static const char  *pcLinkScalePlus        = "[Schaal +]";
static const char  *pcLinkScaleMin         = "[Schaal -]";
static const char  *pcCourierNew           = "Courier New";
//
static const char  *pcWebPageTitleSlim     = "PATRN Smart-E monitor";
static const char  *pcWebPageTitleMonth    = "PATRN Smart-E monitor - Month";
static const char  *pcWebPageTitleDay      = "PATRN Smart-E monitor - Day";
static const char  *pcSmartLegendQtr       = "**************************************************************************************************"; 
static const char  *pcSmartLegendHrs1      = "*0...0...0...0...0...0...0...0...0...0...1...1...1...1...1...1...1...1...1...1...2...2...2...2...*"; 
static const char  *pcSmartLegendHrs2      = "*0...1...2...3...4...5...6...7...8...9...0...1...2...3...4...5...6...7...8...9...0...1...2...3...*"; 
static const char  *pcSmartLegendSun1      = "*................................................................................................*"; 
//
static const char  *pcSmartLegendDay1      = "*********************************";
static const char  *pcSmartLegendDay2      = "*0........1.........2.........3.*";
static const char  *pcSmartLegendDay3      = "*1...5....0....5....0....5....0.*";
//
static const char *pcSmartStuffTotal[NUM_ALL_EMS]  =
{
   "m3",                         // EMS_GAS
   "kWh",                        // EMS_T1P
   "kWh",                        // EMS_T1M
   "kWh",                        // EMS_T2P
   "kWh",                        // EMS_T2M
   "kWh",                        // EMS_T1 
   "kWh",                        // EMS_T2
   "kWh",                        // EMS_TP
   "kWh",                        // EMS_TM
   "kWh"                         // EMS_T 
};
//
static const char *pcSmartStuffDetail[NUM_ALL_EMS]  =
{
   #ifdef FEATURE_DISPLAY_FULL_SCALE
   "dm3 (Gas)",                     // EMS_GAS
   "Wh (Stroom Laag T1+)",          // EMS_T1P
   "Wh (Stroom Laag T1-)",          // EMS_T1M
   "Wh (Stroom Norm T2+)",          // EMS_T2P
   "Wh (Stroom Norm T2-)",          // EMS_T2M
   "Wh (Totaal Laag)",              // EMS_T1 
   "Wh (Totaal Norm)",              // EMS_T2
   "Wh (Totaal verbruik)",          // EMS_TP
   "Wh (Totaal opgewekt)",          // EMS_TM
   "Wh (Totaal)"                    // EMS_T 
   #else    //FEATURE_DISPLAY_FULL_SCALE
   "dm3/sd (Gas)",                  // EMS_GAS
   "Wh/sd (Stroom Laag T1+)",       // EMS_T1P
   "Wh/sd (Stroom Laag T1-)",       // EMS_T1M
   "Wh/sd (Stroom Norm T2+)",       // EMS_T2P
   "Wh/sd (Stroom Norm T2-)",       // EMS_T2M
   "Wh/sd (Totaal Laag)",           // EMS_T1 
   "Wh/sd (Totaal Norm)",           // EMS_T2
   "Wh/sd (Totaal verbruik)",       // EMS_TP
   "Wh/sd (Totaal opgewekt)",       // EMS_TM
   "Wh/sd (Totaal)"                 // EMS_T 
   #endif   //FEATURE_DISPLAY_FULL_SCALE
};

//
// Scales for all stuff
//
static const double flScales[NUM_SCALES] =
{ 80000.0, 40000.0, 20000.0, 10000.0, 5000.0, 2000.0, 1000.0,  500.0, 200.0, 100.0, 50.0, 20.0, 10.0, 5.0, 2.0, 1.0 };
  
//
// Local prototypes
//
static void       slim_InitDynamicPages         (int);
static bool       slim_DynPageDaySmart          (NETCL *, int);
static bool       slim_DynPageDaySmartPrevHtml  (NETCL *, int);
static bool       slim_DynPageDaySmartNextHtml  (NETCL *, int);
static bool       slim_DynPageDay               (NETCL *, int);
static bool       slim_DynPageDayPrevHtml       (NETCL *, int);
static bool       slim_DynPageDayNextHtml       (NETCL *, int);
static bool       slim_DynPageDayPlusHtml       (NETCL *, int);
static bool       slim_DynPageDayMinHtml        (NETCL *, int);
static bool       slim_DynPageDayGas            (NETCL *, int);
static bool       slim_DynPageDayPower          (NETCL *, int);
static bool       slim_DynPageDaySolar          (NETCL *, int);
static bool       slim_DynPageData              (NETCL *, int);
static bool       slim_DynPageFiles             (NETCL *, int);
static bool       slim_DynPageMonth             (NETCL *, int);
static bool       slim_DynPageMonthPrevHtml     (NETCL *, int);
static bool       slim_DynPageMonthNextHtml     (NETCL *, int);
static bool       slim_DynPageMonthPlusHtml     (NETCL *, int);
static bool       slim_DynPageMonthMinHtml      (NETCL *, int);
//
static double     slim_CalculateSumPeriod       (EMS, OBPER);
static double     slim_CalculateGasAverage      (void);
static void       slim_CopyObisData             (OBPER);
static bool       slim_DynBuildPageDayHtml      (NETCL *);
static bool       slim_DynBuildPageDayGasHtml   (NETCL *);
static bool       slim_DynBuildPageDaySmartHtml (NETCL *);
static bool       slim_DynBuildPageDaySmartJson (NETCL *);
static bool       slim_DynBuildPageDayPowerHtml (NETCL *);
static bool       slim_DynBuildPageDaySolarHtml (NETCL *);
static bool       slim_DynBuildPageMonthHtml    (NETCL *);
static int        slim_AutoScaleSmartData       (double *, int, int);
static void       slim_BuildRowHtml             (NETCL *, char *, char *, char *);
static RPIDATA   *slim_BuildSmartDay            (NETCL *, RPIDATA *, double  *, double, EMS);
static RPIDATA   *slim_BuildSmartData           (RPIDATA *, char *, int, int, double *, int);
static bool       slim_ShowFilesHtml            (NETCL *, char *, char *, const char *);
static double     slim_BuildSolarPotential      (char *, int, EMS);

//
//
// Enums and dynamic LUTs (Global DYN pages)
//
enum
{
   #define  EXTRACT_DYN(a,b,c,d,e,f)   a,
   #include "pages.h"
//
// Enums and dynamic LUTs (Local DYN pages)
//
   #include "smarte.h"
   #undef EXTRACT_DYN
   NUM_SMARTE_DYNS
};
//
// Local DYN pages
//
static const NETDYN stDynamicPages[] =
{
//    tUrl,    tType,   iTimeout,   iFlags,  pcUrl,   pfDynCb;
   #define  EXTRACT_DYN(a,b,c,d,e,f)   {a,b,c,d,e,f},
   #include "smarte.h"
   #undef EXTRACT_DYN
   {  -1,      0,       0,          0,       NULL,    NULL  }
};


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   SLIM_InitDynamicPages
// Purpose:    Init the smart-E meter dynamic pages
// 
// Parameters: Port
// Returns:    
// Note:       
//  
void SLIM_InitDynamicPages(int iPort)
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();
                                                      // SmartMeter readout starts with:
   pstMap->G_ulSmartSecs      = RTC_GetSystemSecs();  //    Today
   pstMap->G_iSmartScaleMonth = 0;                    //    Auto scale Month plot
   pstMap->G_iSmartScaleDay   = 0;                    //    Auto scale Day plot
   //
   slim_InitDynamicPages(iPort);
}

/*------  Local functions separator ------------------------------------
__DYN_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   slim_DynPageFiles
// Purpose:    Handle the dynamic Smart-E files display
//
// Parms:
// Returns:    
// Note:       
//
static bool slim_DynPageFiles(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         // 
         // Put out the csv files with Smart-E data to download
         //        
         fCc = slim_ShowFilesHtml(pstCl, RPI_DL_DIR, pstMap->G_pcFilter, "CSV Data files");
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "smartfiles");
         PRINTF("http_DynPageLog():JSON not implemented yet" CRLF);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageData
// Purpose:    Handle the Smart-E data display
//
// Parms:
// Returns:    
// Note:       
//             Smart-E data
//
//             G_pcSamrtTotals
//             G_pcSmartCsvs
//             G_pcSmartCrcs
//             G_pcSmartTime
//             G_pcT1Plus
//             G_pcT1Min
//             G_pcT2Plus
//             G_pcT2Min
//             G_pcGas
//             G_SmartEquipment
//             G_SmartTariff
//             G_SmartPower
//             G_SmartCurrent
//
static bool slim_DynPageData(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   char    *pcLink;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         // 
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleSlim);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildLineBreaks(pstCl, 2);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
         //
         if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAY)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkDay);
         if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MON)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkMonth);
         if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYSLM))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkSmart);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
         //
         HTTP_BuildLineBreaks(pstCl, 3);
         //
         // Table: line width, width, Nr columns
         //
         HTTP_BuildTableStart(pstCl, "Smart-E-meter", 1, 50, 3);
         //
         HTTP_BuildTableRowStart(pstCl, 20);
         HTTP_BuildTableColumnText(pstCl, "Meter", 10);
         HTTP_BuildTableColumnText(pstCl, "Stand", 20);
         HTTP_BuildTableColumnText(pstCl, "Eenheid",  20);
         HTTP_BuildTableRowEnd(pstCl);
         //
         slim_BuildRowHtml(pstCl, "Gas",  pstMap->G_pcGas,     "m3");
         slim_BuildRowHtml(pstCl, "T1 +", pstMap->G_pcT1Plus,  "kWh");
         slim_BuildRowHtml(pstCl, "T1 -", pstMap->G_pcT1Min,   "kWh");
         slim_BuildRowHtml(pstCl, "T2 +", pstMap->G_pcT2Plus,  "kWh");
         slim_BuildRowHtml(pstCl, "T2 -", pstMap->G_pcT2Min,   "kWh");
         HTTP_BuildTableEnd(pstCl);
         //
         HTTP_BuildLineBreaks(pstCl, 3);
         //
         // Table: line width, width, Nr columns
         HTTP_BuildTableStart(pstCl, "Huidige waarden", 1, 50, 3);
         //
         HTTP_BuildTableRowStart(pstCl, 20);
         HTTP_BuildTableColumnText(pstCl, "Meter", 10);
         HTTP_BuildTableColumnText(pstCl, "Stand", 20);
         HTTP_BuildTableColumnText(pstCl, "Opmerkingen",  10);
         HTTP_BuildTableRowEnd(pstCl);
         //
         slim_BuildRowHtml(pstCl, "Naam",        pstMap->G_pcSmartEquipment,   "Type");
         slim_BuildRowHtml(pstCl, "Totaal",      pstMap->G_pcSmartTotals,      "Samples");
         slim_BuildRowHtml(pstCl, "CSV",         pstMap->G_pcSmartCsvs,        "Samples");
         slim_BuildRowHtml(pstCl, "CRC",         pstMap->G_pcSmartCrcs,        "Errors");
         slim_BuildRowHtml(pstCl, "Tarief",      pstMap->G_pcSmartTariff,      "1=Laag, 2=Normaal");
         slim_BuildRowHtml(pstCl, "Vermogen",    pstMap->G_pcSmartPower,       "kW");
         slim_BuildRowHtml(pstCl, "Stroom",      pstMap->G_pcSmartCurrent,     "A");
         HTTP_BuildTableEnd(pstCl);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "data");
         PRINTF("slim-DynPageData():JSON not implemented yet" CRLF);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageDaySmart
// Purpose:    Handle the dynamic display of Smart-E day results
//
// Parms:
// Returns:    
// Note:       Page layout :
//
//             "  Prev Next"
//             "1000 Gas"
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynPageDaySmart(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   pstMap->G_ulSmartSecs = RTC_GetSystemSecs() - (24*60*60);   //Start yesterday !
   //
   if(!RPI_RestoreDay(pstMap->G_ulSmartSecs, (u_int8 *)pstMap->G_flDispDay, NUM_SAMPLES_DAY * sizeof(double)))
   {
      //
      // No data on file: take today instead
      //
      slim_CopyObisData(OBIS_DAY);
   }
   //
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("slim-DynPageDaySmart():not implemented yet" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("slim-DynPageDaySmart()" CRLF);
         fCc = slim_DynBuildPageDaySmartHtml(pstCl);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "slim");
         fCc = slim_DynBuildPageDaySmartJson(pstCl);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageDaySmartNextHtml
// Purpose:    Handle the dynamic display of Smart-E Next Day results
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageDaySmartNextHtml(NETCL *pstCl, int iIdx)
{
   u_int32  ulSecs, ulSecsNow, ulMidnight;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   PRINTF("slim-DynPageDaySmartNextHtml()" CRLF);
   ulSecs     = pstMap->G_ulSmartSecs;
   ulSecsNow  = RTC_GetSystemSecs();
   ulMidnight = RTC_GetMidnight(ulSecsNow);
   //
   // One day forward
   //
   ulSecs += (24*60*60);
   if(ulSecs >= ulMidnight)
   {
      //
      // Reached this day: show current quarters
      //
      PRINTF("slim-DynPageDaySmartNextHtml():Today" CRLF);
      slim_CopyObisData(OBIS_DAY);
      pstMap->G_ulSmartSecs = ulSecsNow;
   }
   else
   {
      //
      // Restore a day from file
      //
      if(RPI_RestoreDay(ulSecs, (u_int8 *)pstMap->G_flDispDay, NUM_SAMPLES_DAY * sizeof(double)))
      {
         //
         // Successfully restored a day from file
         //
         pstMap->G_ulSmartSecs = ulSecs;
      }
      else
      {
         //
         // Not on file: show empty day
         //
         GEN_MEMSET((u_int8 *)pstMap->G_flDispDay, 0x00, NUM_SAMPLES_DAY * sizeof(double));
         pstMap->G_ulSmartSecs = ulSecs;
      }
   }
   //
   // Display day page
   //
   return(slim_DynBuildPageDaySmartHtml(pstCl));
}

//
// Function:   slim_DynPageDaySmartPrevHtml
// Purpose:    Handle the dynamic display of Smart-E Previous Day results
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageDaySmartPrevHtml(NETCL *pstCl, int iIdx)
{
   u_int32  ulSecs;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   PRINTF("slim-DynPageDaySmartPrevHtml()" CRLF);
   ulSecs = pstMap->G_ulSmartSecs;
   //
   // One day back
   //
   if(ulSecs == 0)
   {
      //
      // Not used before: show today
      //
      PRINTF("slim-DynPageDaySmartPrevHtml():Today" CRLF);
      slim_CopyObisData(OBIS_DAY);
      pstMap->G_ulSmartSecs = RTC_GetSystemSecs();
   }
   else
   {
      //
      // Restore a day from file
      //
      ulSecs -= (24*60*60);
      if(RPI_RestoreDay(ulSecs, (u_int8 *)pstMap->G_flDispDay, NUM_SAMPLES_DAY * sizeof(double)))
      {
         //
         // Day restored correctly
         //
         pstMap->G_ulSmartSecs = ulSecs;
      }
      else
      {
         //
         // Day not on file: show empty day
         //
         GEN_MEMSET((u_int8 *)pstMap->G_flDispDay, 0x00, NUM_SAMPLES_DAY * sizeof(double));
         pstMap->G_ulSmartSecs = ulSecs;
      }

   }
   //
   // Display day page
   //
   return(slim_DynBuildPageDaySmartHtml(pstCl));
}

//
// Function:   slim_DynPageDay
// Purpose:    Handle the dynamic display of Smart-E month results
//
// Parms:
// Returns:    
// Note:       Page layout :
//
//             "  Prev Next"
//             "1000 Gas"
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynPageDay(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("http_DynPageDay():not implemented yet" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("http_DynPageDay()" CRLF);
         slim_CopyObisData(OBIS_DAY);
         pstMap->G_ulSmartSecs = RTC_GetSystemSecs();
         fCc = slim_DynBuildPageDayHtml(pstCl);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "smartday");
         PRINTF("http_DynPageDay():JSON not implemented yet" CRLF);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageDayNextHtml
// Purpose:    Handle the dynamic display of Smart-E Next Day results
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageDayNextHtml(NETCL *pstCl, int iIdx)
{
   u_int32  ulSecs, ulSecsNow, ulMidnight;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   PRINTF("slim-DynPageDayNextHtml()" CRLF);
   ulSecs     = pstMap->G_ulSmartSecs;
   ulSecsNow  = RTC_GetSystemSecs();
   ulMidnight = RTC_GetMidnight(ulSecsNow);
   //
   // One day forward
   //
   ulSecs += (24*60*60);
   if(ulSecs >= ulMidnight)
   {
      //
      // Reached this day: show current quarters
      //
      PRINTF("slim-DynPageDayNextHtml():Today" CRLF);
      slim_CopyObisData(OBIS_DAY);
      pstMap->G_ulSmartSecs = ulSecsNow;
   }
   else
   {
      //
      // Restore a day from file
      //
      if(RPI_RestoreDay(ulSecs, (u_int8 *)pstMap->G_flDispDay, NUM_SAMPLES_DAY * sizeof(double)))
      {
         //
         // Successfully restored a day from file
         //
         pstMap->G_ulSmartSecs = ulSecs;
      }
      else
      {
         //
         // Not on file: show empty day
         //
         GEN_MEMSET((u_int8 *)pstMap->G_flDispDay, 0x00, NUM_SAMPLES_DAY * sizeof(double));
         pstMap->G_ulSmartSecs = ulSecs;
      }
   }
   //
   // Display day page
   //
   return(slim_DynBuildPageDayHtml(pstCl));
}

//
// Function:   slim_DynPageDayPrevHtml
// Purpose:    Handle the dynamic display of Smart-E Previous Day results
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageDayPrevHtml(NETCL *pstCl, int iIdx)
{
   u_int32  ulSecs;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   PRINTF("slim-DynPageDayPrevHtml()" CRLF);
   ulSecs = pstMap->G_ulSmartSecs;
   //
   // One day back
   //
   if(ulSecs == 0)
   {
      //
      // Not used before: show today
      //
      PRINTF("slim-DynPageDayPrevHtml():Today" CRLF);
      slim_CopyObisData(OBIS_DAY);
      pstMap->G_ulSmartSecs = RTC_GetSystemSecs();
   }
   else
   {
      //
      // Restore a day from file
      //
      ulSecs -= (24*60*60);
      if(RPI_RestoreDay(ulSecs, (u_int8 *)pstMap->G_flDispDay, NUM_SAMPLES_DAY * sizeof(double)))
      {
         //
         // Day restored correctly
         //
         pstMap->G_ulSmartSecs = ulSecs;
      }
      else
      {
         //
         // Day not on file: show empty day
         //
         GEN_MEMSET((u_int8 *)pstMap->G_flDispDay, 0x00, NUM_SAMPLES_DAY * sizeof(double));
         pstMap->G_ulSmartSecs = ulSecs;
      }

   }
   //
   // Display day page
   //
   return(slim_DynBuildPageDayHtml(pstCl));
}

//
// Function:   slim_DynPageDayPlusHtml
// Purpose:    Handle the dynamic display of Smart-E scale
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageDayPlusHtml(NETCL *pstCl, int iIdx)
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   PRINTF("slim-DynPageDayPlusHtml()" CRLF);
   //
   // Display day page
   //
   if(pstMap->G_iSmartScaleDay < NUM_SCALES-1) pstMap->G_iSmartScaleDay++;
   return(slim_DynBuildPageDayHtml(pstCl));
}

//
// Function:   slim_DynPageDayMinHtml
// Purpose:    Handle the dynamic display of Smart-E scale
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageDayMinHtml(NETCL *pstCl, int iIdx)
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   PRINTF("slim-DynPageDayMinHtml()" CRLF);
   //
   if(pstMap->G_iSmartScaleDay) pstMap->G_iSmartScaleDay--;
   return(slim_DynBuildPageDayHtml(pstCl));
}

//
// Function:   slim_DynPageDayGas
// Purpose:    Handle the dynamic display of Smart-E results: gas only
//
// Parms:
// Returns:    
// Note:       Page layout :
//
//             "  Prev Next"
//             "1000 Gas"
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynPageDayGas(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("slim-DynPageDayGas():not implemented yet" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("slim-DynPageDayGas()" CRLF);
         slim_CopyObisData(OBIS_DAY);
         pstMap->G_ulSmartSecs = RTC_GetSystemSecs();
         fCc = slim_DynBuildPageDayGasHtml(pstCl);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "gas");
         PRINTF("slim-DynPageDayGas():JSON not implemented yet" CRLF);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageDayPower
// Purpose:    Handle the dynamic display of Smart-E results: power only
//
// Parms:
// Returns:    
// Note:       Page layout :
//
//             "  Prev Next"
//             "1000 Power"
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynPageDayPower(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("slim-DynPageDayPower():not implemented yet" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("slim-DynPageDayPower()" CRLF);
         slim_CopyObisData(OBIS_DAY);
         pstMap->G_ulSmartSecs = RTC_GetSystemSecs();
         fCc = slim_DynBuildPageDayPowerHtml(pstCl);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "power");
         PRINTF("slim-DynPageDayPower():JSON not implemented yet" CRLF);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageDaySolar
// Purpose:    Handle the dynamic display of Smart-E results: solar only
//
// Parms:
// Returns:    
// Note:       Page layout :
//
//             "  Prev Next"
//             "1000 Solar"
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynPageDaySolar(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("slim-DynPageDaySolar():not implemented yet" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("slim-DynPageDaySolar()" CRLF);
         slim_CopyObisData(OBIS_DAY);
         pstMap->G_ulSmartSecs = RTC_GetSystemSecs();
         fCc = slim_DynBuildPageDaySolarHtml(pstCl);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "solar");
         PRINTF("slim-DynPageDaySolar():JSON not implemented yet" CRLF);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageMonth
// Purpose:    Handle the dynamic display of Smart-E month results
//
// Parms:
// Returns:
// Note:       Page layout :
//
//             "  Prev Next"
//             "1000 Gas"
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             "  1   5   10   15   20   25  ..    31"
//
static bool slim_DynPageMonth(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("http_DynPageMonth():not implemented yet" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("http_DynPageMonth()" CRLF);
         slim_CopyObisData(OBIS_MONTH);
         pstMap->G_ulSmartSecs = RTC_GetSystemSecs();
         fCc = slim_DynBuildPageMonthHtml(pstCl);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "smartmonth");
         PRINTF("http_DynPageMonth():JSON not implemented yet" CRLF);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageMonthNextHtml
// Purpose:    Handle the dynamic display of Smart-E Next Month results
//
// Parms:      Client, URL index
// Returns:
// Note:
//
static bool slim_DynPageMonthNextHtml(NETCL *pstCl, int iIdx)
{
   int      iDays;
   u_int32  ulSecs, ulSecsNow, ulMidnight;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   PRINTF("slim-DynPageMonthNextHtml()" CRLF);
   ulSecs     = pstMap->G_ulSmartSecs;
   ulSecsNow  = RTC_GetSystemSecs();
   ulMidnight = RTC_GetMidnight(ulSecsNow);
   //
   // One month forward
   //
   iDays = RTC_GetDaysPerMonth(ulSecs);
   PRINTF1("slim-DynPageMonthNextHtml():This month has %d days" CRLF, iDays);
   //
   ulSecs += (iDays*24*60*60);
   if(ulSecs > ulMidnight)
   {
      //
      // No such month: show this month
      //
      PRINTF("slim-DynPageMonthNextHtml():Month has reached this month" CRLF);
      slim_CopyObisData(OBIS_MONTH);
      pstMap->G_ulSmartSecs = ulSecsNow;
   }
   else
   {
      //
      // Restore a month from file
      //
      if(RPI_RestoreMonth(ulSecs, (u_int8 *)pstMap->G_flDispMonth, NUM_SAMPLES_MONTH * sizeof(double)))
      {
         //
         // Month correctly restored
         //
         PRINTF("slim-DynPageMonthNextHtml():Month on file is OK" CRLF);
         pstMap->G_ulSmartSecs = ulSecs;
      }
      else
      {
         //
         // No such month: show empty month
         //
         GEN_MEMSET((u_int8 *)pstMap->G_flDispMonth, 0x00, NUM_SAMPLES_MONTH * sizeof(double));
         PRINTF("slim-DynPageMonthNextHtml():Month is not on file!" CRLF);
         pstMap->G_ulSmartSecs = ulSecs;
      }
   }
   //
   // Display month page
   //
   return(slim_DynBuildPageMonthHtml(pstCl));
}

//
// Function:   slim_DynPageMonthPrevHtml
// Purpose:    Handle the dynamic display of Smart-E Previous Month results
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageMonthPrevHtml(NETCL *pstCl, int iIdx)
{
   int      iDays;
   u_int32  ulSecs;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   PRINTF("slim-DynPageMonthPrevHtml()" CRLF);
   ulSecs = pstMap->G_ulSmartSecs;
   //
   // One month back
   //
   if(ulSecs == 0)
   {
      //
      // No month displayed yet: show this month
      //
      PRINTF("slim-DynPageMonthPrevHtml():This month" CRLF);
      slim_CopyObisData(OBIS_MONTH);
      pstMap->G_ulSmartSecs = RTC_GetSystemSecs();
   }
   else
   {
      //
      // Restore a month
      //
      iDays = RTC_GetDaysPerMonth(ulSecs);
      //
      ulSecs -= (iDays*24*60*60);
      PRINTF1("slim-DynPageMonthPrevHtml():This month has %d days" CRLF, iDays);
      if(RPI_RestoreMonth(ulSecs, (u_int8 *)pstMap->G_flDispMonth, NUM_SAMPLES_MONTH * sizeof(double)))
      {
         //
         // Month restored correctly
         //
         PRINTF("slim-DynPageMonthPrevHtml():Month on file is OK" CRLF);
         pstMap->G_ulSmartSecs = ulSecs;
      }
      else
      {
         //
         // No such month: display empty month
         //
         GEN_MEMSET((u_int8 *)pstMap->G_flDispMonth, 0x00, NUM_SAMPLES_MONTH * sizeof(double));
         PRINTF("slim-DynPageMonthPrevHtml():Month is not on file!" CRLF);
         pstMap->G_ulSmartSecs = ulSecs;
      }
   }
   //
   // Display day page
   //
   PRINTF("slim-DynPageMonthPrevHtml():Show month now..." CRLF);
   return(slim_DynBuildPageMonthHtml(pstCl));
}

//
// Function:   slim_DynPageMonthPlusHtml
// Purpose:    Handle the dynamic display of Smart-E scale
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageMonthPlusHtml(NETCL *pstCl, int iIdx)
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   PRINTF("slim-DynPageMonthPlusHtml()" CRLF);
   //
   // Display month page
   //
   if(pstMap->G_iSmartScaleMonth < NUM_SCALES-1) pstMap->G_iSmartScaleMonth++;
   return(slim_DynBuildPageMonthHtml(pstCl));
}

//
// Function:   slim_DynPageMonthMinHtml
// Purpose:    Handle the dynamic display of Smart-E scale
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageMonthMinHtml(NETCL *pstCl, int iIdx)
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   PRINTF("slim-DynPageMonthMinHtml()" CRLF);
   //
   // Display month page
   //
   if(pstMap->G_iSmartScaleMonth) pstMap->G_iSmartScaleMonth--;
   return(slim_DynBuildPageMonthHtml(pstCl));
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   slim_CalculateSumPeriod
// Purpose:    Calculate the total energie collected over this period
//
// Parms:      Pointer to all NUM_EMS values, period 
// Returns:    Total
// Note:       Values stored in  G_flThisDay
//                               G_flThisMonth
//             are in 1000*kWh and 1000 * m3
//             This function returns the values back as kWh and m3 !
//
static double slim_CalculateSumPeriod(EMS tEms, OBPER tPer)
{
   int      i, iNr=0;
   double  *pflA=NULL;
   double  *pflB=NULL;
   double  *pflC=NULL;
   double  *pflD=NULL;
   double   flTotal=0.0;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(tEms)
   {
      default:
         break;

      case EMS_GAS:
         if(tPer == OBIS_DAY)   
         { 
            pflA = pstMap->G_flDispDay[EMS_GAS];   
            iNr  = MAX_QRTS;
         }
         else if(tPer == OBIS_MONTH) 
         { 
            pflA = pstMap->G_flDispMonth[EMS_GAS]; 
            iNr  = MAX_DAYS;
         }
         break;

      case EMS_T1P:
         if(tPer == OBIS_DAY)   
         { 
            pflA = pstMap->G_flDispDay[EMS_T1P];   
            iNr  = MAX_QRTS;
         }
         else if(tPer == OBIS_MONTH) 
         { 
            pflA = pstMap->G_flDispMonth[EMS_T1P]; 
            iNr  = MAX_DAYS;
         }
         break;

      case EMS_T1M:
         if(tPer == OBIS_DAY)   
         { 
            pflA = pstMap->G_flDispDay[EMS_T1M];   
            iNr = MAX_QRTS;
         }
         else if(tPer == OBIS_MONTH) 
         { 
            pflA = pstMap->G_flDispMonth[EMS_T1M]; 
            iNr  = MAX_DAYS;
         }
         break;

      case EMS_T2P:
         if(tPer == OBIS_DAY)   
         { 
            pflA = pstMap->G_flDispDay[EMS_T2P];   
            iNr  = MAX_QRTS;
         }
         else if(tPer == OBIS_MONTH) 
         { 
            pflA = pstMap->G_flDispMonth[EMS_T2P]; 
            iNr  = MAX_DAYS;
         }
         break;

      case EMS_T2M:
         if(tPer == OBIS_DAY)   
         { 
            pflA = pstMap->G_flDispDay[EMS_T2M];   
            iNr  = MAX_QRTS;
         }
         else if(tPer == OBIS_MONTH) 
         { 
            pflA = pstMap->G_flDispMonth[EMS_T2M]; 
            iNr  = MAX_DAYS;
         }
         break;

      case EMS_T1:
         if(tPer == OBIS_DAY)   
         { 
            pflA = pstMap->G_flDispDay[EMS_T1P];   
            pflB = pstMap->G_flDispDay[EMS_T1M];
            iNr  = MAX_QRTS;
         }
         else if(tPer == OBIS_MONTH) 
         { 
            pflA = pstMap->G_flDispMonth[EMS_T1P]; 
            pflB = pstMap->G_flDispMonth[EMS_T1M]; 
            iNr  = MAX_DAYS;
         }
         break;

      case EMS_T2:
         if(tPer == OBIS_DAY)   
         { 
            pflA = pstMap->G_flDispDay[EMS_T2P];   
            pflB = pstMap->G_flDispDay[EMS_T2M];
            iNr  = MAX_QRTS;
         }
         else if(tPer == OBIS_MONTH) 
         { 
            pflA = pstMap->G_flDispMonth[EMS_T2P]; 
            pflB = pstMap->G_flDispMonth[EMS_T2M];
            iNr  = MAX_DAYS;
         }
         break;

      case EMS_TP:
         if(tPer == OBIS_DAY)   
         { 
            pflA = pstMap->G_flDispDay[EMS_T1P];
            pflB = pstMap->G_flDispDay[EMS_T2P];   
            iNr  = MAX_QRTS;
         }
         else if(tPer == OBIS_MONTH) 
         { 
            pflA = pstMap->G_flDispMonth[EMS_T1P]; 
            pflB = pstMap->G_flDispMonth[EMS_T2P];
            iNr  = MAX_DAYS;
         }
         break;

      case EMS_TM:
         if(tPer == OBIS_DAY)   
         { 
            pflA = pstMap->G_flDispDay[EMS_T1M];   
            pflB = pstMap->G_flDispDay[EMS_T2M];
            iNr  = MAX_QRTS;
         }
         else if(tPer == OBIS_MONTH) 
         { 
            pflA = pstMap->G_flDispMonth[EMS_T1M]; 
            pflB = pstMap->G_flDispMonth[EMS_T2M];
            iNr  = MAX_DAYS;
         }
         break;

      case EMS_T:
         if(tPer == OBIS_DAY)   
         { 
            pflA = pstMap->G_flDispDay[EMS_T1P];
            pflB = pstMap->G_flDispDay[EMS_T1M];   
            pflC = pstMap->G_flDispDay[EMS_T2P];   
            pflD = pstMap->G_flDispDay[EMS_T2M];   
            iNr  = MAX_QRTS;
         }
         else if(tPer == OBIS_MONTH) 
         { 
            pflA = pstMap->G_flDispMonth[EMS_T1P]; 
            pflB = pstMap->G_flDispMonth[EMS_T1M]; 
            pflC = pstMap->G_flDispMonth[EMS_T2P]; 
            pflD = pstMap->G_flDispMonth[EMS_T2M]; 
            iNr  = MAX_DAYS;
         }
         break;
   }
   for(i=0; i<iNr; i++)
   {
      if(pflA) flTotal += pflA[i];
      if(pflB) flTotal += pflB[i];
      if(pflC) flTotal += pflC[i];
      if(pflD) flTotal += pflD[i];
   }
   //
   // Sum totals are in kWh / m3
   //
   flTotal /= (double)1000.0;
   PRINTF2("slim-CalculateSumPeriod():Ems-%d=%5.3f" CRLF, tEms, flTotal);
   return(flTotal);
}

//
// Function:   slim_CalculateGasAverage
// Purpose:    Gas samples come by the burst: average over a certain period per day
//
// Parms:      Pointer to all NUM_EMS values 
// Returns:    Total usage this day
// Note:       Returned value is in dm3 !
//
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |       X                          |" 
//             " |       X   X   X   X              |"
//             " |       X   X   X   X              |"
//             " |       X   X   X   X              |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static double slim_CalculateGasAverage(void)
{
   int      iQtr, x, iAverage;
   double  *pflVar;
   double  *pflTemp;
   double   flTotal=0.0;
   double   flTemp=0.0;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //
   // G_iGasPeriod (= users G_pcGasPeriod) determines the amount of quarters to
   // average the Gas usage (default = 4 Qtrs). It MUST be a whole divisor of 24,
   // so one of [1, 2, 3, 4, 6, 8, 12, 24].
   //
   iAverage = pstMap->G_iGasPeriod;
   PRINTF1("slim-CalculateGasAverage():Avg=%d" CRLF, iAverage);
   //
   if(iAverage && (24 % iAverage == 0) )
   {
      pflVar  = pstMap->G_flDispDay[EMS_GAS];
      pflTemp = pflVar;
      //
      for(iQtr=0; iQtr< MAX_QRTS; iQtr++)
      {
         flTotal += *pflVar;
         if(iQtr % iAverage == 0) 
         {
            // 1st sample of this period
            pflTemp = pflVar;
            flTemp  = *pflVar++;
            PRINTF1("slim-CalculateGasAverage():Start at Qtr=%d" CRLF, iQtr);
         }
         else flTemp += *pflVar++;
         //
         if( (iQtr % iAverage) == (iAverage - 1) )
         {
            // Last sample of this period: average and update the G_flDispDay[EMS_GAS] buffer
            // flTemp = SUM of iAverage Quarters
            //
            flTemp /= (double) iAverage;
            for(x=0; x<iAverage; x++) *pflTemp++ = flTemp;
            PRINTF2("slim-CalculateGasAverage():Qtr=%d, Avg=%5.1f" CRLF, iQtr, flTemp);
         }
      }
   }
   return(flTotal);
}

//
// Function:   slim_CopyObisData
// Purpose:    Copy Flt.Point data from source to display buffer
//
// Parms:      OBIS period (OBIS_QUARTER, ...)
// Returns:    
// Note:       
//
static void slim_CopyObisData(OBPER tPer)
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   switch(tPer)
   {
      default:
         break;

      case OBIS_DAY:
         GEN_MEMCPY((u_int8 *)pstMap->G_flDispDay, (u_int8 *)pstMap->G_flThisDay, (NUM_SAMPLES_DAY) * sizeof(double));
         break;

      case OBIS_MONTH:
         GEN_MEMCPY((u_int8 *)pstMap->G_flDispMonth, (u_int8 *)pstMap->G_flThisMonth, (NUM_SAMPLES_MONTH) * sizeof(double));
         break;
   }
}

//
// Function:   slim_DynBuildPageDayHtml
// Purpose:    Build the dynamic display of Smart-E day
//
// Parms:
// Returns:    
// Note:       Page layout :
//
//             "  Prev Next"
//             "1000 Gas"
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynBuildPageDayHtml(NETCL *pstCl)
{
   bool     fCc=FALSE;
   int      tEms, iLen;
   double  *pflVar, flTotal;
   char    *pcLink, *pcDate;
   RPIDATA *pstObj=HTML_InsertData(NULL, NULL, 0);
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   PRINTF("slim-DynBuildPageDayHtml()" CRLF);
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleDay);
   HTTP_BuildLineBreaks(pstCl, 2);
   //
   // Show the date/time of this graph
   //
   iLen     = RTC_ConvertDateTimeSize(TIME_FORMAT_WW_DD_MMM_YYYY) + 32;
   pcDate   = safemalloc(iLen);
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY, pcDate, pstMap->G_ulSmartSecs);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildGeneric(pstCl, pcWebPageHdr, pcDate);
   HTTP_BuildLineBreaks(pstCl, 1);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
   //
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYMIN))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScaleMin);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYPLS))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScalePlus);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYPRV))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkPrevDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYNXT))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkNextDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MON)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkMonth);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DATA)))   HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkData);
   //
   HTTP_BuildLineBreaks(pstCl, 3);
   //
   slim_CalculateGasAverage();
   //
   for(tEms=0; tEms<NUM_EMS; tEms++)
   {
      flTotal = slim_CalculateSumPeriod(tEms, OBIS_DAY);
      pflVar  = pstMap->G_flDispDay[tEms];
      pstObj  = slim_BuildSmartDay(pstCl, pstObj, pflVar, flTotal, tEms);
   }
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
   HTTP_BuildEnd(pstCl);
   HTML_ReleaseObject(pstObj);
   safefree(pcDate);
   return(fCc);
}

//
// Function:   slim_DynBuildPageDayGasHtml
// Purpose:    Build the dynamic display of Smart-E day gas only
//
// Parms:
// Returns:    
// Note:       Page layout :
//
//             "  Prev Next"
//             "1000 Gas"
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynBuildPageDayGasHtml(NETCL *pstCl)
{
   bool     fCc=FALSE;
   int      iLen;
   double  *pflVar, flTotal;
   char    *pcLink, *pcDate;
   RPIDATA *pstObj=HTML_InsertData(NULL, NULL, 0);
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   PRINTF("slim-DynBuildPageDayGasHtml()" CRLF);
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleDay);
   HTTP_BuildLineBreaks(pstCl, 2);
   //
   // Show the date/time of this graph
   //
   iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_WW_DD_MMM_YYYY) + 32;
   pcDate = safemalloc(iLen);
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY, pcDate, pstMap->G_ulSmartSecs);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildGeneric(pstCl, pcWebPageHdr, pcDate);
   HTTP_BuildLineBreaks(pstCl, 1);
   //
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYMIN))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScaleMin);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYPLS))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScalePlus);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYPRV))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkPrevDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYNXT))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkNextDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MON)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkMonth);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DATA)))   HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkData);
   //
   HTTP_BuildLineBreaks(pstCl, 3);
   HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
   //
   slim_CalculateGasAverage();
   flTotal = slim_CalculateSumPeriod(EMS_GAS, OBIS_DAY);
   pflVar  = pstMap->G_flDispDay[EMS_GAS];
   pstObj  = slim_BuildSmartDay(pstCl, pstObj, pflVar, flTotal, EMS_GAS);
   HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
   HTTP_BuildEnd(pstCl);
   HTML_ReleaseObject(pstObj);
   safefree(pcDate);
   return(fCc);
}

//
// Function:   slim_DynBuildPageDaySmartHtml
// Purpose:    Build the dynamic display of Smart-E day
//
// Parms:
// Returns:    
// Note:       Page layout :
//
//             "  Prev Next"
//             "1000 Power"
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynBuildPageDaySmartHtml(NETCL *pstCl)
{
   bool     fCc=FALSE;
   int      i, iLen;
   double  *pflT1;
   double  *pflT2;
   double  *pflGas, flTotal;
   double  *pflStr;
   char    *pcLink, *pcDate;
   RPIDATA *pstObj=HTML_InsertData(NULL, NULL, 0);
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   PRINTF("slim-DynBuildPageDaySmartHtml()" CRLF);
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleDay);
   HTTP_BuildLineBreaks(pstCl, 2);
   //
   // Show the date/time of this graph
   //
   iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_WW_DD_MMM_YYYY) + 32;
   pcDate = safemalloc(iLen);
   pflStr = safemalloc(MAX_QRTS * sizeof(double));
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY, pcDate, pstMap->G_ulSmartSecs);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildGeneric(pstCl, pcWebPageHdr, pcDate);
   HTTP_BuildLineBreaks(pstCl, 1);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
   //
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_PRVSLM))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkPrevDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_NXTSLM))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkNextDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MON)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkMonth);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DATA)))   HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkData);
   //
   HTTP_BuildLineBreaks(pstCl, 3);
   //
   // GAS
   //
   slim_CalculateGasAverage();
   flTotal = slim_CalculateSumPeriod(EMS_GAS, OBIS_DAY);
   PRINTF1("slim-DynBuildPageDaySmartHtml():Gas total = %5.1f dm3" CRLF, flTotal);
   pflGas  = pstMap->G_flDispDay[EMS_GAS];
   pstObj  = slim_BuildSmartDay(pstCl, pstObj, pflGas, flTotal, EMS_GAS);
   //
   HTTP_BuildLineBreaks(pstCl, 3);
   //
   // Power SUM(T1+ and T2+)
   //
   pflT1   = pstMap->G_flDispDay[EMS_T1P];
   pflT2   = pstMap->G_flDispDay[EMS_T2P];
   //
   for(i=0; i<MAX_QRTS; i++)
   {
      pflStr[i] = pflT1[i] + pflT2[i];
   }
   flTotal = slim_CalculateSumPeriod(EMS_TP, OBIS_DAY);
   PRINTF1("slim-DynBuildPageDaySmartHtml():Power total = %5.3f kWh" CRLF, flTotal);
   pstObj = slim_BuildSmartDay(pstCl, pstObj, pflStr, flTotal, EMS_TP);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
   HTTP_BuildEnd(pstCl);
   HTML_ReleaseObject(pstObj);
   safefree(pcDate);
   safefree(pflStr);
   return(fCc);
}

//
// Function:   slim_DynBuildPageDaySmartJson
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor
// Returns:    TRUE if OKee
//
static bool slim_DynBuildPageDaySmartJson(NETCL *pstCl)
{
   bool     fCc;
   int      i;
   double  *pflT1;
   double  *pflT2;
   double  *pflGas, flTotal;
   double  *pflStr;
   RPIDATA *pstObj=JSON_InsertParameter(NULL, NULL, NULL, 0);
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   PRINTF("slim-DynBuildPageDaySmartJson()" CRLF);
   //
   pflStr = safemalloc(MAX_QRTS * sizeof(double));
   //
   // Build JSON object
   //
   pstObj = JSON_InsertParameter(pstObj, NULL,        NULL,                         JE_CURLB|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Command",   pstMap->G_pcCommand,          JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Version",   pstMap->G_pcSwVersion,        JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Name",      pstMap->G_pcSmartEquipment,   JE_TEXT|JE_COMMA|JE_CRLF);   
   pstObj = JSON_InsertParameter(pstObj, "Gas",       pstMap->G_pcGas,              JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "T1 +",      pstMap->G_pcT1Plus,           JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "T1 -",      pstMap->G_pcT1Min,            JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "T2 +",      pstMap->G_pcT2Plus,           JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "T2 -",      pstMap->G_pcT2Min,            JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Total",     pstMap->G_pcSmartTotals,      JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Tarif",     pstMap->G_pcSmartTariff,      JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Power",     pstMap->G_pcSmartPower,       JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Current",   pstMap->G_pcSmartCurrent,     JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Graph",     NULL,                         JE_SQRB|JE_CRLF);
   //
   // GAS
   //
   slim_CalculateGasAverage();
   flTotal = slim_CalculateSumPeriod(EMS_GAS, OBIS_DAY);
   PRINTF1("slim-DynBuildPageDaySmartJson():Gas total = %5.3f m3" CRLF, flTotal);
   pflGas  = pstMap->G_flDispDay[EMS_GAS];
   pstObj  = slim_BuildSmartDay(pstCl, pstObj, pflGas, flTotal, EMS_GAS);
   //
   // Power SUM(T1+ and T2+)
   //
   pflT1   = pstMap->G_flDispDay[EMS_T1P];
   pflT2   = pstMap->G_flDispDay[EMS_T2P];
   //
   for(i=0; i<MAX_QRTS; i++)
   {
      pflStr[i] = pflT1[i] + pflT2[i];
   }
   flTotal = slim_CalculateSumPeriod(EMS_TP, OBIS_DAY);
   PRINTF1("slim-DynBuildPageDaySmartJson():Power total = %5.3f kWh" CRLF, flTotal);
   pstObj = slim_BuildSmartDay(pstCl, pstObj, pflStr, flTotal, EMS_TP);
   //
   pstObj = JSON_TerminateArray(pstObj);
   pstObj = JSON_TerminateObject(pstObj);
   fCc    = JSON_RespondObject(pstCl, pstObj);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstObj);
   safefree(pflStr);
   return(fCc);
}

//
// Function:   slim_DynBuildPageDayPowerHtml
// Purpose:    Build the dynamic display of Smart-E day power only
//
// Parms:
// Returns:    
// Note:       Page layout :
//
//             "  Prev Next"
//             "1000 Power"
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynBuildPageDayPowerHtml(NETCL *pstCl)
{
   bool     fCc=FALSE;
   int      i, iLen;
   double  *pflT1;
   double  *pflT2;
   double  *pflStr, flTotal;
   char    *pcLink, *pcDate;
   RPIDATA *pstObj=HTML_InsertData(NULL, NULL, 0);
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   PRINTF("slim-DynBuildPageDayPowerHtml()" CRLF);
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleDay);
   HTTP_BuildLineBreaks(pstCl, 2);
   //
   // Show the date/time of this graph
   //
   iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_WW_DD_MMM_YYYY) + 32;
   pcDate = safemalloc(iLen);
   pflStr = safemalloc(MAX_QRTS * sizeof(double));
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY, pcDate, pstMap->G_ulSmartSecs);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildGeneric(pstCl, pcWebPageHdr, pcDate);
   HTTP_BuildLineBreaks(pstCl, 1);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
   //
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYMIN))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScaleMin);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYPLS))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScalePlus);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYPRV))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkPrevDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYNXT))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkNextDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MON)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkMonth);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DATA)))   HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkData);
   //
   HTTP_BuildLineBreaks(pstCl, 3);
   //
   pflT1   = pstMap->G_flDispDay[EMS_T1P];
   pflT2   = pstMap->G_flDispDay[EMS_T2P];
   //
   for(i=0; i<MAX_QRTS; i++)
   {
      pflStr[i] = pflT1[i] + pflT2[i];
   }
   flTotal = slim_CalculateSumPeriod(EMS_TP, OBIS_DAY);
   PRINTF1("slim-DynBuildPageDayPowerHtml():Power total = %5.3f kWh" CRLF, flTotal);
   pstObj = slim_BuildSmartDay(pstCl, pstObj, pflStr, flTotal, EMS_TP);
   HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
   HTTP_BuildEnd(pstCl);
   HTML_ReleaseObject(pstObj);
   safefree(pcDate);
   safefree(pflStr);
   return(fCc);
}

//
// Function:   slim_DynBuildPageDaySolarHtml
// Purpose:    Build the dynamic display of Smart-E day solar only
//
// Parms:
// Returns:    
// Note:       Page layout :
//
//             "  Prev Next"
//             "1000 Zon"
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynBuildPageDaySolarHtml(NETCL *pstCl)
{
   bool     fCc=FALSE;
   int      i, iLen;
   double  *pflT1;
   double  *pflT2;
   double  *pflStr, flTotal;
   char    *pcLink, *pcDate;
   RPIDATA *pstObj=HTML_InsertData(NULL, NULL, 0);
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   PRINTF("slim-DynBuildPageDaySolarHtml()" CRLF);
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleDay);
   HTTP_BuildLineBreaks(pstCl, 2);
   //
   // Show the date/time of this graph
   //
   iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_WW_DD_MMM_YYYY) + 32;
   pcDate = safemalloc(iLen);
   pflStr = safemalloc(MAX_QRTS * sizeof(double));
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY, pcDate, pstMap->G_ulSmartSecs);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildGeneric(pstCl, pcWebPageHdr, pcDate);
   HTTP_BuildLineBreaks(pstCl, 1);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
   //
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYMIN))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScaleMin);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYPLS))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScalePlus);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYPRV))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkPrevDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYNXT))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkNextDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MON)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkMonth);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DATA)))   HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkData);
   //
   HTTP_BuildLineBreaks(pstCl, 3);
   //
   pflT1   = pstMap->G_flDispDay[EMS_T1M];
   pflT2   = pstMap->G_flDispDay[EMS_T2M];
   //
   for(i=0; i<MAX_QRTS; i++)
   {
      pflStr[i] = pflT1[i] + pflT2[i];
   }
   flTotal = slim_CalculateSumPeriod(EMS_TM, OBIS_DAY);
   PRINTF1("slim-DynBuildPageDaySolarHtml():Power total = %5.3f kWh" CRLF, flTotal);
   pstObj = slim_BuildSmartDay(pstCl, pstObj, pflStr, flTotal, EMS_TM);
   HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
   HTTP_BuildEnd(pstCl);
   HTML_ReleaseObject(pstObj);
   safefree(pcDate);
   safefree(pflStr);
   return(fCc);
}

//
// Function:   slim_DynBuildPageMonthHtml
// Purpose:    Build the dynamic display of Smart-E month results
//
// Parms:
// Returns:
// Note:       Page layout :
//
//             "  Prev Next"
//             "1000 Gas"
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             "  1   5   10   15   20   25  ..    31"
//
static bool slim_DynBuildPageMonthHtml(NETCL *pstCl)
{
   bool     fCc=FALSE;
   int      tEms, iFs;
   double  *pflVar, flTotal;
   char    *pcBuffer, *pcLink, *pcTemp;
   RPIDATA *pstObj=HTML_InsertData(NULL, NULL, 0);
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   PRINTF("slim-DynBuildPageMonthHtml()" CRLF);
   pcBuffer = (char *) safemalloc(MAX_DAYSZ * MAX_LINES);
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleMonth);
   HTTP_BuildLineBreaks(pstCl, 2);
   //
   // Show the date/time of this graph
   //
   pcTemp = safemalloc(256);
   //
   RTC_ConvertDateTime(TIME_FORMAT_AMM_YYYY, pcTemp, pstMap->G_ulSmartSecs);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildGeneric(pstCl, pcWebPageHdr, pcTemp);
   HTTP_BuildLineBreaks(pstCl, 1);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
   //
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MONMIN))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScaleMin);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MONPLS))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScalePlus);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_PRVMON))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkPrevMonth);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_NXTMON))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkNextMonth);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAY)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DATA)))   HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkData);
   //
   HTTP_BuildLineBreaks(pstCl, 3);
   //
   for(tEms=0; tEms<NUM_EMS; tEms++)
   {
      pflVar = pstMap->G_flDispMonth[tEms];
      iFs    = pstMap->G_iSmartScaleMonth;
      if(iFs >= NUM_SCALES)
      {
         PRINTF2("slim-DynBuildPageMonthHtml():Scale index %d too large (%s)" CRLF, iFs, pcSmartStuffDetail[tEms]);
         iFs = pstMap->G_iSmartScaleMonth = 0;
      }
      //
      // HTTP_BuildGeneric() is NOT good at formatted float (or double) conversions ! It will do basic formatting
      // ("%f"  -->%f) and ("%F" -->%5.3f) on double precision floating point numbers only.
      //
      //    So, i.o. HTTP_BuildGeneric(..., "%5.1f", flXxxx) convert float to string and use
      //             HTTP_BuildGeneric(..., "%s", pcXxxx) 
      //
      // Example pcTemp:
      // "Totaal: 14.0 kWh    (Auto)VS=500.00 Wh T1"
      //    or
      // "Totaal: 14.0 kWh    (Auto)500.00 Wh/sd T1"
      //
      flTotal = slim_CalculateSumPeriod(tEms, OBIS_MONTH);
      if(iFs) 
      {
         #ifdef FEATURE_DISPLAY_FULL_SCALE
         GEN_SNPRINTF(pcTemp, 255, "Totaal %5.3f %s &nbsp;&nbsp;&nbsp;&nbsp; VS=%5.2f %s",
                        flTotal, pcSmartStuffTotal[tEms], flScales[iFs], pcSmartStuffDetail[tEms]);
         #else    //FEATURE_DISPLAY_FULL_SCALE
         GEN_SNPRINTF(pcTemp, 255, "Totaal %5.3f %s &nbsp;&nbsp;&nbsp;&nbsp; %5.2f %s",
                        flTotal, pcSmartStuffTotal[tEms], flScales[iFs]/MAX_LINES, pcSmartStuffDetail[tEms]);
         #endif   //FEATURE_DISPLAY_FULL_SCALE
      }
      else    
      {
         iFs = slim_AutoScaleSmartData(pflVar, MAX_DAYSZ, iFs);
         #ifdef FEATURE_DISPLAY_FULL_SCALE
         GEN_SNPRINTF(pcTemp, 255, "Totaal %5.3f %s &nbsp;&nbsp;&nbsp;&nbsp; (Auto)VS=%5.2f %s",
                        flTotal, pcSmartStuffTotal[tEms], flScales[iFs], pcSmartStuffDetail[tEms]);
         #else    //FEATURE_DISPLAY_FULL_SCALE
         GEN_SNPRINTF(pcTemp, 255, "Totaal %5.3f %s &nbsp;&nbsp;&nbsp;&nbsp; (Auto)%5.2f %s",
                        flTotal, pcSmartStuffTotal[tEms], flScales[iFs]/MAX_LINES, pcSmartStuffDetail[tEms]);
         #endif   //FEATURE_DISPLAY_FULL_SCALE
      }
      //
      HTTP_BuildGeneric(pstCl, "%s", pcTemp);
      //
      pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendDay1, HE_SPACE4|HE_BR);
      pstObj = slim_BuildSmartData(pstObj, pcBuffer, MAX_DAYSZ, MAX_LINES, pflVar, iFs);
      //
      pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendDay1, HE_SPACE4|HE_BR);
      pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendDay2, HE_SPACE4|HE_BR);
      pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendDay3, HE_SPACE4|HE_BR);
      //
      // Put out HTML graph
      //
      HTTP_BuildLineBreaks(pstCl, 1);
      HTML_RespondObject(pstCl, pstObj);
      HTTP_BuildLineBreaks(pstCl, 3);
   }
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
   HTTP_BuildEnd(pstCl);
   HTML_ReleaseObject(pstObj);
   safefree(pcTemp);
   safefree(pcBuffer);
   return(fCc);
}

//
// Function:   slim_BuildRowHtml
// Purpose:    Build a singe row with text
// 
// Parameters: Client, text 1, text2, text3
// Returns:    
// Note:       
//
static void slim_BuildRowHtml(NETCL *pstCl, char *pcText1, char *pcText2, char *pcText3)
{
   HTTP_BuildTableRowStart(pstCl, 10);
   HTTP_BuildTableColumnText(pstCl, pcText1, 10);
   HTTP_BuildTableColumnText(pstCl, pcText2, 10);
   HTTP_BuildTableColumnText(pstCl, pcText3, 10);
   HTTP_BuildTableRowEnd(pstCl);
}

// 
// Function:   slim_AutoScaleSmartData
// Purpose:    Auto scale this plot, if requiered
// 
// Parameters: Values ptr, Number of values, ScaleIdx
// Returns:    Scale
// Note:       
//
static int slim_AutoScaleSmartData(double *pflVal, int iNr, int iFs)
{
   bool     fOvfl=TRUE;
   int      x;

   //
   // If AUTO scale: find best scale
   //
   if(iFs == 0)
   {
      iFs = NUM_SCALES;
      while(iFs && fOvfl)
      {
         iFs--;
         //
         // Check if any sample overflows the graph, if so try next scale
         //
         fOvfl = FALSE;
         for(x=0; x<iNr-1; x++)
         {
            if(pflVal[x] > flScales[iFs]) 
            {
               fOvfl = TRUE;
               break;
            }
         }
      }
   }
   return(iFs);
}

// 
// Function:   slim_BuildSmartDay
// Purpose:    Build HTLML data for a single day
// 
// Parameters: NetCl, Object, FP data array, total this period, EMS type
// Returns:    Object
// Note:       Object->tType knows HTML or JSON
//
static RPIDATA *slim_BuildSmartDay(NETCL *pstCl, RPIDATA *pstObj, double  *pflVar, double flTotal, EMS tEms)
{
   int      iFs;
   double   flSunPot;
   char    *pcBuffer, *pcTemp;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   pcTemp   = safemalloc(256);
   pcBuffer = safemalloc(MAX_QRTSZ * MAX_LINES);
   //
   iFs = pstMap->G_iSmartScaleDay;
   if(iFs >= NUM_SCALES)
   {
      PRINTF2("slim-BuildSmartDay():Scale index %d too large (%s)" CRLF, iFs, pcSmartStuffDetail[tEms]);
      iFs = pstMap->G_iSmartScaleDay = 0;
   }
   PRINTF3("slim-BuildSmartDay():Scale[%d]=%5.1f %s" CRLF, iFs, flScales[iFs], pcSmartStuffDetail[tEms]);
   //
   // HTTP_BuildGeneric() is NOT good at formatted float (or double) conversions ! It will do basic formatting
   // ("%f"  -->%f) and ("%F" -->%5.3f) on double precision floating point numbers only.
   //
   //    So, i.o. HTTP_BuildGeneric(..., "%5.1f", flXxxx) convert float to string ansd use
   //             HTTP_BuildGeneric(..., "%s", pcXxxx) 
   //
   if(iFs) 
   {
      #ifdef FEATURE_DISPLAY_FULL_SCALE
      GEN_SNPRINTF(pcTemp, 255, "Totaal %5.3f %s &nbsp;&nbsp;&nbsp;&nbsp; VS=%5.2f %s",
                     flTotal, pcSmartStuffTotal[tEms], flScales[iFs], pcSmartStuffDetail[tEms]);
      #else    //FEATURE_DISPLAY_FULL_SCALE
      GEN_SNPRINTF(pcTemp, 255, "Totaal %5.3f %s &nbsp;&nbsp;&nbsp;&nbsp; %5.2f %s",
                     flTotal, pcSmartStuffTotal[tEms], flScales[iFs]/MAX_LINES, pcSmartStuffDetail[tEms]);
      #endif   //FEATURE_DISPLAY_FULL_SCALE
   }
   else    
   {
      iFs = slim_AutoScaleSmartData(pflVar, MAX_QRTSZ, iFs);
      #ifdef FEATURE_DISPLAY_FULL_SCALE
      GEN_SNPRINTF(pcTemp, 255, "Totaal %5.3f %s &nbsp;&nbsp;&nbsp;&nbsp; (Auto)VS=%5.2f %s",
                     flTotal, pcSmartStuffTotal[tEms], flScales[iFs], pcSmartStuffDetail[tEms]);
      #else    //FEATURE_DISPLAY_FULL_SCALE
      GEN_SNPRINTF(pcTemp, 255, "Totaal %5.3f %s &nbsp;&nbsp;&nbsp;&nbsp; (Auto)%5.2f %s",
                     flTotal, pcSmartStuffTotal[tEms], flScales[iFs]/MAX_LINES, pcSmartStuffDetail[tEms]);
      #endif   //FEATURE_DISPLAY_FULL_SCALE
   }
   switch(pstObj->tType)
   {
      default:
      case HTTP_HTML:
         HTTP_BuildGeneric(pstCl, "%s", pcTemp);
         //
         pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendQtr,  HE_SPACE4|HE_BR);
         pstObj = slim_BuildSmartData(pstObj, pcBuffer, MAX_QRTSZ,   MAX_LINES, pflVar, iFs);
         pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendQtr,  HE_SPACE4|HE_BR);
         pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendHrs1, HE_SPACE4|HE_BR);
         pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendHrs2, HE_SPACE4|HE_BR);
         //
         // May need to add the potential Solar kwh here
         //
         flSunPot = slim_BuildSolarPotential(pcBuffer, MAX_QRTS * MAX_LINES, tEms);
         if(flSunPot > 0.0)
         {
            pstObj   = HTML_InsertData(pstObj, (char *)pcBuffer, HE_SPACE4|HE_BR);
            //
            GEN_SNPRINTF(pcBuffer, MAX_QRTS, "Zon Potentiaal=%5.3f kWh", flSunPot);
            pstObj   = HTML_InsertData(pstObj, (char *)pcBuffer, HE_SPACE4|HE_BR);
         }
         //
         // Put out HTML graph
         //
         HTTP_BuildLineBreaks(pstCl, 1);
         HTML_RespondObject(pstCl, pstObj);
         HTTP_BuildLineBreaks(pstCl, 3);
         break;

      case HTTP_JSON:
         pstObj = slim_BuildSmartData(pstObj, pcBuffer, MAX_QRTSZ, MAX_LINES, pflVar, iFs);
         break;
   }
   safefree(pcTemp);
   safefree(pcBuffer);
   return(pstObj);
}

// 
// Function:   slim_BuildSmartData
// Purpose:    Build data
// 
// Parameters: Object, Buffer, Buffer size, num lines, double ptr, ScaleIdx
// Returns:    Object
// Note:       Object->tType knows HTML or JSON
//
static RPIDATA *slim_BuildSmartData(RPIDATA *pstObj, char *pcBuffer, int iSize, int iMaxLines, double *pflVal, int iFs)
{
   int      x, iIdx, iLine;
   char     cIcon;
   char     cLedgeY[4];
   double   flRes, flFs, flVal, flTh;

   //
   // iFs has the correct value for this plot
   //
   flFs  = flScales[iFs];
   flRes = flFs / iMaxLines;
   PRINTF1("slim-BuildSmartData():scale=%5.1f" CRLF, flRes);
   //
   // Buffer hold records of symbols + '\0': Preserve the ASCIIZ terminator !
   //
   for(x=0; x<iSize-1; x++)
   {
      iIdx  = x;
      flVal = pflVal[x];
      if(flVal > flFs) cIcon = 'O';
      else             cIcon = 'X';
      //
      flTh = (double)0.0;
      for(iLine=0; iLine<iMaxLines; iLine++)
      {
         if(flVal > flTh) pcBuffer[iIdx] = cIcon;
         else             pcBuffer[iIdx] = '.';
         flTh += flRes;
         iIdx += iSize;
      }
   }
   //
   // Put out buffer line by line (buffer is mirrored)
   //
   switch(pstObj->tType)
   {
      default:
      case HTTP_HTML:
         do
         {
            GEN_SNPRINTF(cLedgeY, 3, "%02d", iLine);
            iLine--;
            pstObj = HTML_InsertData(pstObj, cLedgeY, 0);
            pstObj = HTML_InsertData(pstObj, &pcBuffer[iLine*iSize], HE_SPACE2|HE_VLS|HE_VLE|HE_BR);
         }
         while(iLine);
         break;

      case HTTP_JSON:
         do
         {
            iLine--;
            pstObj = JSON_InsertParameter(pstObj, NULL, &pcBuffer[iLine*iSize], JE_TEXT|JE_COMMA|JE_CRLF);
         }
         while(iLine);
         break;
   }
   return(pstObj);
}

// 
// Function:   slim_InitDynamicPages
// Purpose:    Register dynamic webpages
// 
// Parameters: Port
// Returns:    
// Note:       
//
static void slim_InitDynamicPages(int iPort)
{
   const NETDYN  *pstDyn=stDynamicPages;

   PRINTF1("slim-InitDynamicPages(): port=%d" CRLF, iPort);
   //
   while(pstDyn->pcUrl)
   {
      //PRINTF1("slim-InitDynamicPages():Url=%s" CRLF, pstDyn->pcUrl);
      if(pstDyn->iFlags & DYN_FLAG_PORT)
      {
         //
         // Register for this specific port
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout, iPort, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      else
      {
         //
         // Register for all ports
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout,     0, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      pstDyn++;
   }
}

//
// Function:   slim_ShowFilesHtml
// Purpose:    Show files in directory from disk 
// 
// Parameters: Client, dir name, filter, header
// Returns:    TRUE if OKee
// Note:       
//
static bool slim_ShowFilesHtml(NETCL *pstCl, char *pcDir, char *pcFilter, const char *pcHeader)
{
   bool     fCc=FALSE;
   int      x, iNum=0;
   char     cForD;
   char    *pcFileName;
   char    *pcFullPath;
   char    *pcFilePath;
   char    *pcTemp;
   void    *pvData;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   pcFileName = (char *) safemalloc(MAX_PATH_LEN);
   pcFilePath = (char *) safemalloc(MAX_PATH_LEN);
   pcFullPath = (char *) safemalloc(MAX_PATH_LEN);
   pcTemp     = (char *) safemalloc(MAX_PATH_LEN);
   //
   GEN_SPRINTF(pcFullPath, "%s%s", pstMap->G_pcWwwDir, pcDir);
   PRINTF1("slim-ShowFilesHtml(): Path = %s" CRLF, pcFullPath);
   //
   pvData = FINFO_GetDirEntries(pcFullPath, pcFilter, &iNum);
   if(iNum > 0)
   {
      HTTP_BuildRespondHeader(pstCl);
      HTTP_BuildStart(pstCl);
      //
      // Build table
      //
      HTTP_BuildTableStart(pstCl, pcHeader, 1, 70, 3);
      //
      for (x=0; x<iNum; x++)
      {
         FINFO_GetFilename(pvData, x, pcFileName, MAX_PATH_LEN);
         GEN_SPRINTF(pcFilePath, "%s%s", pcFullPath, pcFileName);
         FINFO_GetFileInfo(pcFilePath, FI_FILEDIR, &cForD, 1);
         PRINTF3("slim-ShowFilesHtml(): Filter(%s)-File(%c):[%s]" CRLF, pcFilter, cForD, pcFileName);
         if( cForD == 'F')
         {
            GEN_SPRINTF(pcTemp, "%s%s", pcDir, pcFileName);
            HTTP_BuildTableRowStart(pstCl, 6);
            HTTP_BuildTableColumnLink(pstCl, pcFileName, 25, pcTemp);
            FINFO_GetFileInfo(pcFilePath, FI_SIZE, pcTemp, MAX_PATH_LEN);
            HTTP_BuildTableColumnText(pstCl, pcTemp, 15);
            FINFO_GetFileInfo(pcFilePath, FI_MDATE, pcTemp, MAX_PATH_LEN);
            HTTP_BuildTableColumnText(pstCl, pcTemp, 30);
            HTTP_BuildTableRowEnd(pstCl);
         }
      }
      FINFO_ReleaseDirEntries(pvData, iNum);
      HTTP_BuildTableEnd(pstCl);
      HTTP_BuildLineBreaks(pstCl, 1);
      HTTP_BuildEnd(pstCl);
      fCc = TRUE;
   }
   else
   {
      PRINTF("slim-ShowFilesHtml(): No file/dir entries" CRLF);
      HTTP_BuildRespondHeader(pstCl);
      HTTP_BuildStart(pstCl);
      HTTP_BuildGeneric(pstCl, pcWebPageStopBad);
      HTTP_BuildLineBreaks(pstCl, 1);
      HTTP_BuildEnd(pstCl);
   }
   safefree(pcFileName);
   safefree(pcFilePath);
   safefree(pcFullPath);
   safefree(pcTemp);
   return(fCc);
}

//
// Function:   slim_BuildSolarPotential
// Purpose:    Calculate the amount of kwh we could have generate using solarpanels
//
// Parms:      Buffer ptr, size, EMS
// Returns:    Total amount of potential solar kwh
// Note:       Buffer is filled with the solar period
//             Values stored in G_flThisDay are in 1000*kWh.
//             This function returns the value back as kWh!
//
static double slim_BuildSolarPotential(char *pcBuffer, int iSize, EMS tEms)
{
   int      i, iQsta, iQend;
   double  *pflA=NULL;
   double  *pflB=NULL;
   double   flTotal=0.0;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   GEN_MEMSET(pcBuffer, 0, iSize);
   //
   // Retrieve the quarter start and end
   //
   iQsta = atoi(pstMap->G_pcSunQsta);
   iQend = atoi(pstMap->G_pcSunQend);
   //
   if(iQend >= MAX_QRTS) return(flTotal);
   if(iQsta >= iQend)    return(flTotal);
   //
   GEN_STRNCPY(pcBuffer, pcSmartLegendSun1, iSize);
   for(i=iQsta; i<iQend; i++) pcBuffer[i+1] = 'Z';
   //
   switch(tEms)
   {
      default:
         break;

      case EMS_T1P:
         pflA = pstMap->G_flDispDay[EMS_T1P];   
         break;

      case EMS_T2P:
         pflA = pstMap->G_flDispDay[EMS_T2P];   
         break;

      case EMS_T1:
         pflA = pstMap->G_flDispDay[EMS_T1P];   
         break;

      case EMS_T2:
         pflA = pstMap->G_flDispDay[EMS_T2P];   
         break;

      case EMS_TP:
         pflA = pstMap->G_flDispDay[EMS_T1P];
         pflB = pstMap->G_flDispDay[EMS_T2P];   
         break;

      case EMS_T:
         pflA = pstMap->G_flDispDay[EMS_T1P];
         pflB = pstMap->G_flDispDay[EMS_T2P];   
         break;
   }
   for(i=iQsta; i<iQend; i++)
   {
      if(pflA) flTotal += pflA[i];
      if(pflB) flTotal += pflB[i];
   }
   //
   // Sum totals are in kWh
   //
   flTotal /= (double)1000.0;
   PRINTF2("slim-CalculateSumPeriod():Ems-%d=%5.3f" CRLF, tEms, flTotal);
   return(flTotal);
}

