/*  (c) Copyright:  2017..2018  Patrn, Confidential Data 
 *
 *  Workfile:           rpi_func.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       20 Oct 2017
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_http.h"
#include "rpi_page.h"
#include "rpi_json.h"
//
#include "rpi_func.h"

#define USE_PRINTF
#include "printf.h"

//
// Enums
//
enum
{
#define  EXTRACT_DYN(a,b,c,d,e,f)           a,
#include "pages.h"
#undef EXTRACT_DYN
   NUM_PAGES_DYNS
};
//
typedef bool (*HTTPFUN)(int);
//
typedef struct httpargs
{
   int            iId;
   int            iFunction;
   const char    *pcJson;
   const char    *pcHtml;
   int            iValueOffset;
   int            iValueSize;
   int            iChangedOffset;
   HTTPFUN        pfHttpFun;
}  HTTPARGS;
//
// Prototypes Collect Callback
//
static bool  http_CollectDebug               (int);
static bool  http_CollectRcuKey              (int);
static bool  http_CollectGasPeriod           (int);
//
// Index list to global Arguments G_xxxx
//
static const HTTPARGS stHttpArguments[] =
{
//       a                 b                                c           d        f                             g              h                                      k
//       Enum,             iFunction                        pcJson,     pcHtml   iValueOffset                  iValueSize     iChangedOffset,                        pfHttpFun
//       xxx,              JSN_TXT|VAR_STI|...|VAR____,     "Width",    "w=",    offsetof(RPIMAP, G_pcWidth),  MAX_PARM_LEN,  offsetof(RPIMAP, G_ubWidthChanged),    NULL
//   
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    {a,b,c,d,f,g,h,k},
#include "par_defs.h"
#undef   EXTRACT_PAR
   {     NUM_GLOBAL_DEFS,  0,                               NULL,       NULL,    0,                            0,             0,                                     NULL}
};


//
// Index list to global Rcu/Fp keys
//
static const char *pcRcuKeyLut[] =
{
#define  EXTRACT_KEY(a,b)    b,
#include "key_defs.h"
#undef   EXTRACT_KEY
         NULL
};

//
// Local prototypes
//
static bool       rpi_HandleParm          (char *, FTYPE, int);
static bool       rpi_ParmSetChanged      (const HTTPARGS *, bool);
//
#ifdef  FEATURE_JSON_SHOW_REPLY
static void rpi_ShowNetReply              (const char *, int, char *);
#define SHOW_NET_REPLY(a,b,c)             rpi_ShowNetReply(a,b,c)
#else
#define SHOW_NET_REPLY(a,b,c)
#endif  //FEATURE_JSON_SHOW_REPLY
//
static const char pcWebPageCommandBusy[]  = "<p>Raspicam Error: Ongoing cam action!</p>" NEWLINE;
static const char pcWebPageCommandBad[]   = "<p>Raspicam Error: Unknown command!</p>" NEWLINE;
//
static const char pcFltFile[]             = RPI_FLT_FILE;
static const char pcAmpAnd[]              = "&";
static const char pcParmDelims[]          = "=:";

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   RPI_BuildJsonMessageBody
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, variables mask
// Returns:    TRUE if OKee
//
bool RPI_BuildJsonMessageBody(NETCL *pstCl, int iArgs)
{
   bool     fCc;

   PRINTF("RPI_BuildJsonMessageBody()" CRLF);
   fCc = RPI_BuildJsonMessageArgs(pstCl, iArgs);
   return(fCc);
}

//
// Function:   RPI_BuildJsonMessageArgs
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, Args 
// Returns:    TRUE if OKee
//
bool RPI_BuildJsonMessageArgs(NETCL *pstCl, int iArgs)
{
   bool            fCc;
   char           *pcValue;
   RPIDATA        *pstCam=NULL;
   const HTTPARGS *pstArgs=stHttpArguments;
   RPIMAP         *pstMap=GLOBAL_GetMapping();

   PRINTF1("RPI_BuildJsonMessageArgs():Function %s" CRLF, pstMap->G_pcCommand);
   if(iArgs == 0)
   {
      PRINTF("RPI_BuildJsonMessageArgs():No JSON data to reply!" CRLF);
      iArgs = VAR_ERR;
   }
   //
   // Build JSON object
   //
   pstCam = JSON_InsertParameter(pstCam, NULL, NULL, JE_CURLB|JE_CRLF);
   while(pstArgs->iFunction)
   {
      if(pstArgs->iFunction & iArgs)
      {
         pcValue = (char *)pstMap + pstArgs->iValueOffset;
         //
         // Add all parameters to JSON object
         //
         if(pstArgs->iFunction & JSN_TXT) pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, pcValue, JE_TEXT|JE_COMMA|JE_CRLF);
         else                             pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, pcValue,         JE_COMMA|JE_CRLF);
      }
      pstArgs++;
   }
   pstCam = JSON_TerminateObject(pstCam);
   fCc    = JSON_RespondObject(pstCl, pstCam);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstCam);
   return(fCc);
}

//
// Function:   RPI_CollectParms
// Purpose:    Collect PiCam parameter passing from HTTP-data into the MMAP global area
//
// Parms:      Client area, HTTP-type
// Returns:    Nr of parms found
// Note:       
//             pstCl->pcUrl-> "/all/public/www/cam.json?Command=snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024"
//                             |               |   |    |
//             iPathIdx = -----+               |   |    |
//             iFileIdx = ---------------------+   |    |
//             iExtnIdx = -------------------------+    |
//             iPaylIdx = ------------------------------+
//  
int RPI_CollectParms(NETCL *pstCl, FTYPE tType)
{
   int             iNr=0;
   char           *pcParm;
   const HTTPARGS *pstArgs=stHttpArguments;

   //
   // Set first parameter
   //
   pstCl->iNextIdx = pstCl->iPaylIdx;
   //
   // Before collecting new parms, reset all changed markers
   //
   while(pstArgs->iFunction)
   {
      rpi_ParmSetChanged(pstArgs, 0);
      pstArgs++;
   }

#ifdef FEATURE_HTML_DEFAULT_COMMAND
   //
   // HTML syntax has the command first:
   // so:   cam.html?snapshot&w=400
   // else: cam.html?cmd=snapshot&w=400
   //
   if(tType == HTTP_HTML)
   {
      if( (pcParm = HTTP_CollectGetParameter(pstCl)) )
      {
         iNr++;
         GEN_STRNCPY(GLOBAL_GetMapping()->G_pcCommand, pcParm, MAX_PARM_LEN);
      }
   }
#endif   //FEATURE_HTML_DEFAULT_COMMAND
   //
   // Retrieve and store all arguments
   //
   do
   {
      pcParm = HTTP_CollectGetParameter(pstCl);
      if(pcParm)
      {
         iNr++;
         rpi_HandleParm(pcParm, tType, VAR_ALL);
      }
   }
   while(pcParm);
   //
   PRINTF1("RPI_CollectParms(): %d arguments found" CRLF, iNr);
   return(iNr);
}

//
// Function:   RPI_ReportBusy
// Purpose:    Report that the server thread is busy
//
// Parms:      Socket descriptor
// Returns:    
// Note:       
//             
void RPI_ReportBusy(NETCL *pstCl)
{
   int      iIdx, tType;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   iIdx  = pstMap->G_iCurDynPage;
   tType = GEN_GetDynamicPageType(iIdx);        // HTTP_HTML, HTTP_JSON
   //
   switch(tType)
   {
      default:
      case HTTP_HTML:
         // Use HTML/JS API
         HTTP_BuildRespondHeader(pstCl);        //"HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
         HTTP_BuildStart(pstCl);                // Title - eyecandy
         HTTP_BuildGeneric(pstCl, pcWebPageCommandBusy);
         HTTP_BuildLineBreaks(pstCl, 1);
         HTTP_BuildEnd(pstCl);
         break;
   
      case HTTP_JSON:
         // Use JSON API
         RPI_BuildJsonMessageBody(pstCl, pstMap->G_iHttpArgs);
         break;
   }
}

//
// Function:   RPI_ReportError
// Purpose:    Report error
//
// Parms:      Socket descriptor
// Returns:    
// Note:       
//             
void RPI_ReportError(NETCL *pstCl)
{
   int      iIdx, tType;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   iIdx  = pstMap->G_iCurDynPage;
   tType = GEN_GetDynamicPageType(iIdx);        // HTTP_HTML, HTTP_JSON
   //
   switch(tType)
   {
      default:
      case HTTP_HTML:
         // Use HTML/JS API
         LOG_Report(0, "RPI", "RPI_ReportError():HTML");
         PRINTF("RPI_ReportError():HTML" CRLF);
         HTTP_BuildRespondHeader(pstCl);        //"HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
         HTTP_BuildStart(pstCl);                // Title - eyecandy
         HTTP_BuildGeneric(pstCl, pcWebPageCommandBad);
         HTTP_BuildLineBreaks(pstCl, 1);
         HTTP_BuildEnd(pstCl);
         break;
   
      case HTTP_JSON:
         // Use JSON API
         LOG_Report(0, "RPI", "RPI_ReportError():JSON");
         PRINTF("RPI_ReportError():JSON" CRLF);
         RPI_BuildJsonMessageBody(pstCl, pstMap->G_iHttpArgs);
         break;
   }
}

//
// Function:   RPI_RestoreDay
// Purpose:    Restore a day from file
//
// Parms:      Secs timestamp, Buffer ptr, Buffer size
// Returns:
// Note:
//
bool RPI_RestoreDay(u_int32 ulSecs, u_int8 *pubData, int iSize)
{
   bool     fCc=FALSE;
   int      iLen, tFile;
   char    *pcFile, cSrc;
   char    *pcTemp;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   if(ulSecs)
   {
      //
      // Build bin file "/WwwDir/SmartE_yyymmdd.bin"
      //
      iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM_DD);
      iLen  += 32;
      iLen  += GEN_STRLEN(pstMap->G_pcWwwDir);
      iLen  += GEN_STRLEN(pcFltFile);
      pcFile = safemalloc(iLen);
      pcTemp = safemalloc(iLen);
      //
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD, pcTemp, ulSecs);
      //
      GEN_SNPRINTF(pcFile, iLen, pcFltFile, pstMap->G_pcWwwDir, pcTemp);
      //
      FINFO_GetFileInfo((char *)pcFile, FI_FILEDIR, &cSrc, 1);
      if(cSrc == 'F')
      {
         PRINTF1("slim_RestoreDay():Open %s" CRLF, pcFile);
         tFile = safeopen(pcFile, O_RDONLY);
         if(tFile > 0)
         {
            saferead(tFile, pubData, iSize);
            safeclose(tFile);
            LOG_Report(0, "SMA", "slim_RestoreDay():Day-file %s OKee",  pcFile);
            fCc = TRUE;
         }
         else
         {
            PRINTF1("slim_RestoreDay():Error opening day %s" CRLF, pcFile);
            GEN_MEMSET(pubData, 0, iSize);
         }
      }
      else PRINTF1("slim_RestoreDay():No such day %s" CRLF, pcFile);
      safefree(pcFile);
      safefree(pcTemp);
   }
   return(fCc);
}

//
// Function:   RPI_RestoreMonth
// Purpose:    Restore a month from file
//
// Parms:      Secs timestamp, buffer, size
// Returns:
// Note:
//
bool RPI_RestoreMonth(u_int32 ulSecs, u_int8 *pubData, int iSize)
{
   bool     fCc=FALSE;
   int      iLen, tFile;
   char    *pcFile, cSrc;
   char    *pcTemp;
   RPIMAP  *pstMap=GLOBAL_GetMapping();
   
   if(ulSecs)
   {
      //
      // Build bin file "/WwwDir/SmartE_yyymm.bin"
      //
      iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM);
      iLen  += 32;
      iLen  += GEN_STRLEN(pstMap->G_pcWwwDir);
      iLen  += GEN_STRLEN(pcFltFile);
      pcFile = safemalloc(iLen);
      pcTemp = safemalloc(iLen);
      //
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM, pcTemp, ulSecs);
      //
      GEN_SNPRINTF(pcFile, iLen, pcFltFile, pstMap->G_pcWwwDir, pcTemp);
      //
      FINFO_GetFileInfo((char *)pcFile, FI_FILEDIR, &cSrc, 1);
      if(cSrc == 'F')
      {
         PRINTF1("slim_RestoreMonth():Open %s" CRLF, pcFile);
         tFile = safeopen(pcFile, O_RDONLY);
         if(tFile > 0)
         {
            saferead(tFile, pubData, iSize);
            safeclose(tFile);
            LOG_Report(0, "SMA", "slim_RestoreMonth():Month-file %s OKee",  pcFile);
            fCc = TRUE;
         }
         else
         {
            PRINTF1("slim_RestoreMonth():ERROR opening month %s" CRLF, pcFile);
            GEN_MEMSET(pubData, 0, iSize);
         }
      }
      else PRINTF1("slim_RestoreMonth():No such month %s" CRLF, pcFile);
      safefree(pcFile);
      safefree(pcTemp);
   }
   return(fCc);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   rpi_HandleParm
// Purpose:    Lookup an argement in the main Args list and copy its value (if any) into the Global mmap area
//
// Parms:      Ptr tp parm "XXX=500", HTTP_HTML|HTTP_JSON|..., VAR_xxx
// Returns:    TRUE if parm OKee
// Note:       
//             
static bool rpi_HandleParm(char *pcParm, FTYPE tType, int iFunction)
{
   bool            fCc=FALSE;
   int             x;
   const char     *pcArg;
   const HTTPARGS *pstArgs=stHttpArguments;
   char           *pcValue;
   HTTPFUN         pfCallback;
   RPIMAP         *pstMap=GLOBAL_GetMapping();

   //PRINTF1("rpi_HandleParm(): Parm=<%s>" CRLF, pcParm);
   //
   while(pstArgs->iFunction)
   {
      //
      // iFunction is the bitwise function-filter (VAR_xxx)
      //
      if(pstArgs->iFunction & iFunction)
      {
         switch(tType)
         {
            case HTTP_JSON:   
               pcArg = pstArgs->pcJson; 
               break;
            
            case HTTP_HTML:
            default:          
               pcArg = pstArgs->pcHtml; 
               break;
         }
         //
         // Check parameter_to_variable delimiter "=" or ":"
         //
         x = GEN_SizeToDelimiters(pcParm, (char *)pcParmDelims);
         if(x == 0)
         {
            //
            // No delimiter: check variable without value (flag, ...)
            //
            x = GEN_SizeToDelimiters(pcParm, (char *)pcAmpAnd);
         }
         if( (x > 0) && (GEN_STRNCMPI(pcParm, pcArg, x) == 0) )
         {
            //PRINTF3("rpi_HandleParm(): cmp(%s,%s,%d) match" CRLF, pcArg, pcParm, x);
            //
            // Option found: copy value
            //
            pcArg = GEN_FindDelimiters(pcParm, (char *)pcParmDelims);
            if(pcArg)
            {
               pcValue = (char *)pstMap + pstArgs->iValueOffset;
               //
               // Copy parameter value from HTTP buffer to Global mmap
               //  "Command=Blablablabla&Parm1=Blah&Parm2=Bloh"
               //           ^----------^
               //             copy
               //
               GEN_CopyToDelimiter((char)*pcAmpAnd, pcValue, (char *)pcArg, pstArgs->iValueSize);
               rpi_ParmSetChanged(pstArgs, 1);
               PRINTF2("rpi_HandleParm(): Opt=<%s>:Value=<%s>" CRLF, pcParm, pcValue);
               fCc = TRUE;
            }
            else
            {
               //
               // ParameterX without value:
               //    "ParameterX&ParameterY=12033"
               //    "ParameterX"
               //
               pcValue = (char *)pstMap + pstArgs->iValueOffset;
               GEN_STRCPY(pcValue, "1");
               rpi_ParmSetChanged(pstArgs, 1);
               PRINTF1("rpi_HandleParm(): Opt:<%s>:No Value:force <1> or TRUE" CRLF, pcParm);
               fCc = TRUE;
            }
            //
            // Call the parameter callback function, if any
            //
            pfCallback = pstArgs->pfHttpFun;
            if(pfCallback) 
            {
               PRINTF1("rpi_HandleParm(): Id=%d" CRLF, pstArgs->iId);
               fCc = pfCallback(pstArgs->iId);
            }
            //
            // Arg found: no need to search for more !
            //
            break;
         }
         else
         {
            //PRINTF3("rpi_HandleParm(): cmp(%s,%s,%d) NO match" CRLF, pcArg, pcParm, x);
         }
      }
      pstArgs++;
   }
   return(fCc);
}

//
// Function:   rpi_ParmSetChanged
// Purpose:    Set the parm changed marker
//
// Parms:      Parm ^, set/unset
// Returns:    TRUE if parm has changed successfully
// Note:       Some parms cannot be changed (NEVER_CHANGED)
//             Some are always volatile (ALWAYS_CHANGED)
//
static bool rpi_ParmSetChanged(const HTTPARGS *pstArgs, bool fChanged)
{
   u_int8     *pubChanged;
   int         iChangedOffset=pstArgs->iChangedOffset;
   RPIMAP     *pstMap=GLOBAL_GetMapping();

   switch(iChangedOffset)
   {
      case ALWAYS_CHANGED:
         // Do not alter the change marker
         break;

      case NEVER_CHANGED:
         // Cannot alter the change marker
         if(fChanged)  fChanged = FALSE;
         break;

      default:
         pubChanged = (u_int8 *)pstMap + iChangedOffset;
        *pubChanged = (u_int8)fChanged;
         fChanged   = TRUE;
         break;
   }
   return(fChanged);
}

#ifdef  FEATURE_JSON_SHOW_REPLY
/*
 * Function    : rpi_ShowNetReply
 * Description : Print out the network json payload reply
 *
 * Parameters  : Title, port, JSON Object
 * Returns     : 
 *
 */
static void rpi_ShowNetReply(const char *pcTitle, int iPort, char *pcData)
{
   LOG_printf("%s(p=%d):Data=%s" CRLF, pcTitle, iPort, pcData);
}
#endif   //FEATURE_JSON_SHOW_REPLY


/*------  Local functions separator ------------------------------------
__CALLBACK_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   http_CollectDebug
// Purpose:    Callback function G_pcDebugMask
//
// Parms:      Parameter Enum
// Returns:    TRUE if OKee
// Note:       
//             
static bool http_CollectDebug(int iId)
{
   GLOBAL_ConvertDebugMask(TRUE);
   //
   PRINTF1("http_CollectDebug(): ID=%ld" CRLF, iId);
   return(TRUE);
}

//
// Function:   http_CollectRcuKey
// Purpose:    Callback function or remote RCU key
//
// Parms:      Parameter Enum
// Returns:    TRUE if found
// Note:       
//             
static bool http_CollectRcuKey(int iId)
{
   bool     fCc=FALSE;
   int      iIdx, iLen;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   for(iIdx=0; iIdx<NUM_RCU_KEYS; iIdx++)
   {
      iLen = GEN_STRLEN(pcRcuKeyLut[iIdx]);
      if(GEN_STRNCMPI(pcRcuKeyLut[iIdx], pstMap->G_pcRcuKey, iLen) == 0)
      {
         LOG_Report(0, "FPK", "http_CollectRcuKey(): ID=%ld, Key=%d (%s)", iId, iIdx, pcRcuKeyLut[iIdx]);
         pstMap->G_iRcuKey = iIdx;
         kill(GLOBAL_PidGet(PID_SRVR), SIGUSR1);
         fCc = TRUE;
         break;
      }
   }
   return(fCc);
}

//
// Function:   http_CollectGasPeriod
// Purpose:    Callback function G_pcGasPeriod
//
// Parms:      Parameter Enum
// Returns:    TRUE if OKee
// Note:       
//             
static bool http_CollectGasPeriod(int iId)
{
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   pstMap->G_iGasPeriod = (int)strtoul(pstMap->G_pcGasPeriod, NULL, 10);
   //
   PRINTF1("http_CollectGasPeriod(): ID=%ld" CRLF, iId);
   return(TRUE);
}

