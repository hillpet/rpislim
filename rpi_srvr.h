/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           rpi_srvr.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for HTTP-server thread rpi_srvr.c
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       20 Oct 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_SRVR_H_
#define _RPI_SRVR_H_

int      SRVR_Init         (void);


#endif /* _RPI_SRVR_H_ */
