#################################################################################
# 
# makefile:
#	$(TARGET) - Raspberry Pi RpiSlim Smart E-meter
#
#	Copyright (c) 2012-2018 Peter Hillen
#################################################################################
BOARD=$(shell cat ../../RPIBOARD)

ifeq ($(BOARD),1)
#
# RPi#1 (Rev-1): Metal case with CAM
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE
endif

ifeq ($(BOARD),2)
#
# RPi#2 (Rev-1): Black box with PiFace CAD LCD
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE
endif

ifeq ($(BOARD),3)
#
# RPi#3 (Rev-1): PiFace Digital
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE
endif

ifeq ($(BOARD),4)
#
# RPi#4 (Rev-1): 
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE
endif

ifeq ($(BOARD),6)
#
# RPi#6 (Rev-1): Rpi TFT Camera
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE
endif

ifeq ($(BOARD),7)
#
# RPi#7 (Rev-2): PopKodi and Apache web server
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE
endif

ifeq ($(BOARD),8)
#
# RPi#8 (Rev-2): CNC Controller
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE
endif

ifeq ($(BOARD),9)
#
# RPi#9 (Rev-3): VPN Server (with smart-E-meter)
#
	BOARD_OK=yes
	TARGET=rpislim
	COMMON=../common
	OBJX= rpi_slim.o rpi_obis.o rpi_comm.o gen_button.o
	DEFS=-D FEATURE_IO_PATRN
endif

ifeq ($(BOARD),10)
#
# RPi#10 (Rev-3): SpiCam HTTP Server
#
	BOARD_OK=yes
	TARGET=rpidog
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE
endif

ifndef BOARD_OK
#
# All other RPi's: 
#
	BOARD_OK=yes
	TARGET=spicam
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE
endif

CC		= gcc
CFLAGS	= -O2 -Wall -g
DEFS	+= -D BOARD_RPI$(BOARD)
INCDIR	+= -I/opt/vc/include
INCDIR	+= -I/opt/vc/include/interface/vmcs_host/linux
INCDIR	+= -I/opt/vc/include/interface/vcos/pthreads -DRPI=1
INCDIR	+= -I$(COMMON)
LDFLGS	= -pthread
DESTDIR	= ./bin
LIBFLGS	= -L/usr/local/lib -L/usr/lib -L/opt/vc/lib -L$(COMMON)
LIBS	= -lrt -lwiringPi -lwiringPiDev
LIBCOM	= -lcommon
DEPS	= config.h
OBJ		= rpi_main.o \
			rpi_srvr.o rpi_page.o rpi_json.o rpi_func.o rpi_html.o \
			gen_http.o \
			globals.o
OBJ		+= $(OBJX)

debug: LIBCOM = -lcommondebug
debug: DEFS += -D FEATURE_USE_PRINTF -D DEBUG_ASSERT
debug: all

all: modules $(DESTDIR)/$(TARGET)

modules:
$(DESTDIR)/$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(DEFS) -o $@ $(OBJ) $(LIBFLGS) $(LIBS) $(LIBCOM) $(LDFLGS)
	strip $(DESTDIR)/$(TARGET)

sync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp -u /mnt/rpi/softw/$(TARGET)/*.c ./
	cp -u /mnt/rpi/softw/$(TARGET)/*.h ./
	cp -u /mnt/rpi/softw/$(TARGET)/makefile ./
	cp -u /mnt/rpi/softw/$(TARGET)/$(TARGET).service ./

forcesync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp /mnt/rpi/softw/$(TARGET)/*.c ./
	cp /mnt/rpi/softw/$(TARGET)/*.h ./
	cp /mnt/rpi/softw/$(TARGET)/makefile ./
	cp /mnt/rpi/softw/$(TARGET)/$(TARGET).service ./

ziplog:
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo gzip -c /usr/local/share/rpi/$(TARGET).log >>/usr/local/share/rpi/$(TARGET)logs.gz
	sudo rm /usr/local/share/rpi/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET).log

copylog:
	@echo "Copy logfile to PC: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo cp /mnt/rpicache/$(TARGET).log /mnt/rpi/softw/$(TARGET)/$(TARGET).log

sortlog:
	@echo "Sort and copy logfile to PC: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo cat /mnt/rpicache/$(TARGET).log | sort -n >/mnt/rpicache/$(TARGET)sort.log
	sudo cp /mnt/rpicache/$(TARGET)sort.log /mnt/rpi/softw/$(TARGET)/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET)sort.log

dellog:
	@echo "Delete logfiles: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm /usr/local/share/rpi/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET).log

delmap:
	@echo "Delete mapfiles: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm /usr/local/share/rpi/$(TARGET).map
	sudo rm /mnt/rpicache/$(TARGET).map

clean:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	rm -rf *.o
	rm -rf $(DESTDIR)/$(TARGET)

install:
	sudo systemctl stop $(TARGET).service
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET)
	sudo systemctl restart $(TARGET).service

restart:
	sudo systemctl stop $(TARGET).service
	sudo systemctl restart $(TARGET).service

reload:
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET)
	sudo cp $(TARGET).service /lib/systemd/system/$(TARGET).service
	sudo systemctl enable $(TARGET).service
	sudo systemctl start $(TARGET).service

setup:
	sudo cp /usr/local/share/rpi/$(TARGET).map /mnt/rpicache/$(TARGET).map
	sudo cp /usr/local/share/rpi/$(TARGET).log /mnt/rpicache/$(TARGET).log
	sudo cp $(DESTDIR)/$(TARGET).service /lib/systemd/system/$(TARGET).service

.c.o: $(DEPS)
	$(CC) -c $(CFLAGS) $(DEFS) $(INCDIR) $< 
