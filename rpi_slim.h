/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           rpi_slim.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for rpi_slim.c
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       13 Okt 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_SLIM_H_
#define _RPI_SLIM_H_

void SLIM_InitDynamicPages    (int);

#endif /* _RPI_SLIM_H_ */
