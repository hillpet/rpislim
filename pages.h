/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           pages.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Extract headerfile for RPI dynamic http pages
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// The default (empty) URL page MUST be the first in this list
//
EXTRACT_DYN(DYN_HTML_EMPTY,    HTTP_HTML,  0, DYN_FLAG_PORT, "",                  http_DynPageEmpty          )
EXTRACT_DYN(DYN_HTML_INFO,     HTTP_HTML,  0, DYN_FLAG_PORT, "info.html",         http_DynPageDefault        )
EXTRACT_DYN(DYN_JSON_INFO,     HTTP_JSON,  0, DYN_FLAG_PORT, "info.json",         http_DynPageDefault        )
EXTRACT_DYN(DYN_JSON_PARMS,    HTTP_JSON,  0, DYN_FLAG_PORT, "parms.json",        http_DynPageParms          )
EXTRACT_DYN(DYN_HTML_PARMS,    HTTP_HTML,  0, DYN_FLAG_PORT, "parms.html",        http_DynPageParms          )
EXTRACT_DYN(DYN_HTML_LOG,      HTTP_HTML,  0, DYN_FLAG_PORT, "log.html",          http_DynPageLog            )
EXTRACT_DYN(DYN_JSON_LOG,      HTTP_JSON,  0, DYN_FLAG_PORT, "log.json",          http_DynPageLog            )
EXTRACT_DYN(DYN_HTML_EXIT,     HTTP_HTML,  0, DYN_FLAG_NONE, "exitslim.html",     http_DynPageExitSlim       )
EXTRACT_DYN(DYN_HTML_NEXIT,    HTTP_HTML,  0, DYN_FLAG_NONE, "exit.html",         http_DynPageExit           )
EXTRACT_DYN(DYN_JSON_NEXIT,    HTTP_JSON,  0, DYN_FLAG_PORT, "exit.json",         http_DynPageExit           )
