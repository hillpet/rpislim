/*  (c) Copyright:  2017..2018 Patrn, Confidential Data 
 *
 *  Workfile:           rpi_srvr.c
 *  Revision:   
 *  Modtime:    
 *
 *  Purpose:            Dynamic HTTP server
 *
 *
 *
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       19 Oct 2017
 *                                   
 *  Revisions:
 *  Entry Points:       SRVR_Init()
 *
 *                      Threads: (assume option -D for background execution):
 *
 *                       HOST : Thread Parent
 *                               SIGINT
 *                               SIGUSR1
 *                               SIGUSR2
 *
 *                       SRVR : Thread RPI Server
 *                               SIGINT
 *                               SIGUSR1
 *                               SIGUSR2
 *
 *                       SLIM : Thread Smart-E-Meter comms  
 *                               SIGINT
 *                               SIGUSR1
 *                               SIGUSR2
 *
 *
 *
 *
**/


#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_http.h"
#include "rpi_page.h"
#include "rpi_func.h"
#include "rpi_slim.h"
//
#include "rpi_srvr.h"

#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
static void    srvr_Daemon                (void);
static bool    srvr_SignalRegister        (sigset_t *);
static void    srvr_ReceiveSignalInt      (int);
static void    srvr_ReceiveSignalTerm     (int);
static void    srvr_ReceiveSignalUser1    (int);
static void    srvr_ReceiveSignalUser2    (int);
static void    srvr_ReceiveSignalSegmnt   (int);
//
static void    srvr_HandleCompletion      (NETCL *);
static void    srvr_HandleGuard           (void);
static bool    srvr_CallbackConnection    (NETCL *);
static int     srvr_HandleHttpReply       (NETCL *);
static int     srvr_HandleHttpReplyHtml   (NETCL *);
static int     srvr_HandleHttpReplyJson   (NETCL *);
static int     srvr_HandleHttpRequest     (NETCL *);
static void    srvr_PostSemaphore         (void);
//
static bool    fSrvrRunning    = TRUE;

/* ======   Local Functions separator ===========================================
___MAIN_FUNCTIONS(){}
==============================================================================*/

//
//  Function:   SRVR_Init
//  Purpose:    HTTP server init
//
//  Parms:      
//  Returns:    Startup code GLOBAL_XXX_INI
//
int SRVR_Init(void)
{
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_SRVR, tPid);
         break;

      case 0:
         // Child: RPI HTTP-server
         GEN_Sleep(2000);
         srvr_Daemon();
         LOG_Report(0, "SVR", "SRVR_Init():Exit normally");
         _exit(0);
         break;

      case -1:
         // Error
         GEN_Printf("SRVR_Init(): Error!"CRLF);
         LOG_Report(errno, "SVR", "SRVR_Init():Exit fork ERROR:");
         _exit(1);
         break;
   }
   return(GLOBAL_SVR_INI);
}

/* ======   Local Functions separator ===========================================
___LOCAL_FUNCTIONS(){}
==============================================================================*/

//
//  Function:   srvr_Daemon
//  Purpose:    Run the Raspberry Pi HTTP mini-server as a deamon
//
//  Parms:
//  Returns:
//
static void srvr_Daemon(void)
{
   int         iPort, iButtonOff, iCnxs;
   sigset_t    tBlockset;
   NETCON     *pstNet=NULL;
   RPIMAP     *pstMap=GLOBAL_GetMapping();

   if(sem_init(&pstMap->G_tSemSrvr, 1, 0) == -1) 
   {
      LOG_Report(errno, "SVR", "srvr_Daemon():Exit semaphore ERROR:");
      _exit(1);
   }
   if(srvr_SignalRegister(&tBlockset) == FALSE) 
   {
      LOG_Report(errno, "SVR", "srvr_Daemon():Exit fork ERROR:");
      _exit(1);
   }
   //
   // Set up server socket 
   //
   pstNet = NET_Init(pstMap->G_pcWwwDir, pstMap->G_pcRamDir);
   if( NET_ServerConnect(pstNet, pstMap->G_pcSrvrPort, HTTP_PROTOCOL) == -1)
   {
      LOG_printf("srvr_Daemon():Server connect failed: exiting" CRLF);
      LOG_Report(errno, "SVR", "srvr_Daemon():Exit NET connect ERROR:");
      _exit(1);
   }
   iPort = atoi(pstMap->G_pcSrvrPort);
   //
   HTTP_InitDynamicPages(iPort);
   SLIM_InitDynamicPages(iPort);
   GLOBAL_GetHostName();
   GLOBAL_PidSaveGuard(PID_SRVR, GLOBAL_SVR_INI);
   GLOBAL_HostNotification(GLOBAL_SVR_INI);
   //
   // Wait for data
   //
   while(fSrvrRunning)
   {
      NET_BuildConnectionList(pstNet);
      //
      // Wait for http data
      //
      if(NET_WaitForConnection(pstNet, HTTP_CONNECTION_TIMEOUT_MSECS) > 0)
      {
         PRINTF("srvr_Daemon(): Socket has data" CRLF); 
         //
         // There is data on some of the sockets
         //
         NET_HandleSessions(pstNet, &srvr_CallbackConnection, pstMap->G_pcIpAddr, MAX_ADDR_LEN);
      }
      else
      {
         //
         // Timeout: check 24 hour activity log
         //
         iButtonOff = BUTTON(BUTTON_OFF);
         PRINTF1("srvr_Daemon():BUTTON=%d" CRLF, iButtonOff); 
         if(iButtonOff)
         {
            //
            // Button pressed
            //
         #ifdef DEBUG_ASSERT
            //
            // Test/Debug mode: No IO buttons
            //
            PRINTF("srvr_Daemon():Power OFF button: IGNORE HERE" CRLF); 
         #else
            kill(GLOBAL_PidGet(PID_HOST), SIGUSR2);
            PRINTF("srvr_Daemon():Power OFF button: signal host" CRLF); 
         #endif
         }
         PRINTF("srvr_Daemon():no data" CRLF); 
         //
         // Check network stack
         //
         if((iCnxs = NET_ReportServerStatus(pstNet, NET_CHECK_COUNT)) == 0)
         {
            //
            // No connections available: reset all expired ones !
            //
            NET_ReportServerStatus(pstNet, NET_CHECK_LOG|NET_CHECK_FREE);
         }
         //
         // Handle the Guard duty here
         //
         GLOBAL_PidSetGuard(PID_COMM);
         if(!GLOBAL_PidGetGuard(PID_COMM)) LOG_Report(0, "COM", "slim_HandleMeter(): ERROR Guard set !!!!!!!!");
      }
   }
   //
   // Close all socket connections
   //
   NET_ServerTerminate(pstNet);
   if( sem_destroy(&pstMap->G_tSemSrvr) == -1) LOG_Report(errno, "SVR", "srvr_Daemon():sem_destroy");
}

//
//  Function:   srvr_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool srvr_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &srvr_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr_SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &srvr_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr_SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &srvr_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr_SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &srvr_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr_SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &srvr_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr_ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
  }
  return(fCC);
}

//
//  Function:   srvr_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "SVR", "srvr_ReceiveSignalTerm()");
   fSrvrRunning = FALSE;
   srvr_PostSemaphore();
}

//
//  Function:   srvr_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "SVR", "srvr_ReceiveSignalInt()");
   fSrvrRunning = FALSE;
   srvr_PostSemaphore();
}

//
//  Function:   srvr_ReceiveSignalSegmnt
//  Purpose:    Add the SIGSEGV command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalSegmnt(int iSignal)
{
   if(fSrvrRunning)
   {
      LOG_Report(errno, "SVR", "srvr_ReceiveSignalSegmnt()");
      GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_FLT_NFY);
      kill(GLOBAL_PidGet(PID_HOST), SIGTERM);
      fSrvrRunning = FALSE;
      srvr_PostSemaphore();
   }
}

//
// Function:   srvr_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR1 is used to 
//
static void srvr_ReceiveSignalUser1(int iSignal)
{
   srvr_PostSemaphore();
}

//
// Function:   srvr_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR2 is not used
//
static void srvr_ReceiveSignalUser2(int iSignal)
{
}

/* ======   Local Functions separator ===========================================
___MISC_FUNCTIONS(){}
==============================================================================*/

//  
// Function    :  srvr_CallbackConnection
// Description :  Handle the connection list
// 
// Parameters  :  Socket descriptor
// Returns     :  TRUE if OKee
// Note:          NET_HandleSessions(..) will callback here as soon as a complete
//                HTTP Get/POST request has been acquired through the socket.
//                This Callback will verify if the HTTP request concerns:
//                o a STATIC  page: Try to read it from file and return to the socket
//                o a DYNAMIC page: Lookup the page and forward to the registered handler
//
//                The handler will respond back with a SIGUSR2 signal with the appropriate
//                notification set.
//  
//  
static bool srvr_CallbackConnection(NETCL *pstCl)
{
   bool     fCc=TRUE;
   int      iTimeout;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   PRINTF("srvr_CallbackConnection()" CRLF);
   //
   // Data came in from the socket: check if we have something to do
   //
   if( (iTimeout = srvr_HandleHttpRequest(pstCl)) > 0) 
   {
      //
      // Command has been forwarded to a handler. It will report back through the semaphore (via SIGUSR2) 
      // Wait the timeout and act accordingly
      //
      switch( GEN_WaitSemaphore(&pstMap->G_tSemSrvr, iTimeout) )
      {
         case 0:
            //
            // Server has completed
            //
            if( GLOBAL_GetSignalNotification(PID_SRVR, GLOBAL_VAR_NFY) ) srvr_HandleCompletion(pstCl);
            if( GLOBAL_GetSignalNotification(PID_SRVR, GLOBAL_GRD_NFY) ) srvr_HandleGuard();
            break;

         default:
         case -1:
            LOG_Report(errno, "SVR", "srvr_CallbackConnection():ERROR sem_timedwait");
            PRINTF1("srvr_CallbackConnection():ERROR Sem_timedwait(errno=%d)" CRLF, errno);
            fCc = FALSE;
            break;

         case 1:
            //
            // Timeout:
            //
            LOG_Report(errno, "SVR", "srvr_CallbackConnection():sem_timedwait");
            PRINTF1("srvr_CallbackConnection():Timeout(errno=%d)" CRLF, errno);
            //
            if( srvr_HandleHttpReply(pstCl) )
            {
               //
               // The command timed out: specific reply has already been send back to the app
               //
            }
            else
            {
               //
               // Nothing specific can be done: generate a generic response
               //
               srvr_HandleCompletion(pstCl);
            }
            break;
      }
   }
   NET_FlushCache(pstCl);
   //
   // Session is finished: close connection
   //
   PRINTF("srvr_CallbackConnection():Ready, socket disconnected." CRLF);
   return(fCc);
}

// 
// Function:   srvr_HandleGuard
// Purpose:    Respond to the guard query
// 
// Parameters: 
// Returns:    
// Note:       
//
static void srvr_HandleGuard(void)
{
   LOG_Report(0, "SVR", "srvr_HandleGuard():Host-notification send");
   GLOBAL_HostNotification(GLOBAL_GRD_NFY);
}

//  
//  Function    : srvr_HandleCompletion
//  Description : Handle the HTTP server completion code
//  
//  Parameters  : Socket descriptor
//  Returns     : Timeout in msecs
//  
static void srvr_HandleCompletion(NETCL *pstCl)
{
   RPIMAP     *pstMap=GLOBAL_GetMapping();

   switch(GLOBAL_Status(GEN_STATUS_ASK))
   {
      case GEN_STATUS_TIMEOUT:
         PRINTF("srvr_HandleCompletion():TIMEOUT" CRLF);
         RPI_ReportError(pstCl);
         break;

      case GEN_STATUS_ERROR:
         PRINTF("srvr_HandleCompletion():ERROR" CRLF);
         RPI_ReportError(pstCl);
         break;

      case GEN_STATUS_REJECTED:
         PRINTF("srvr_HandleCompletion():REJECTED" CRLF);
         RPI_ReportBusy(pstCl);
         break;

      case GEN_STATUS_REDIRECT:
         PRINTF("srvr_HandleCompletion():REDIRECTED" CRLF);
         GLOBAL_Status(GEN_STATUS_IDLE);
         HTTP_DynamicPageHandler(pstCl, pstMap->G_iCurDynPage);
         break;

      default:
         //PRINTF("srvr_HandleCompletion():OKee" CRLF);
         srvr_HandleHttpReply(pstCl);
         break;
   }
}

//  
//  Function    : srvr_HandleHttpReply
//  Description : Request has been completed: handle the reply back to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : 
//  
static int srvr_HandleHttpReply(NETCL *pstCl)
{
   bool     fCc=FALSE;
   FTYPE    tType;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   tType = GEN_GetDynamicPageType(pstMap->G_iCurDynPage);
   switch(tType)
   {
      default:
      case HTTP_HTML:
         fCc = srvr_HandleHttpReplyHtml(pstCl);
         break;

      case HTTP_JSON:
         fCc = srvr_HandleHttpReplyJson(pstCl);
         break;
   }
   return(fCc);
}

//  
//  Function    : srvr_HandleHttpReplyHtml
//  Description : Request has been completed: handle HTML reply back to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : True if handled
//  
static int srvr_HandleHttpReplyHtml(NETCL *pstCl)
{
   bool     fCc=FALSE;

   return(fCc);
}

//  
//  Function    : srvr_HandleHttpReplyJson
//  Description : Request has been completed: handle JSON reply back to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : True if handled
//  
static int srvr_HandleHttpReplyJson(NETCL *pstCl)
{
   bool     fCc=FALSE;
   
   return(fCc);
}

//  
//  Function    : srvr_HandleHttpRequest
//  Description : Data came in through the socket: handle it
//  
//  Parameters  : Socket descriptor
//  Returns     : Timeout in msecs
//  
static int srvr_HandleHttpRequest(NETCL *pstCl)
{
   int      iWait=0, iIdx;
   FTYPE    tType;
   pid_t    tPid;
   int      ePid;
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   //
   // Handle incoming HTTP GET request:
   //
   //   "GET /index.html HTTP/1.1" CR,LF
   //   "Host: 10.0.0.231" CR,LF
   //   "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)" CR,LF
   //   "Accept: image/png,image/*;q=0.8,*/*;q=0.5" CR,LF
   //   "Accept-Encoding: gzip,deflate" CR,LF
   //   "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7" CR,LF
   //   "Keep-Alive: 115" CR,LF
   //   "Connection: keep-alive" CR,LF
   //   CR,LF
   //
   // Check if the requested URL is a dynamically generated page or if it should
   // be on the filesystem.
   //
   if( (iIdx = HTTP_PageIsDynamic(pstCl)) < 0 )
   {
      // DFD-0-3.3 (RPI Xmt STATIC page)
      // This page is a STATIC page (needs to come from the file system)
      //
      pstMap->G_iCurDynPage = -1;
      PRINTF1("srvr_HandleHttpRequest():STATIC page (%s)" CRLF, pstCl->pcUrl);
      HTTP_RequestStaticPage(pstCl);
   }
   else
   {
      //
      // This page is an internal (DYNAMICALLY build) page
      // Check if we can do this ourselves here, or if it need additional
      // handling.
      //
      PRINTF1("srvr_HandleHttpRequest():DYNAMIC page (%s)" CRLF, pstCl->pcUrl);
      pstMap->G_iCurDynPage = iIdx;
      //
      tType        = GEN_GetDynamicPageType(iIdx);
      ePid         = GEN_GetDynamicPagePid(iIdx);
      iWait        = GEN_GetDynamicPageTimeout(iIdx);
      //
      // Type is HTTP_HTML or HTTP_JSON
      // HTTP_HTML: ...cam.html?xxxx&-p17-p2....
      // HTTP_JSON: ...cam.jsom?command=xxxx&para1=yyyy&....
      //
      tPid = GLOBAL_PidGet(ePid);
      RPI_CollectParms(pstCl, tType);
      //
      PRINTF4("srvr_HandleHttpRequest():Dynamic URL:Idx=%d, Pid=%d, Wait=%d, URL=%s" CRLF, iIdx, tPid, iWait, GEN_GetDynamicPageName(iIdx) );
      //
      // Signal the thread to handle it (if it is not us)
      //
      if(tPid > 0)
      {
         PRINTF3("srvr_HandleHttpRequest():Forward request to Pid=%d, [%s]-[%s]" CRLF, tPid, pstCl->pcUrl, pstMap->G_pcCommand);
         GLOBAL_SetSignalNotification(ePid, GLOBAL_SVR_NFY);
         kill(tPid, SIGUSR1);
      }
      else
      {
         if(HTTP_DynamicPageHandler(pstCl, iIdx))
         {
            //
            // Dynamic page returns TRUE if the current page request should complete the HTTP request
            //
            PRINTF2("srvr_HandleHttpRequest():OKee :[%s]-[%s]" CRLF, pstCl->pcUrl, pstMap->G_pcCommand);
         }
         else
         {
            //
            // No reply was set up: send back error
            //
            PRINTF2("srvr_HandleHttpRequest():ERROR:[%s]-[%s]" CRLF, pstCl->pcUrl, pstMap->G_pcCommand);
         }
      }
   }
   return(iWait);
}

//
// Function:   srvr_PostSemaphore
// Purpose:    Unlock the Server Semaphore
//
// Parms:      
// Returns:    
// Note:       sem_post() increments (unlocks) the semaphore pointed to by sem. If the semaphore's 
//             value consequently becomes greater than zero, then another process or thread blocked
//             in a sem_wait(3) call will be woken up and proceed to lock the semaphore. 
//
static void srvr_PostSemaphore(void)
{
   RPIMAP     *pstMap=GLOBAL_GetMapping();

   //
   // sem++ (UNLOCK)   
   //
   if(sem_post(&pstMap->G_tSemSrvr) == -1) LOG_Report(errno, "SVR", "srvr_PostSemaphore(): sem_post error");
}


