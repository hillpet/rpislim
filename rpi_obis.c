/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           template.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_obis.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local generic prototypes
//
static bool    obis_ConvertToFloat           (char *, double *);
static bool    obis_ConvertToInt             (char *, int *);
static char   *obis_FindValueStart           (char *, char);
//
// Local OBIS prototypes
//
static bool    obis_AdmEquipment             (char *, EMTR *);
static bool    obis_AdmPowerFailLong         (char *, EMTR *);
static bool    obis_AdmPowerFailShort        (char *, EMTR *);
static bool    obis_CurrentTar               (char *, EMTR *);
static bool    obis_PowerActualDelivered     (char *, EMTR *);
static bool    obis_PowerActualReceived      (char *, EMTR *);
static bool    obis_PowerTotalDeliveredTar1  (char *, EMTR *);
static bool    obis_PowerTotalDeliveredTar2  (char *, EMTR *);
static bool    obis_PowerTotalReceivedTar1   (char *, EMTR *);
static bool    obis_PowerTotalReceivedTar2   (char *, EMTR *);
static bool    obis_PowerL1InstantDelivered  (char *, EMTR *);
static bool    obis_PowerL1InstantReceived   (char *, EMTR *);
static bool    obis_CurrentL1Instant         (char *, EMTR *);
static bool    obis_VoltageL1Instant         (char *, EMTR *);
static bool    obis_VoltageL1Sags            (char *, EMTR *);
static bool    obis_VoltageL1Swells          (char *, EMTR *);
static bool    obis_PowerL2InstantDelivered  (char *, EMTR *);
static bool    obis_PowerL2InstantReceived   (char *, EMTR *);
static bool    obis_CurrentL2Instant         (char *, EMTR *);
static bool    obis_VoltageL2Instant         (char *, EMTR *);
static bool    obis_VoltageL2Sags            (char *, EMTR *);
static bool    obis_VoltageL2Swells          (char *, EMTR *);
static bool    obis_PowerL3InstantDelivered  (char *, EMTR *);
static bool    obis_PowerL3InstantReceived   (char *, EMTR *);
static bool    obis_CurrentL3Instant         (char *, EMTR *);
static bool    obis_VoltageL3Instant         (char *, EMTR *);
static bool    obis_VoltageL3Sags            (char *, EMTR *);
static bool    obis_VoltageL3Swells          (char *, EMTR *);
static bool    obis_PowerFailLog             (char *, EMTR *);
static bool    obis_GasDelivered             (char *, EMTR *);
//
//
//
static const OBIS stObisDefinition[] =
{
#define EXTRACT_OBIS(a,b,c,d) {b,c,d},
#include "obis.h"
#undef   EXTRACT_OBIS
//
   {  NULL,    NULL,    NULL }
};


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   OBIS_LookupCode
// Purpose:    Lookup the OBIS code from the list and run the handler function
// 
// Parameters: OBIS code buffer ptr, EMTR info struct ptr
// Returns:    Index to next record or -1 on EOF
// Note:       pcCode contains:
//
//             "/KFM5KAIFA-METER\r\n
//             \r\n
//             1-3:0.2.8(42)\r\n
//             0-0:1.0.0(171013150653S)\r\n
//             0-0:96.1.1(4530303235303030303639373538373136)\r\n
//             1-0:1.8.1(001852.792*kWh)\r\n
//             1-0:1.8.2(002550.969*kWh)\r\n
//             1-0:2.8.1(000000.000*kWh)\r\n
//             1-0:2.8.2(000000.000*kWh)\r\n
//             0-0:96.14.0(0002)\r\n
//             1-0:1.7.0(02.236*kW)\r\n
//             1-0:2.7.0(00.000*kW)\r\n
//             0-0:96.7.21(00002)\r\n
//             0-0:96.7.9(00002)\r\n
//             1-0:99.97.0(2)(0-0:96.7.19)(170116085159W)(0000000933*s)(000101000001W)(2147483647*s)\r\n
//             1-0:32.32.0(00000)\r\n
//             1-0:32.36.0(00000)\r\n
//             0-0:96.13.1()\r\n
//             0-0:96.13.0()\r\n
//             1-0:31.7.0(009*A)\r\n
//             1-0:21.7.0(02.235*kW)\r\n
//             1-0:22.7.0(00.000*kW)\r\n
//             0-1:24.1.0(003)\r\n
//             0-1:96.1.0(4730303139333430333037303635303136)\r\n
//             0-1:24.2.1(171013150000S)(02348.695*m3)\r\n
//             !6546\r\n"
//
//  
int OBIS_LookupCode(char *pcCode, EMTR *pstObis)
{
   int         iIdx=0, iLen, tObis=0;
   const char *pcRes;
   PFFUN       pfFun;

   if(pcCode[iIdx] == '\0') return(-1);
   //
   do
   {
      pcRes = stObisDefinition[tObis].pcObis;
      if(pcRes)
      {
         PRINTF2("OBIS_LookupCode():Id=%2d:%s" CRLF, tObis, pcRes);
         iLen = GEN_STRLEN(pcRes);
         if(GEN_STRNCMP(pcCode, pcRes, iLen) == 0)
         {
            //
            // OBIS code found
            //
            PRINTF2("OBIS_LookupCode():Found: Id=%2d:%s" CRLF, tObis, pcRes);
            pfFun = stObisDefinition[tObis].pfHandler;
            if(pfFun)
            {
               pstObis->tObis = tObis;
               pfFun(pcCode, pstObis);
            }
            break;
         }
      }
      tObis++;
   }
   while(pcRes);
   //
   // Find EOR or EOF
   //
   while(1)
   {
      if( pcCode[iIdx] == '\n')
      {
         iIdx++;
         break;
      }
      else if( pcCode[iIdx] == '\0')
      {
         iIdx = -1;
         break;
      }
      else iIdx++;
   }
   return(iIdx);
}

//  
// Function:   OBIS_LookupMeterName
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "/KFM5KAIFA-METER", EMTR struct ptr
// Returns:    Length name
// Note:       
//  
int OBIS_LookupMeterName(char *pcCode, EMTR *pstEmtr)
{
   bool  fBusy=TRUE;
   int   iIdx=0;

   if(*pcCode++ == '/')
   {
      // correct start
      while(fBusy)
      {
         switch(*pcCode)
         {
            default:
               pstEmtr->cEquipment[iIdx++] = *pcCode++;
               if(iIdx >= MAX_EMTR-1)
               {
                  // truncate buffer
                  pstEmtr->cEquipment[iIdx] = 0;
                  fBusy = FALSE;
               }
               break;

            case '\r':
            case '\n':
               // EOR: ready
               pstEmtr->cEquipment[iIdx] = 0;
               fBusy = FALSE;
               break;
         }
      }
   }
   return(iIdx);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   obis_ConvertToFloat
// Purpose:    Convert a ASCII string to double
// 
// Parameters: OBIS code, Float ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_ConvertToFloat(char *pcCode, double *pflResult)
{
   bool        fCc=FALSE;
   const char *pcFloat;

   pcFloat = obis_FindValueStart(pcCode, '(');
   if(pcFloat)
   {
      *pflResult = atof(pcFloat);
      fCc = TRUE;
   }
   return(fCc);
}

//  
// Function:   obis_ConvertToInt
// Purpose:    Convert a ASCII string to integer
// 
// Parameters: OBIS code, integer ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_ConvertToInt(char *pcCode, int *piResult)
{
   bool        fCc=FALSE;
   const char *pcInt;

   pcInt = obis_FindValueStart(pcCode, '(');
   if(pcInt)
   {
      *piResult = atoi(pcInt);
      fCc = TRUE;
   }
   return(fCc);
}

//  
// Function:   obis_FindValueStart
// Purpose:    Find the starting point of a value in the OBIS record
// 
// Parameters: Record ptr, delimiter char
// Returns:    Value ptr
// Note:       
//  
static char *obis_FindValueStart(char *pcCode, char cDelim)
{
   char *pcValue=NULL;

   while(*pcCode)
   {
      if(*pcCode++ == cDelim)
      {
         pcValue = pcCode;
         break;
      }
   }
   return(pcValue);
}


/*------  Local functions separator ------------------------------------
__OBIS_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   obis_AdmEquipment
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "/KFM5KAIFA-METER", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_AdmEquipment(char *pcCode, EMTR *pstEmtr)
{
   return(FALSE);
}

//  
// Function:   obis_AdmPowerFailLong
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "0-0:96.7.9(00002)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_AdmPowerFailLong(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToInt(pcCode, &pstEmtr->iNumLongPowerFails);
   return(fCc);
}

//  
// Function:   obis_AdmPowerFailShort
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "0-0:96.7.21(00002)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_AdmPowerFailShort(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToInt(pcCode, &pstEmtr->iNumShortPowerFails);
   return(fCc);
}

//  
// Function:   obis_CurrentTar
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "0-0:96.14.0(0002)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_CurrentTar(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToInt(pcCode, &pstEmtr->iCurrTariff);
   return(fCc);
}

//  
// Function:   obis_PowerActualDelivered
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:1.7.0(02.231*kW)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_PowerActualDelivered(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flPowerActualDelivered);
   return(fCc);
}

//  
// Function:   obis_PowerActualReceived
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:2.7.0(00.000*kW)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_PowerActualReceived(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flPowerActualReceived);
   return(fCc);
}

//  
// Function:   obis_PowerTotalDeliveredTar1
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:1.8.1(001852.792*kWh)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_PowerTotalDeliveredTar1(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flPowerT1Delivered);
   return(fCc);
}

//  
// Function:   obis_PowerTotalDeliveredTar2
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:1.8.2(002550.994*kWh)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_PowerTotalDeliveredTar2(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flPowerT2Delivered);
   return(fCc);
}

//  
// Function:   obis_PowerTotalReceivedTar1
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:2.8.1(000000.000*kWh)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_PowerTotalReceivedTar1(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flPowerT1Received);
   return(fCc);
}

//  
// Function:   obis_PowerTotalReceivedTar2
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:2.8.2(000000.000*kWh)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_PowerTotalReceivedTar2(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flPowerT2Received);
   return(fCc);
}

//  
// Function:   obis_PowerL1InstantDelivered
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:21.7.0(02.234*kW)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_PowerL1InstantDelivered(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flPowerL1Delivered);
   return(fCc);
}

//  
// Function:   obis_PowerL1InstantReceived
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:22.7.0(00.000*kW)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_PowerL1InstantReceived(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flPowerL1Received);
   return(fCc);
}

//  
// Function:   obis_CurrentL1Instant
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:31.7.0(009*A)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_CurrentL1Instant(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flCurrentL1);
   return(fCc);
}

//  
// Function:   obis_VoltageL1Instant
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:32.7.0(0230*V)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_VoltageL1Instant(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flVoltageL1);
   return(fCc);
}

//  
// Function:   obis_VoltageL1Sags
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:32.32.0(00000)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_VoltageL1Sags(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToInt(pcCode, &pstEmtr->iVoltageL1Sags);
   return(fCc);
}

//  
// Function:   obis_VoltageL1Swells
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:32.36.0(00000)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_VoltageL1Swells(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToInt(pcCode, &pstEmtr->iVoltageL1Swells);
   return(fCc);
}

//  
// Function:   obis_PowerL2InstantDelivered
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:41.7.0", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_PowerL2InstantDelivered(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flPowerL2Delivered);
   return(fCc);
}

//  
// Function:   obis_PowerL2InstantReceived
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:42.7.0", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_PowerL2InstantReceived(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flPowerL2Received);
   return(fCc);
}

//  
// Function:   obis_CurrentL2Instant
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:51.7.0", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_CurrentL2Instant(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flCurrentL2);
   return(fCc);
}

//  
// Function:   obis_VoltageL2Instant
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:52.7.0", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_VoltageL2Instant(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flVoltageL2);
   return(fCc);
}

//  
// Function:   obis_VoltageL2Sags
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:52.32.0", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_VoltageL2Sags(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToInt(pcCode, &pstEmtr->iVoltageL2Sags);
   return(fCc);
}

//  
// Function:   obis_VoltageL2Swells
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:52.36.0", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_VoltageL2Swells(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToInt(pcCode, &pstEmtr->iVoltageL2Swells);
   return(fCc);
}

//  
// Function:   obis_PowerL3InstantDelivered
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:61.7.0", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_PowerL3InstantDelivered(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flPowerL3Delivered);
   return(fCc);
}

//  
// Function:   obis_PowerL3InstantReceived
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:62.7.0)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_PowerL3InstantReceived(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flPowerL3Received);
   return(fCc);
}

//  
// Function:   obis_CurrentL3Instant
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:71.7.0", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_CurrentL3Instant(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flCurrentL3);
   return(fCc);
}

//  
// Function:   obis_VoltageL3Instant
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:72.7.0", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_VoltageL3Instant(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToFloat(pcCode, &pstEmtr->flVoltageL3);
   return(fCc);
}

//  
// Function:   obis_VoltageL3Sags
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:72.32.0", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_VoltageL3Sags(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToInt(pcCode, &pstEmtr->iVoltageL3Sags);
   return(fCc);
}

//  
// Function:   obis_VoltageL3Swells
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:72.36.0", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Not present in KAIFA 130C
//  
static bool obis_VoltageL3Swells(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   fCc = obis_ConvertToInt(pcCode, &pstEmtr->iVoltageL3Swells);
   return(fCc);
}

//  
// Function:   obis_PowerFailLog
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "1-0:99.97.0", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       Ignore for now
// 
// 1-0:99.97.0(2)(0-0:96.7.19)(170116085159W)(0000000933*s)(000101000001W)(2147483647*s)
//             
static bool obis_PowerFailLog(char *pcCode, EMTR *pstEmtr)
{
   return(FALSE);
}

//  
// Function:   obis_GasDelivered
// Purpose:    Handle this OBIS code
// 
// Parameters: OBIS code "0-1:24.2.1(171013150000S)(02348.695*m3)", EMTR struct ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool obis_GasDelivered(char *pcCode, EMTR *pstEmtr)
{
   bool  fCc;

   pcCode = obis_FindValueStart(pcCode, '(');
   fCc    = obis_ConvertToFloat(pcCode, &pstEmtr->flGasDelivered);
   return(fCc);
}