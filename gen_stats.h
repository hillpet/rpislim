/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           gen_stats.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for GEN_STATUS_xxx
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       20 Oct 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
 
EXTRACT_ST( GEN_STATUS_IDLE,           "Rpi-Idle"         )
EXTRACT_ST( GEN_STATUS_PARMS,          "Rpi-Parms"        )
EXTRACT_ST( GEN_STATUS_ERROR,          "Rpi-Error"        )
EXTRACT_ST( GEN_STATUS_REDIRECT,       "Rpi-Redirect"     )
EXTRACT_ST( GEN_STATUS_SNAPSHOT,       "Rpi-Snapshot"     )
EXTRACT_ST( GEN_STATUS_MOTION,         "Rpi-Motion"       )      
EXTRACT_ST( GEN_STATUS_RECORDING,      "Rpi-Recording"    )      
EXTRACT_ST( GEN_STATUS_TIMELAPSE,      "Rpi-Timelapse"    )      
EXTRACT_ST( GEN_STATUS_STREAMING,      "Rpi-Streaming"    )      
EXTRACT_ST( GEN_STATUS_STOPPED,        "Rpi-Stopped"      )      
EXTRACT_ST( GEN_STATUS_REJECTED,       "Rpi-Rejected"     )      
EXTRACT_ST( GEN_STATUS_KEYPRESS,       "Rpi-Keypress"     )      
EXTRACT_ST( GEN_STATUS_KEYRELEASE,     "Rpi-Keyrelease"   )
EXTRACT_ST( GEN_STATUS_TIMEOUT,        "Rpi-Timeout"      )



