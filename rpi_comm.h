/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           rpi_comm.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for rpi_comm.c
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       22 Okt 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_COMM_H_
#define _RPI_COMM_H_

int      SLIM_Init            (void);
u_int16  SLIM_CalculateCrc16  (const u_int8 *, int);
//
#define  SMART_E_CYCLE_SECS   10
#define  SMART_E_TIMEOUT      ((SMART_E_CYCLE_SECS+2)*1000)
//
enum
{
   STATE_READ_DATA      = 0,
   STATE_READ_CRC,
   //
   NUM_STATES
};


#endif /* _RPI_COMM_H_ */
