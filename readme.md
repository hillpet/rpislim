=====================================================================
Readme.md
=====================================================================

rpislim:

This systemd service is used to communicate with a KAIFA Smart-E-meter 
over a serial connection (115.200, 8N1). The signal itself is conditioned 
and inverted through a simple n-Channel MOSFET interface with converts the
P1-port of the KAIFA to something the Raspberry Pi RxD input understands.

The KAIFA will send out one package of data to the serial port which will 
be parsed by the service and periodically (5 mins) appended to a monthly 
CSV-file.

Example package send every 10 secs :

/KFM5KAIFA-METER

1-3:0.2.8(42)
0-0:1.0.0(171013150653S)
0-0:96.1.1(4530303235303030303639373538373136)
1-0:1.8.1(001852.792*kWh)
1-0:1.8.2(002550.969*kWh)
1-0:2.8.1(000000.000*kWh)
1-0:2.8.2(000000.000*kWh)
0-0:96.14.0(0002)
1-0:1.7.0(02.236*kW)
1-0:2.7.0(00.000*kW)
0-0:96.7.21(00002)
0-0:96.7.9(00002)
1-0:99.97.0(2)(0-0:96.7.19)(170116085159W)(0000000933*s)(000101000001W)(2147483647*s)
1-0:32.32.0(00000)
1-0:32.36.0(00000)
0-0:96.13.1()
0-0:96.13.0()
1-0:31.7.0(009*A)
1-0:21.7.0(02.235*kW)
1-0:22.7.0(00.000*kW)
0-1:24.1.0(003)
0-1:96.1.0(4730303139333430333037303635303136)
0-1:24.2.1(171013150000S)(02348.695*m3)
!6546


17 Oct 2017
Hillpet
Patrn ESS
