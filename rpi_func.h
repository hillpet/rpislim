/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           rpi_func.h.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for rpi_func
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_FUNC_H_
#define _RPI_FUNC_H_

bool  RPI_BuildHtmlMessageBody               (NETCL *);
bool  RPI_BuildJsonMessageBody               (NETCL *, int);
bool  RPI_BuildJsonMessageArgs               (NETCL *, int);
int   RPI_CollectParms                       (NETCL *, FTYPE);
void  RPI_ReportBusy                         (NETCL *);
void  RPI_ReportError                        (NETCL *);
bool  RPI_RestoreDay                         (u_int32, u_int8 *, int);
bool  RPI_RestoreMonth                       (u_int32, u_int8 *, int);


#endif /* _RPI_FUNC_H_ */
